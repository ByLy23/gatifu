# Cálculo de WIP
***
### Metodología: Planning poker  
***

___Código de colores para ponderaciones en tarjetas de Trello:___

|Valor|Color|
|:-|:-|
|5| Morado|
|4| Rojo|
|3| Naranja|
|2| Amarillo|
|1| Verde|

***
_Se utilizó la ponderación de actividades para el cálculo de la cantidad de actividades que podrían ser realizadas por el equipo y asi obtener el WIP._

***

### Aplicación planning poker online

_Se utilizó esta aplicación para el cálculo de ponderación de las actividades utilizando la metodología de planning poker_

![poker](documentacion/files/ppoker.jpg)



### Sprints

- [Sprint #1](./documentacion/retro1.md)
- [Sprint #2](./documentacion/retro2.md)
- [Sprint #3](./documentacion/retro3.md)
- [Sprint #4](./documentacion/retro4.md)

