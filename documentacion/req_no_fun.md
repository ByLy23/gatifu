## Requerimientos no funcionales

- El sistema debe enviar un aviso al correo electrónico del usuario, notificando usuario y contraseña
- Cuando se registra un cliente el sistema lo colocará en una lista de espera para poder ser aprobado.
- El sistema debe tener la capacidad de registrar a diferentes tipos de usuarios, con y sin confirmación.
- El sistema debe enviar un link de pago para el cliente.
- El sistema debe validar el ingreso al sistema únicamente archivos de office, imágenes o pdf
- El sistema debe validar que la sesión del usuario esté activa previo a acceder a cualquier página. 
- El sistema debe redireccionar al usuario a la página de inicio, en caso no se encuentre la sesión activa.
- El sistema debe tener un sistema de seguridad para la información sensible ingresada por los usuarios.
- El sistema debe contar con un módulo de pago con tarjeta.
- El sistema debe proporcionar mensajes de confirmación en la realización de las tareas
- El sistema debe tener persistencia de datos.
- La aplicación debe tener un diseño intuitivo y de uso fácil.