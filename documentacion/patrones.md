## Patrones de diseño

***
- [Patrón Strategy](patronStrategy.md)
- [Patrón Builder](patronBuilder.md)
- [Patrón Template](patronTemplate.md)
- [Patrón State](patronState.md)
***

