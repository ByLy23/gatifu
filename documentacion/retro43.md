## Sprint #12

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|2|Joel|3|
|1|Audie|3|
|1|Marvin|2|
|1|Christopper|3|
|1|Byron|1|
|__6__|__Total__|__12__ | 



***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Diagrama devops|2|
|Actualizar documentación|1|
|CI/CD|3|
|Diagrama de pruebas|2|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Diagrama devops|Done||
|Actualizar documentación|Done||
|CI/CD|Done||
|Diagrama de pruebas |Done||



***
#### Tablero previo al sprint


![](documentacion/files/s12_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s12_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Diagrama devops|Joel|diagram-devops|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/diagram-devops>|
|Actualizar documentación|Joel/Byron|update-docu|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/update-docu>|
|Diagrama de pruebas|Marvin|diagram-devops|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/diagram-devops>|
|CI/CD|Audrie/Christopper|test/developPrueba-CICD|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/test/developPrueba-CICD>|





***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminó el Proceso de DevOps 

- **¿Qué se hizo mal durante el Sprint?**

Se tuvo retraso en las pruebas de integración continua

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Se debe completar las tareas con mejor tiempo e informar a los compañeros del estado de las mismas.

**Marvin:** 

- **¿Qué se hizo bien durante el Sprint?**

Se agregaron todos los diagramas ya revisados

- **¿Qué se hizo mal durante el Sprint?**

Nada que agregar

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Nada que agregar

**Joel:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron todas las tareas a tiempo.

- **¿Qué se hizo mal durante el Sprint?**

No se hizo priorización de tareas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Las tareas deben de dividirse en más secciones para que puedan ser finalizadas a tiempo en los sprints.

**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron todas las tareas	

- **¿Qué se hizo mal durante el Sprint?**

Tiempo muy limitado para verificar errores.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Planificación de tiempos.

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron todas las tareas

- **¿Qué se hizo mal durante el Sprint?**

No se pudo tener suficiente tiempo y no se priorizaron tareas importantes

- **¿Qué mejoras se deben implementar para el próximo sprint?**

La planificación de las tareas