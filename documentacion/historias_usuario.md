# Historias de usuario

### Cliente

- Como usuario quiero poder iniciar sesión de forma única y segura y que la clínica pueda conocer mi datos.
- Como Usuario quiero poder agendar una cita para poder ser atendido en la fecha seleccionada y evitar así la espera.
- Como usuario quiero poder visualizar los datos de mis mascotas para poder hacer actualizaciones.
- Como usuario quiero poder obtener descuentos para obtener mayores beneficios al ser un cliente frecuente.
- Como usuario quiero poder ver el historial médico de mis mascotas para tener acceso a los medicamentos recetados por el doctor.
- Como usuario quiero poder pagar con tarjeta para evitar tener que manejar efectivo.

### Médico

- Como doctor quiero cambiar el estado del paciente en espera a “en examinación” para poder reflejar el estado real en que se encuentran los pacientes en examinación e iniciar el cronómetro para el cálculo del costo total de la cita.
- Como doctor quiero cambiar el estado del paciente de examinado a “finalizado” para terminar la cita del paciente y el cronómetro que calcule los costos totales.
- Como doctor quiero visualizar las citas agendadas en un calendario para poder recuperar el historial y los datos de los pacientes antes de los exámenes.
- Como doctor quiero visualizar el historial y los datos del paciente para conocer el estado actual del paciente.
- Como doctor quiero poder cargar exámenes a la plataforma para alimentar el historial médico de los pacientes y dejar constancia de los estudios realizados y sus resultados.
- Como doctor quiero poder eliminar exámenes previamente cargados a la plataforma

### Administrador

- Como administrador quiero asignar ofertas a los paquetes existentes en la empresa para ofrecer más opciones de precios y pagos en los servicios existentes del hospital.
 -Como administrador quiero cambiar el estado de los pacientes, secretarias o médicos que han sido registrados en el hospital para poder  darlos  de alta en el sistema y así tener un mejor control del acceso de  los usuarios.
- Como administrador quiero cambiar el estado de los pacientes, secretarias o médicos que han sido registrados en el sistema del  hospital para poder  dar de baja, evitando oficialmente el uso del sistema y así tener un mejor control del acceso de los usuarios.
- Como administrador quiero aprobar las solicitudes de registro de los usuarios ingresados por la secretaría, los cuales se encuentran  en lista de espera, para poder registrarlos en el sistema.
- Como administrador quiero poder asignar descuentos según considere basándose en la combinación de motivos de consulta, esto para dar un servicio más llamativo para los usuarios que utilizaron varios servicios.

### Secretaria

- Como secretaria me gustaría iniciar con mis credenciales a la aplicación de una forma segura y sencilla.
- Como secretaria quiero que solo se mostrará una breve información de mi usuario cuando haya iniciado sesión y no ingresar muy seguidas veces mis credenciales
- Como secretaría quiero que en el momento de registrar a los clientes sea intuitivo  y que si es posible me autocomplete datos y que yo solo tenga que confirmar la información.
- Como secretaría quiero revisar la disponibilidad de salas de una forma gráfica y colorida para representar de mejor manera los estados de las salas para poder agendar citas de manera eficaz.
- Como secretaria quiero que al momento de revisar la disponibilidad de los médicos  sea en un calendario ostentoso en vista de 2 semanas y que se coloquen tipo stickers en cada hora y día que tenga programado el médico la cita y diferenciar cada médico con colores y ver su nombre de igual manera.
- Como secretaria quiero que en el sistema de pago de los clientes, que lleguen de emergencia, solicitar el número de caso de emergencia y que el sistema me realice el cálculo tomando en cuenta la hora de entrada que se registró y la de salida en el momento que estoy ingresado el número de emergencia, entonces que ya me aparezca dicho cálculo, y poder hacer el cobro, mostrando los detalles.
- Como secretaria quiero que en el momento de registrar una cita al cliente sea posible tener el cálculo previo del cobro por realizar, antes de mandar el correo de pago, para tener la confirmación del cliente.

