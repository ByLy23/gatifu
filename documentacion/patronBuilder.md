## Template Builder

Este patrón de diseño ayuda a solventar el problema de lectura del código. Ayuda a evitar el paso de parámetros excesivos en el constructor ya que esto hace parecer el código poco entendible, y podemos dar mantenimiento de una mejor manera. Para esto se debe crear por separado los métodos de clases y deben retornar el mismo objeto.

Este patrón de diseño se utilizó para la creación de usuarios debido a que, este actor posee múltiples parámetros en su estructura: 

![builder](./files/patronBuilder.png)

Usando el Builder Pattern tenemos más control sobre los argumentos y es altamente leíble y fácil de mantener

const newUser = await new personaBuilder(nombre,correo,edad,direccion,telefono)
    .setPassword(password)
    .setUsuario(usuario)
    .setEspecialidad(especialidad)
    .setHorario(horario)
    .setTipo(tipo)
    .build();
