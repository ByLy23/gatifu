## Sprint #11

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|1|Joel|1|
|1|Audie|3|
|1|Marvin|2|
|2|Christopper|5|
|1|Byron|5|
|__5__|__Total__|__16__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :- | :- |
|Actualizar documentación|1|
|Pruebas unitarias - secretaria|2|
|Pruebas unitarias - medicos|2|
|CI/CD pruebas unitarias|3|
|Pruebas automatizadas con Cypress|5|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Actualizar documentación|Blocked|Algunos diagramas no fueron finalizados a tiempo.|
|Pruebas automatizadas con Cypress|Done||
|Pruebas unitarias - medicos|Done||
|Pruebas unitarias - secretaria|Done||
|CI/CD|Blocked|No se terminó el flujo de CI/CD|


***
#### Tablero previo al sprint


![](documentacion/files/s11_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s11_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Actualizar documentación|Joel|update-docu|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/update-docu>|
|Pruebas unitarias - medicos|Marvin|feature/unit-tests-server-medicos|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/unit-tests-server-medicos>|
|Pruebas unitarias -secretaria|Christtopher|feature/unit-test-admin|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Funit-tests-admin>|
|Pruebas de automatizacion con Cypress |Byron|feature/test-automatizacion|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/test-automatizacion>|
|CI/CD|Audrie/Christopper|feature/test|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/test-automatizacion>|



***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se logró completar las tareas de la integración continua 

- **¿Qué se hizo mal durante el Sprint?**

Se logró terminar la tarea fuera de tiempo

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Calcular mejor el tiempo para las tareas asignadas y comunicar con el equipo la dificultad de las mismas.

**Marvin** 

- **¿Qué se hizo bien durante el Sprint?**

Todo estuvo correcto en los test		

- **¿Qué se hizo mal durante el Sprint?**

No se comentó cambios anteriores en los servidores

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Comentar los cambios que se hicieron para que todo el equipo está 



**Joel**

- **¿Qué se hizo bien durante el Sprint?**

Se trabajó bien en los sprints anteriores y no hubo mucha carga en este sprint.

- **¿Qué se hizo mal durante el Sprint?**

Se quedaron bloqueadas muchas tareas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Que las tareas se dividan de mejor manera.


**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se comenzaron con las pruebas y se inició con el proceso de integración continua. 

- **¿Qué se hizo mal durante el Sprint?**

No se avanzó mucho.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Terminar las actividades asignadas

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

El inicio de las pruebas

- **¿Qué se hizo mal durante el Sprint?**

El bloqueo de tareas

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Dividir mejor las tareas
