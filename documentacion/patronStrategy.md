## Patrón Strategy

### ¿Cómo funciona?

Es un patrón de diseño de comportamiento, permite definir una familia de algoritmos, colocar cada uno de ellos en una clase separada y hacer sus objetos intercambiables.
Este patrón toma una clase que hace algo en específico, pero de muchas formas diferentes, la clase principal se llama contexto y las clases separadas se llaman estrategias.  La clase contexto tiene un campo para almacenar la referencia a la estrategia a utilizar y en las clases estrategias se extraen los algoritmos extra o que diferencia a la clase de la base principal. También en la clase contexto se delega el trabajo a un objeto de estrategia vinculado en lugar de ejecutarlo por su cuenta, así mismo esta no es responsable de seleccionar el algoritmo adecuado para la tarea, el cliente es el encargado de pasar la estrategia deseada a la clase contexto. 

### ¿Dónde y por qué decidimos utilizarlo?

En nuestro proyecto decidimos utilizarlo en la adquisición de productos, servicios y paquetes, esto debido a que el hecho de la adquisición era similar con la única diferencia en que tanto paquetes como servicios, requerían del dato para identificar a la mascota a quién se le iban a aplicar dicho paquete o servicio, mientras que en los productos no era necesario. Por lo que se consideró que las estrategias podrían ser 2, una para la adquisición de productos y la segunda para la adquisición de servicios o paquetes. 

### Implementación

En este proyecto utilizamos nodejs para la parte del backend, por lo que las interfaces no existen como tal. Así a continuación se mostrará como se implementó el código para esta estrategia.
Para la clase contexto podemos ver lo siguiente:

![strategy1](./files/patronStrategy1.png)


Se definió un atributo para definir la estrategia a utilizar y otros atributos referentes a la forma de pago.
Y dado que no se utilizaron interfaces, se implementó una función que estaría en todas las estrategias desde la clase contexto. En este caso fue la función adquirir.
Del lado de las estrategias, podemos encontrar la estrategia utilizada para servicios y paquetes, como se puede ver en la siguiente imagen:


![strategy2](./files/patronStrategy2.png)

Esta estrategia permitía realizar la comunicación con la base de datos y crear la factura, así como agregar los servicios o paquetes a la factura y guardar el total facturado. Todo esto dentro de su función adquirir. 

También podemos encontrar la estrategia para los productos:


![strategy3](./files/patronStrategy3.png)

En esta estrategia se realiza lo mismo que en la estrategia de servicios, a excepción que no se solicitan ningún dato extra al obtenido en la clase contexto, en cambio la estrategia de servicio sí requiere datos extra como el id de la mascota y una fecha.

Por último la forma en que cada estrategia y la clase contexto se utilizan es mediante rutas como se puede observar:

![strategy4](./files/patronStrategy4.png)

![strategy5](./files/patronStrategy5.png)

En cada ruta dependiendo de lo que se desea adquirir se crea una instancia de la clase contexto así como del producto, servicio o paquete, dependiendo lo que se solicite y se envía la información necesaria por medio de parámetros para crear cada instancia.  

![strategy6](./files/patronStrategy6.png)