# Historias de usuario

### Sistema de puntos

- Como cliente quiero tener una billetera de puntos acumulados
- Como cliente quiero ganar puntos por adquisición de servicios
- Como cliente quiero poder obtener mi código de billetera de puntos 
- Como cliente me gustaría poder realizar pagos mediante el uso de puntos
- Como cliente me gustaría poder completar pagos mediante el uso de puntos y pagar con dinero o crédito.

