## Sprint #4

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|8| Joel|3 |
|17 |Byron|7 |
|16 |Marvin|9 |
|8 |Chris|4 |
|9 |Audrie|5 |
|55|__Total__|28 | 

***
#### WIP por tareas
|Tarea|Ponderación|
| :- | :- |
|post pago|1|
|Cancelar Pago|1|
|get link Pago|1|
|listado pagos realizados|1|
|Correo cuando se registra usuario|1|
|link de pago|3|
|optimizar peticion historial|2|
|horario laboral medico|1|
|manejo token frontend|3|
|manejar cerrar sesion|3|
|historial de citas pagadas|1|
|peticion token pago|2|
|ver historial medico|1|
|animacion de carga|1|
|validacion de archivos|1|
|optimizar peticion historial|1|
|horario laboral medico|1|
|manejo token frontend|1|
|manejar cerrar sesion|2|
|historial de citas pagadas|1|
|peticion token pago|1|
|ver historial medico|2|
|animacion de carga|1|
|validacion de archivos|1|
|Asignar oferta|2|
|Conterización|5|
|Documentación|1|



***

#####  Tabla comparativa del Sprint Backlog 

|Elemento|Estado|Justificación|
| :- | :- | :- |
|Asignar oferta|Done||
|Conterizacion|Done||
|Documentacion|Done||
|link de pago - frontend|Done||
|optimizar peticion historial|Done||
|horario laboral medico - frontend|Done||
|manejo token frontend|Done||
|manejar cerrar sesion |Done||
|historial de citas pagadas - frontend|Done||
|peticion token pago - frontend|Done||
|ver historial medico - frontend|Done||
|animacion de carga - frontend|Done||
|validacion de archivos - fronten|Done||
|post pago|Done||
|Cancelar Pago|Done||
|get link Pago|Done||
|listado pagos realizados|Done||
|Correo cuando se registra usuario|Done||

***
#### Tablero previo al sprint


![](documentacion/files/s4_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s4_t2.jpeg)
***
#### Ramas e historias

| Nombre historia                                    | Nombre de la persona | Nombre de la rama                     | Link hacia la rama                                                                                       |
| -------------------------------------------------- | -------------------- | ------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| Asignar oferta                                     | Joel                 | feature/procs-ofertas-DB              |                                                                                                          |
| Conterización                                      | Joel                 | feature/dockerizar                    | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/dockerizar                                  |
| Documentación                                      | Joel                 | feature/documentacion                 | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/documentacion                               |
| post pago                                          | Audrie               | feature/postpago-backend              | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postpago-backend                            |
| Cancelar Pago                                      | Audrie               | feature/cancelarpago-backend          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/cancelarpago-backend                        |
| get link Pago                                      | Audrie               | feature/getlistadopagos-backend       | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getlistadopagos-backend                     |
| listado pagos realizados                           | Audrie               | feature/getlistadopagos-backend       | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getlistadopagos-backend                     |
| Correo cuando se registra usuario                  | Audrie               | feature/correoregistrousuairo-backend | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/correoregistrousuairo-backend               |
| Midleware modulo Citas                             | christtopher         | feature/                              | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/mdlwr%2Dsesion                             |
| Midleware modulo Clientes                          | christtopher         | feature/                              | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/mdlwr%2Dsesion                             |
| Midleware modulo Medicos                           | christtopher         | feature/                              | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/mdlwr%2Dsesion                             |
| Midleware modulo Admin                             | Christtopher         | feature/mdlwr-admin                   | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=develop-,feature/mdlwr%2Dadmin,-bugfix/0.6         |
| Midleware modulo Sesion                            | christtopher         | feature/mdlwr-sesion                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/mdlwr%2Dsesion                             |
| Token de pagos                                     | christtopher         | feature/tkn-pago                      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Flink-tkn-pago |
| optimizar peticion historial                       | Marvin               | bugfix/0.3                            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.3                                               |
| horario laboral medico                             | Marvin               | bugfix/0.1                            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.1                                               |
| manejo token frontend                              | Marvin               | bugfix/0.4                            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.4                                               |
| manejar cerrar sesion                              | Marvin               | bugfix/0.6                            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.6                                               |
| historial de citas pagadas                         | Marvin               | feature/listarPagos-frontend          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/listarPagos-frontend                             |
| peticion token pago                                | Marvin               | bugfix/0.6                            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.6                                               |
| ver historial medico                               | Marvin               | bugfix/0.0.6                          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.0.6                                             |
| animacion de carga                                 | Marvin               | feature/moduloCarga-frontend          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCarga-frontend                             |
| validacion de archivos                             | Marvin               | feature/validarArchivos               | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/validarArchivos                                  |
| pago secretaria                                    | Byron                | feature/crearPagoSecretaria           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crearPagoSecretaria                         |
| muestra el calculo del pago que realiza el cliente | Byron                | feature/crearPagoSecretaria           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crearPagoSecretaria                         |



***
#### Conclusiones 

**Audrie**
- ___¿Qué se hizo bien durante el Sprint?___

    Se concluyeron todas las tareas bloqueadas y las que faltan por hacer. 

- ___¿Qué se hizo mal durante el Sprint?___

    Se perdió un poco el control entre lo que tenía que hacer cada integrante. 

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Definir un formato para nombrar variables.


**Joel**
- ___¿Qué se hizo bien durante el Sprint?___

    Se terminaron las tareas bloqueadas y pendientes de los sprints anteriores y se mejoró el manejo de los nombres de las variables. 


- ___¿Qué se hizo mal durante el Sprint?___

    Algunos merge hacia la rama develop generaron conflictos ya que algunas ramas estaban muy desactualizadas por atrasos en algunas entregas. No hubo una puesta común en el manejo de ciertos aspectos en la programación, lo que generó cierto conflicto a la hora de juntar algunas partes.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Puesta en común de estándares en la codificación, para que todos los métodos, funciones, etc. se manejen de la misma forma en cada parte de la producción.



**Christtopher**
- ___¿Qué se hizo bien durante el Sprint?___

    Se implementó el registro de las acciones por los distintos usuarios del sistema, así como la validación del link de pago de servicio.


- ___¿Qué se hizo mal durante el Sprint?___

    Algunos merges hacia la rama develop generaron conflictos ya que algunas ramas estaban muy desactualizadas por atrasos en algunas entregas. No hubo una puesta común en el manejo de ciertos aspectos en la programación, lo que generó cierto conflicto a la hora de juntar algunas partes.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Puesta en común de estándares en la codificación, para que todos los métodos, funciones, etc. se manejan de la misma forma en cada parte de la producción.



**Marvin** 

- ___¿Qué se hizo bien durante el Sprint?___

    Se terminó toda funcionalidad pendiente y se mejoró el manejo de errores, se implementó la seguridad en la aplicación, por último se crearon los contenedores para su producción.


- ___¿Qué se hizo mal durante el Sprint?___

    Se mejoró sobre los anteriores sprint la comunicación pero aún quedó pendiente probar las funcionalidades antes de colocarlas en develop.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Simplificar el codigo y asi poder tener una escalabilidad en el proyecto para la siguiente fase.



**Byron**

- ___¿Qué se hizo bien durante el Sprint?___

    Se terminaron todas las actividades programadas para la fecha de entrega.


- ___¿Qué se hizo mal durante el Sprint?___

    No probar todas las funcionalidades y retrasarse por errores que no debieran existir.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    La comunicación y estructura del desarrollo de actividades.


