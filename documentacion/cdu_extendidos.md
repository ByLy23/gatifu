## Casos de Uso

### Tabla de contenidos
- [Administrador](#admin) 
- [Doctores](#doc) 
- [Clientes](#cli) 
- [Secretarias](#secre) 

***
<a name="admin"/> 

_**Casos de usos de Administrador**_

|Nombre CDU|CDU001DOC\_Manejo\_de\_Ofertas\_y\_Precios|
| :- | :- |
|Descripción|El administrador asigna ofertas a los paquetes existentes ofrecidos en la empresa|
|Actores|Administrador, Sistema|
|Secuencia Normal| |
|Pasos|<p>·        El administrador selecciona un paquete de la lista de paquetes existentes en la empresa (E-1).</p><p>·        El administrador elige la opción de crear una nueva oferta para el paquete seleccionado (E-2).</p><p>·        El administrador ingresa los datos de la nueva oferta y la crea (E-3).</p><p>·        El sistema actualiza el precio del paquete aplicando la nueva oferta asignada (E-4).</p>|
|Excepciones|<p>E-1: El administrador selecciona un paquete equivocado; puede quitar ese paquete y volver a seleccionar al paquete correcto.</p><p> </p><p>E-3: El administrador ingresa mal los datos de la oferta creada; puede editar la oferta y corregir el dato erróneo.</p>|




|Nombre CDU|CDU002DOC\_Dar\_de\_Alta|
| :- | :- |
|Descripción|El administrador cambia el estado de un cliente, médico o secretaria registrado en el hospital a dado de alta .|
|Actores|Administrador|
|Secuencia Normal| |
|Pasos|<p>·        El administrador selecciona un usuario de la lista de usuarios aprobados para el registro del sistema (E-1).</p><p>·        El administrador cambia el estado del paciente seleccionado y lo da de alta(E-2).</p>|
|Excepciones|<p>E-1: El administrador selecciona un paciente equivocado; puede quitar ese paciente y volver a seleccionar al paciente correcto.</p><p> </p><p>E-2: El administrador se equivoca de estado asignado al paciente; puede volver a seleccionar el estado correcto.</p>|



|Nombre CDU|CDU003DOC\_Dar\_de\_Baja|
| :- | :- |
|Descripción|El administrador cambia el estado de un cliente, médico o secretaria registrado en el sistema a dado de baja.|
|Actores|Administrador|
|Secuencia Normal| |
|Pasos|<p>·        El administrador selecciona un usuario de la lista de usuarios dados de alta en el  sistema (E-1).</p><p>·        El administrador cambia el estado del usuario seleccionado y lo da de baja (E-2).</p>|
|Excepciones|E-1: El administrador selecciona un usuario equivocado; puede volver a dar de alta ese usuario y volver a seleccionar al paciente correcto.|




|Nombre CDU|CDU004DOC\_Aprobar\_Registro|
| :- | :- |
|Descripción|El administrador aprueba los registros ingresados por la secretaria en la lista de espera.|
|Actores|Administrador, Sistema|
|Secuencia Normal| |
|Pasos|<p>·        El administrador selecciona un usuario de la lista de espera de registro (E-1).</p><p>·        El administrador acepta los datos del nuevo usuario para ser ingresados al sistema (E-2).</p><p>·        El sistema envía un aviso de las credenciales del usuario al correo registrado en los datos del usuario (E-3).</p>|
|Excepciones|<p>E-1: El administrador selecciona un usuario equivocado; puede eliminar a ese usuario en caso no desee ingresar al sistema.</p><p>E-2: El administrador se equivoca y elimina al usuario elegido; Le aparecerá una confirmación para la eliminación de dicho usuario.</p><p> </p><p>E-3: El sistema no logra enviar el aviso al usuario; Se aborta la aprobación del registro del usuario seleccionado.</p>|



|Nombre CDU|CDU005DOC\_Manejo\_de\_Descuentos|
| :- | :- |
|Descripción|El administrador asigna un porcentaje de descuento al monto total facturado por un cliente, esto según la cantidad de motivos de consulta utilizados en una cita .|
|Actores|Administrador|
|Secuencia Normal| |
|Pasos|<p>·        El administrador ingresa la cantidad de motivos que desea agregar como condición de descuento (E-1).</p><p>·        El administrador asigna un descuento a la cantidad de motivos seleccionados (E-2).</p>|
|Excepciones|<p>E-1: El administrador ingresa la cantidad de motivos errónea; puede modificar dicha cantidad.</p><p> </p><p>E-3: El administrador se equivoca y asigna mal el descuento; puede modificar el descuento ingresado.</p>|


***
<a name="doc"/>  

_**Casos de usos de doctores**_

|Nombre CDU|CDU001DOC\_Examinar\_paciente|
| :- | :- |
|Descripción|El médico cambia el estado del paciente en espera a ser examinado y comienza a correr el tiempo de examinación.|
|Actores|Médico, Paciente|
|Secuencia Normal| |
|Pasos|<p>·       El médico selecciona el siguiente paciente a examinar seleccionado al paciente de la lista de pacientes con cita en una determinada fecha (E-1).</p><p>·       El médico cambia el estado del paciente en espera a en examinación seleccionando el estado de la lista de estados (E-2).</p>|
|Excepciones|<p>E-1: El médico selecciona un paciente equivocado; puede quitar ese paciente y volver a seleccionar al paciente correcto.</p><p></p><p>E-2: El médico cambia el estado del paciente a uno incorrecto; puede volver a seleccionar el estado correcto.</p>|




|Nombre CDU|CDU002DOC\_Finalizar\_examen|
| :- | :- |
|Descripción|El médico cambia el estado del paciente a finalizado y finaliza la cita del paciente.|
|Actores|Médico, Paciente|
|Secuencia Normal| |
|Pasos|<p>·       El médico cambia el estado del paciente en examinación a finalizado seleccionado el estado desde el listado de estado(E-3).</p><p>·       El médico finaliza la cita del paciente seleccionado la opción de finalizar cita.</p>|
|Excepciones|E-3: El médico cambia el estado del paciente a uno incorrecto; puede volver a seleccionar el estado correcto.|


|Nombre CDU|CDU003DOC\_Visualizar\_Calendario\_Citas|
| :- | :- |
|Descripción|El médico visualiza todas las citas agendadas en un calendario.|
|Actores|Médico|
|Secuencia Normal| |
|Pasos|<p>·       El médico selecciona la opción de desplegar calendario y visualiza las citas agendadas por mes.</p><p> </p>|
|Excepciones| |




|Nombre CDU|CDU004DOC\_Visualizar\_Datos\_Paciente|
| :- | :- |
|Descripción|El médico visualiza todos los datos disponibles de un paciente específico|
|Actores|Médico|
|Secuencia Normal| |
|Pasos|<p>·       El médico selecciona un paciente de la lista de pacientes registrados en la base de datos. (E-4)</p><p>·       El médico selecciona la opción de ver datos del paciente para visualizar todo el contenido relacionado con el paciente.</p><p> </p>|
|Excepciones|E-4: El médico selecciona el paciente incorrecto. Puede cerrar la opción de visualización actual y volver a seleccionar un paciente.|


|Nombre CDU|CDU005DOC\_Cargar\_Estudios|
| :- | :- |
|Descripción|El médico carga imágenes de estudios realizados a un paciente en específico.|
|Actores|Médico|
|Secuencia Normal| |
|Pasos|<p>·       El médico selecciona un paciente de la lista de pacientes disponibles en la base de datos (E-5).</p><p>·       El médico selecciona la opción de cargar estudio.</p><p>·       El médico selecciona la imagen a cargar (E-6).</p><p>·       El médico acepta la carga de la imagen y esta se muestra en la lista de exámenes cargados (E-7).</p><p> </p>|
|Excepciones|<p>E-5: El médico selecciona el paciente incorrecto. Puede cancelar la selección actual y volver a seleccionar el paciente correcto.</p><p>E-6: El médico selecciona la imagen incorrecta. Puede cancelar la selección actual y volver a seleccionar la imagen correcta.</p><p>E-7: El médico carga un estudio incorrecto. Puede seleccionar el estudio y eliminarlo, para luego volver a ejecutar el caso CD005\_DOC.</p>|





|Nombre CDU|CDU006DOC\_Cambiar\_Estado\_Paciente|
| :- | :- |
|Descripción|El médico selecciona un paciente y cambia su estado entre: en espera, en examinación y examinado|
|Actores|Médico|
|Secuencia Normal| |
|Pasos|<p>·       El médico selecciona al paciente al siguiente paciente que se encuentra agendado. (E-8).</p><p>·       El médico selecciona un estado de los estados disponibles en el dropdown. (E-9)</p><p>·       El médico guarda los cambios de la selección. (E-10)</p>|
|Excepciones|<p>E-8: El médico selecciona el paciente incorrecto. Puede cancelar la selección actual y volver a seleccionar el paciente correcto.</p><p>E-9: El médico selecciona el estado incorrecto. Puede volver a cambiar la selección al estado correcto.</p><p>E-10: El médico guarda los cambios con el estado incorrecto. Puede volver a iniciar el CDU006DOC\_Cambiar\_Estado\_Paciente</p>|




|Nombre CDU|CDU007DOC\_Cancelar\_Carga|
| :- | :- |
|Descripción|El médico cancela la carga actual de imágenes de estudios a un paciente.|
|Actores|Médico|
|Secuencia Normal| |
|Pasos|·       El médico selecciona la opción de cancelar en la carga de imágenes de estudios en el paciente actual (E-11)|
|Excepciones|E-11: El médico cancela por error la carga de estudios; debe ejecutar el caso CDU005DOC\_Cargar\_Estudios.|

***
<a name="cli"/>  

_**Casos de usos de clientes**_


|Nombre CDU|CDU001CLI\_Inicio\_Sesion|
| :- | :- |
|Descripción|El cliente inicia sesión en el sistema de la empresa.|
|Actores|Clientes|
|Secuencia Normal| |
|Pasos|<p>- El cliente ingresa a la página</p><p>- El cliente da click en botón de iniciar sesión</p><p>- El cliente ingresa su correo en el campo seleccionado al igual que la contraseña (E-1)</p><p>- El cliente da click en el botón de iniciar sesion (E-2)</p>|
|Excepciones|<p>E-1: Los datos no están llenos completamente.</p><p>E2: El usuario o la contraseña pueden ser incorrectos para el intento de iniciar sesión.</p>|



|Nombre CDU|CDU002CLI\_Ver\_Historial|
| :- | :- |
|Descripción|El cliente puede observar el historial médico que tiene con su mascota.|
|Actores|Cliente|
|Secuencia Normal| |
|Pasos|<p>- El cliente debe iniciar sesión (CDU001CLI\_Iniciar\_Sesion) para poder acceder a su perfil</p><p>- El cliente debe dar click al perfil.</p><p>- El cliente debe dar click en la mascota a revisar.</p><p>- El cliente puede dar click en historial médico para observar el estado de la mascota.</p>|
|Excepciones||



|Nombre CDU|CDU003CLI\_AgendarCita|
| :- | :- |
|Descripción|El cliente agenda una cita con una fecha y hora programada|
|Actores|Cliente|
|Secuencia Normal|` `Una cita es agendada para ser atendida en una fecha prevista por un Doctor específico el cliente obtiene un link de pago.|
|Pasos|<p>·       El cliente selecciona el menú de agendar cita.</p><p>·     El cliente selecciona una de la lista de mascotas las cuales le pertenecen</p><p>·      El cliente selecciona el tipo de servicio que necesita.</p><p>.      El cliente selecciona una fecha y hora disponible en el calendario.</p><p>.     El cliente procede a agendar la cita por medio del botón de confirmación agendar.</p>|
|Excepciones|<p>E-1: El Usuario escoge un servicio con un doctor que está ocupado.</p><p></p><p>E-2: El sistema asigna un doctor que esté disponible.</p><p>E-3: El Usuario escoge una fecha y un horario que ya no está disponible.</p><p>E-10: El médico guarda los cambios con el estado incorrecto. Puede volver a iniciar el CDU006DOC\_Cambiar\_Estado\_Paciente</p>|


***
<a name="secre"/>  

_**Casos de usos de secretaria**_


|Nombre CDU|Registrar Clientes|
| :- | :- |
|Descripción|La secretaria le solicita los datos al cliente para poder registrar lo. |
|Actores|Secretaría, paciente |
|Secuencia Normal||
|Pasos|<p>- El usuario se presenta al establecimiento </p><p>- El usuario comunica a qué tipo se presenta.</p><p>- La secretaria le solicita los datos requeridos.</p><p>- Se coloca el usuario en una lista de espera de aprobación		 </p>|
|Excepciones|<p>- El usuario ya existe </p><p>- El administrador rechaza el registro del cliente </p><p>- El cliente no tienen la información solicitada </p>|


|Nombre CDU|Revisar disponibilidad de salas |
| :- | :- |
|Descripción|La secretaría en caso de llegada del paciente por emergencia necesita saber en qué sala puede ir el paciente |
|Actores|Secretaria|
|Secuencia Normal||
|Pasos|<p>- Se presenta un nuevo caso de emergencias </p><p>- Se muestra un listado de todas las salas</p><p>- Se visualiza las salas llenas </p><p>- Se visualiza las salas que pueden aceptar nuevos pacientes </p>|
|Excepciones|<p>- La sala no está llena pero no es capaz de recibir el nuevo cliente</p><p>- El usuario solicita una sala, solo para el.</p><p>- No hay ninguna sala disponible</p>|


|Nombre CDU|Revisar disponibilidad de médico |
| :- | :- |
|Descripción|La secretaría en caso de llegada del paciente por emergencia necesita asignar un medio disponible|
|Actores|Secretaria|
|Secuencia Normal||
|Pasos|<p>- Se asigna una sala al paciente </p><p>- Se le busca un doctor disponible </p><p>- Se busca un doctor con menos carga de trabajo</p>|
|Excepciones|<p>- El médico no puede asistir a la sala </p><p>- El médico no se especializa en la emergencia presentada</p>|



|Nombre CDU|Manejar el sistema de pago|
| :- | :- |
|Descripción|Al finalizar el servicio el médico cambia a estado de finalizado, y el paciente se presenta con la secretaria para poder cancelar |
|Actores|Secretaría, paciente|
|Secuencia Normal||
|Pasos|<p>- El médico termina de atender al paciente </p><p>- La secretaria le presenta el cálculo del cobro que se ha realizado  </p><p>- El paciente realiza el pago </p>|
|Excepciones|<p>- No se registró la hora de llegada del paciente </p><p>- El paciente no cuenta con la cantidad para realizar el pago.</p>|




