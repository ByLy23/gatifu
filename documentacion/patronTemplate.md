## Template Pattern

El patrón de diseño témplate provee una línea de pasos para un algoritmo. Los objetos que implementan estos pasos retienen la estructura original del algoritmo, pero tiene la opción de redefinirlo o ajustar ciertos pasos. Este patrón está diseñado para ofrecer extensibilidad al cliente desarrollador.

Es usado frecuentemente para un propósito general de un framework o una librería la cual será usada por otros desarrolladores, como ejemplo sería un objeto que tendrá una secuencia de eventos en respuesta a ciertas acciones, como un proceso de solicitudes. 

El equipo de desarrollo ha aplicado este patrón de diseño a la creación de citas debido a que existen diversos tipos de citas que podrían irse generando al pasar el tiempo lo que provee una fácil aplicación de estas al momento de desarrollarlas. 

Para esto se utilizan clases abstractas y se puede elegir qué métodos deben ser implementados y sobre escritos. 

![builder](./files/patronTemplate.png)


