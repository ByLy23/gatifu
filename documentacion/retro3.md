## Sprint #3

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|7| Joel|5 |
|6 |Byron|5 |
|8 |Marvin|8 |
|7 |Chris|4 |
|7 |Audrie|8 |
|37|__Total__|30 | 

***
#### WIP por tareas
|Tarea|Ponderación|
| :- | :- |
|crear procs. de creación de citas y obtención de citas|4|
|crear procs. de creación de recetas y obtención de recetas|2|
|crear procs. para el manejo de pagos|2|
|crear procs. para la actualización de datos en citas|1|
|crear procs. para la actualización de estados en citas|1|
|obtener historial médico|1|
|obtener citas pendientes|1|
|get médico y estado médico|1|
|getRecetas|1|
|get citas registradas|1|
|get medicamentos|1|
|getSalas|1|
|obtener cita|1|
|Crear Cta Frontend|3|
|Agregar Motivo a la cita Frontend|2|
|En listar medicos y salas ocupados Frontend|3|
|Crear estudios al motivo de la cita Frontend|2|
|Obtener el historial de cada cliente Frontend|2|
|Obtener las citas pendientes por cliente Frontend|2|
|Obtener Cita a despachar Frontend|2|
|Actualizdo Cita Frontend|2|
|Obtengo Medicamentos Frontend|1|
|Se filtran medicos, horas y salas Frontend|3|
|Crear Receta a un motivo de la Cita Frontend|2|
|Obtener todas las salas Frontend|1|
|Obtengo todas las mascotas del cliente Frontend|1|

***

#####  Tabla comparativa del Sprint Backlog 

|Elemento|Estado|Justificación|
| :- | :- | :- |
|crear procs. de creación de citas y obtención de citas|Done||
|crear procs. de creación de recetas y obtención de recetas|Done||
|crear procs. para el manejo de pagos|Validación|Espera validación por parte del frontend.|
|crear procs. para la actualización de datos en citas|Done||
|crear procs. para la actualización de estados en citas|Done||
|<p>obtener historial médico</p><p></p>|Done||
|obtener citas pendientes|Done||
|get médico y estado médico|Done||
|getRecetas|Done||
|get citas registradas|Done||
|get medicamentos|Done||
|getSalas|Done||
|obtener cita|Done||
||||
|Crear Cta Frontend|Done||
|Agregar Motivo a la cita Frontend|Done||
|En listar medicos y salas ocupados Frontend|Done||
|Crear estudios al motivo de la cita Frontend|Done||
|Obtener el historial de cada cliente Frontend|Done||
|Obtener las citas pendientes por cliente Frontend|Done||
|Obtener Cita a despachar Frontend|Done||
|Actualizdo Cita Frontend|Done||
|Obtengo Medicamentos Frontend|Done||
|Se filtran medicos, horas y salas Frontend|Done||
|Crear Receta a un motivo de la Cita Frontend|Done||
|Obtener todas las salas Frontend|Done||
|Obtengo todas las mascotas del cliente Frontend|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s3_t1.jpg)


#### Tablero en la finalización del sprint


![](documentacion/files/s3_t2.png)
***


#### Ramas e historias

| Nombre historia                                            | Nombre de la persona | Nombre de la rama                      | Link hacia la rama                                                                                       |
| ---------------------------------------------------------- | -------------------- | -------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| crear procs. de creación de citas y obtención de citas     | Joel                 | feature/procs-manejo-citas-DB          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-manejo-citas-DB                       |
| crear procs. de creación de recetas y obtención de recetas | Joel                 | feature/procs-manejo-recetas-DB        | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-manejo-recetas-DB                     |
| crear procs. para el manejo de pagos                       | Joel                 | feature/procs-manejo-pagos-DB          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-manejo-pagos-DB                       |
| crear procs. para la actualización de datos en citas       | Joel                 | feature/procs-manejo-citas-DB          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-manejo-citas-DB                       |
| crear procs. para la actualización de estados en citas     | Joel                 | feature/procs-manejo-citas-DB          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-manejo-citas-DB                       |
| Crear Cta Frontend                                         | Marvin               | feature/CrearCita-frontend             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/CrearCita-frontend                               |
| Agregar Motivo a la cita Frontend                          | Marvin               | feature/CrearCita-frontend             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/CrearCita-frontend                               |
| En listar medicos y salas ocupados Frontend                | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| Crear estudios al motivo de la cita Frontend               | Marvin               | bugfix/citas-frontend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/citas-frontend                                    |
| Obtener el historial de cada cliente Frontend              | Marvin               | bugfix/0.0.3                           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.0.3                                             |
| Obtener las citas pendientes por cliente Frontend          | Marvin               | bugfix/citas-frontend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/citas-frontend                                    |
| Obtener Cita a despachar Frontend                          | Marvin               | bugfix/0.0.3                           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.0.3                                             |
| Actualizdo Cita Frontend                                   | Marvin               | bugfix/0.0.5                           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/bugfix/0.0.5                                             |
| Obtengo Medicamentos Frontend                              | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| Se filtran medicos, horas y salas Frontend                 | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| Crear Receta a un motivo de la Cita Frontend               | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| Obtener todas las salas Frontend                           | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| Obtengo todas las mascotas del cliente Frontend            | Marvin               | feature/moduloCitas-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/moduloCitas-frontend                             |
| post cita bakend                                           | Christtopher         | feature/citas-backend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Fcitas-backend |
| post motivo cita backend                                   | Christtopher         | feature/citas-backend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Fcitas-backend |
| update estado cita backend                                 | Christtopher         | feature/citas-update-backend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/citas%2Dupdate%2Dbackend                   |
| post receta backend                                        | Christtopher         | feature/receta-backend                 | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/receta%2Dbackend                           |
| obtener historial médico                                   | Audrie               | feature/obtenerhistorialmedico-backend | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/obtenerhistorialmedico-backend              |
| obtener citas pendientes                                   | Audrie               | feature/citas-backend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/citas-backend                               |
| get médico y estado médico                                 | Audrie               | feature/obtenerhistorialmedico-backend | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/obtenerhistorialmedico-backend              |
| getRecetas                                                 | Audrie               | feature/getRecetas-backend             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getRecetas-backend                          |
| get citas registradas                                      | Audrie               | feature/citas-backend                  | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/citas-backend                               |
| get medicamentos                                           | Audrie               | feture/getMedicamentos-backend         | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feture/getMedicamentos-backend                      |
| getSalas                                                   | Audrie               | feature/getSalas-backend               | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getSalas-backend                            |
| obtener cita                                               | Audrie               | feature/obtereCita-backend             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/obtereCita-backend                          |
| ver Horarios citas                                         | Byron                | feature/moduloSecretaria               | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/moduloCitas                                 |
| Revisar disponibildad de medico                            | Byron                | feature/moduloSecretaria               | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/moduloCitas                                 |
| Registro emergencia                                        | Byron                | feature/creacionEmergencia             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/creacionEmergencia                          |



***

#### Conclusiones 

**Audrie**
- ___¿Qué se hizo bien durante el Sprint?___

    Se realizaron las tareas seleccionadas en el sprint. 

- ___¿Qué se hizo mal durante el Sprint?___

    No se manejó correctamente la carga de trabajo. No se listaron todas las tareas y se tuvieron que listar nuevas o modificar algunas ya listadas.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Mejorar la integración entre el backend y el frontend.

**Joel**
- ___¿Qué se hizo bien durante el Sprint?___

    Se trabajó bastante bien y en equipo en la realización de las tareas de citas que eran las tareas que más necesitan de trabajo en conjunto.

- ___¿Qué se hizo mal durante el Sprint?___

    Aún no se logró arreglar el problema al estilo de escritura de las tareas, se utilizaron camel case y snake case en variables y generaron algunos  errores.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Definir el estilo de escritura y que sea único para todos los desarrolladores.

**Christtopher**
- ___¿Qué se hizo bien durante el Sprint?___

    Se logró completar funcionalidades para el control de Citas

- ___¿Qué se hizo mal durante el Sprint?___

    Las diferentes maneras de programar de cada uno no seguían un marco establecido que hubiera facilitado una mejor manera de implementar y desarrollar.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Estandarizar la manera de trabajar para que pueda ser de mayor ayuda al implementar y desarrollar.

**Marvin** 

- ___¿Qué se hizo bien durante el Sprint?___

    Se realizó todo el módulo citas del lado del médico y del cliente, se llevó un registro de cada cita y se lleva la implementación de realizar la receta y estudio para cada cita 

- ___¿Qué se hizo mal durante el Sprint?___

    No realizar pruebas de cómo se realiza la consulta hacia la base de datos y retrasar la implementación en el frontend

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Que todos los desarrolladores tengan la misma de cómo se va implementar la funcionalidad



**Byron**

- ___¿Qué se hizo bien durante el Sprint?___

    Se terminaron las conexiones con el backend del sprint anterior y se completaron la mayoria de las tareas de este sprint, como la creacion de citas de clientes

- ___¿Qué se hizo mal durante el Sprint?___

    No se tomaron en cuenta algunas implementaciones ya existentes y se realizaron unas similares por lo que eso retrasó el trabajo ya que se realizaron dos tareas que realizan la misma función.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Al momento de realizar las implementaciones, se debe comunicar al equipo de desarrollo el uso de las mismas.

