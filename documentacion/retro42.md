## Sprint #8

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|3| Joel|2 |
|2 |Byron|1 |
|2 |Marvin|1 |
|3 |Chris|2 |
|2 |Audrie|1 |
|__12__|__Total__|__7__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Actualizar documentación|2|
|Dockerizacion f2|1|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Actualizar documentación|Done||
|Dockerizacion f2|Done||
|Mockups|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s8_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s8_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Actualizar documentación|Joel, Marvin, Christtopher, Byron, Audrie|feature/update-docu|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/update-docu>|
|Dockerizacion f2|Joel, Christtopher|feature/dockerizacion-f2|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Fdokerizacion>|




***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se completó con éxito la documentación de las tareas designadas previamente.

- **¿Qué se hizo mal durante el Sprint?**

Se tiene que mejorar los tiempos de integración debido a que estuvieron cortos para las tareas designadas. 

- **¿Qué mejoras se deben implementar para el próximo sprint?**

nada que agregar

**Marvin** 

- **¿Qué se hizo bien durante el Sprint?**

Se completaron las tareas sin ningún tipo de inconveniente

- **¿Qué se hizo mal durante el Sprint?**

Se hicieron cambios de último momento en la unión de todo el trabajo

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Tener en cuenta el tiempo límite de entrega para el sprint 


**Joel**

- **¿Qué se hizo bien durante el Sprint?**

Todas las tareas se terminaron con antelación.

- **¿Qué se hizo mal durante el Sprint?**

El tiempo del sprint fue muy corto en comparación con los anteriores.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Configurar mejor los tiempos de los sprints.


**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron todas las tareas correctamente.

- **¿Qué se hizo mal durante el Sprint?**

Dado que fue la unión de todo lo realizado a lo largo de los sprints, no hay mayor cosa que se hizo mal.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Tal vez, intentar terminar todo al menos un día antes del final del sprint, por cualquier cosa. 

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

Se pudieron completar las tareas realizadas dentro del tiempo fijado para el mismo.

- **¿Qué se hizo mal durante el Sprint?**

El tiempo fue un poco limitado, para la creación de las tareas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Una mejor planificación, ordenada para todos los sprints