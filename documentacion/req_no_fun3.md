## Requerimientos no funcionales

- RQNF01 - El sistema deberá de realizar los cálculos de precios a pagar automáticamente sin necesidad de realizar consultas desde botones u opciones extra
- RQNF02 - El sistema deberá mantener el sistema de puntos actualizado tras cada transacción que realicen los usuarios.

