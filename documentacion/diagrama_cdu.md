# Diagramas de casos de uso  

## Administrador  
![Casos de uso administrador](documentacion/files/CDU01.jpg)  

## Clientes  
![Casos de uso clientes](documentacion/files/CDU02.png)  

## Doctores  
![Casos de uso doctores](documentacion/files/CDU03.png)  

## Secretarias  
![Casos de uso secretarias](documentacion/files/CDU04.jpg)  