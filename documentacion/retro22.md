## Sprint #6

***
#### WIP por integrante
|**Suma**|**Integrante**|**Actividades**|
| :- | :- | :- |
|10| Joel|2 |
|6 |Byron|4 |
|15 |Marvin|6 |
|3 |Chris|2 |
|7 |Audrie|2 |
|__41__|__Total__|__16__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Creación de procs. para la gestión de paquetes|5|
|Creación de procs. para el pago de productos, servicios y paquetes|5|
|Ver todos los paquetes disponibles|1|
|Modificar Paquete|2|
|Editar la cantidad del precio del paquete|2|
|Eliminar Paquete - Frontend|3|
|Crear Paquete con servicios - Frontend|4|
|Ver todos los paquetes Disponibles|1|
|Modificar Paquete - Frontend|2|
|Agregar o eliminar servicios del paquete - Frontend|3|
|Comprar Producto|2|
|Crear paquete con todos los servicios agregados - Backend|5|
|Eliminar Paquete - Backend|1|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Creación de procs. para la gestión de paquetes|Done||
|Creación de procs. para el pago de productos, servicios y paquetes|Done||
|Ver todos los paquetes disponibles|<p>Done</p><p></p>||
|Modificar Paquete|Done||
|Editar la cantidad del precio del paquete|Done||
|Eliminar Paquete - Frontend|Done||
|Crear Paquete con servicios - Frontend|Done||
|Ver todos los paquetes Disponibles|Done||
|Modificar Paquete - Frontend|Done||
|Agregar o eliminar servicios del paquete - Frontend|Done||
|Comprar Producto|Done||
|Crear paquete con todos los servicios agregados - Backend|Done||
|Eliminar Paquete - Backend|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s6_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s6_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Creación de procs. para la gestión de paquetes|Joel|feature/gestionPaquetes-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/gestionPaquetes-DB>|
|Creación de procs. para el pago de productos, servicios y paquetes|Joel|feature/gestionPagos-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/gestionPagos-DB>|
|Eliminar servicios al paquete|Christtopher|feature/serviciosPaquetes-backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2FserviciosPaquetes-backend>|
|Agregar servicios al paquete|Christtopher|feature/serviciosPaquetes-backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2FserviciosPaquetes-backend>|
|Eliminar Paquete - Frontend|Marvin|feature/crudPaquete-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudPaquete-frontend>|
|Crear Paquete con servicios - Frontend|Marvin|feature/crudPaquete-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudPaquete-frontend>|
|Ver todos los paquetes Disponibles|Marvin|feature/crudPaquete-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudPaquete-frontend>|
|Modificar Paquete - Frontend|Marvin|feature/crudPaquete-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudPaquete-frontend>|
|Agregar o eliminar servicios del paquete - Frontend|Marvin|feature/crudPaquete-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudPaquete-frontend>|
|Comprar Producto|Marvin|feature/compra-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/compra-frontend>|
|Crear paquete con todos los servicios agregados|Audrie|feature/postPaquete-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postPaquete-Backend>|
|Eliminar paquete|Audrie|feature/deletePaquete-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/deletePaquete-Backend>|
|Crear patron de diseno state - Backend|Byron|feature/statePattern|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/statePattern>|
|Ver todos los paquetes disponibles -Backend|Byron|feature/paqueteUpdates|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/paqueteUpdates>|
|Modificar paquete - Backend|Byron|feature/paqueteUpdates|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/paqueteUpdates>|
|Editar la cantidad de precio del paquete - Backend|Byron|feature/paqueteUpdates|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/paqueteUpdates>|


***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**
Se completó tareas de agregar servicios a paquetes la cual fue bien implementada al haber seguido los documentos de control proporcionados por el equipo, y así cumplir con cada parámetro necesitado en esta tarea.

- **¿Qué se hizo mal durante el Sprint?**
El método para utilizar los los presento algunos problemas en nuevas funcionalidades que se implementaron en este sprint 

- **¿Qué mejoras se deben implementar para el próximo sprint?**
Compartir de mejor manera las funcionalidades implementadas y el funcionamiento de las mismas con el equipo para que todos las tomen en cuenta a la hora de desarrollarlas.


**Marvin** 

- **¿Qué se hizo bien durante el Sprint?**

Las tareas se simplificaron por la buena planificación, y la toma de decisiones entre todos los integrantes del grupo ayudó a tener una idea clara sobre las tareas 

- **¿Qué se hizo mal durante el Sprint?**

Se hicieron varias correcciones al juntar el servidor y el frontend, por falta de pruebas por separado en cada tarea

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Tener en cuenta toda la información que se solicita en la documentación para realizar las tareas asignadas y de esa manera no tener discrepancias entre backend y frontend


**Joel**

- **¿Qué se hizo bien durante el Sprint?**

La repartición de tareas fue óptima, se mantuvo debajo del WIP y se completaron en tiempo todas las tareas.

- **¿Qué se hizo mal durante el Sprint?**

Hubo confusión en el formato de las fechas al no ser definidas en la documentación como en la primera fase.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Definir el manejo de horas en el documento de endpoints para que no exista confusiones futuras.


**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se eligieron con un orden coherente las tareas del backlog

- **¿Qué se hizo mal durante el Sprint?**

Se presentaron algunos problemas al aplicar los métodos para registrar los logs.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Realizar más pruebas integrando tareas, previo a terminar el sprint. 

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

La distribución de las tareas fue más ordenada y creada de mejor manera

- **¿Qué se hizo mal durante el Sprint?**

Las tareas se realizaron y se validaron muy poco antes de la terminación del sprint

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Mejorar la forma en la cual se trabaja para realizar mejor las pruebas y la integración de la aplicacion, ademas de manejar de mejor manera el git