## Requerimientos funcionales

- El administrador puede dar de alta a clientes, médicos y secretarias
- El administrador puede dar de baja a clientes, médicos y secretarias
- El administrador es el encargado de manejar ofertas y precios.
- El administrador puede aprobar registros de usuarios.
- El administrador debe poder manejar descuentos en las tarifas de las citas

- Los médicos podrán cargar exámenes en formato de imagen a la plataforma
- Los médicos podrán terminar una cita.
- Los médicos podrán cambiar los estados de un paciente (En espera, examinando, examinado)
- Los médicos podrán visualizar los datos de los pacientes.
- Los médicos podrán visualizar su calendario de citas agendadas.

- El Usuario debe ser capaz de agendar sus citas. 
- El usuario puede elegir qué doctor quiere que lo atienda
- El usuario puede elegir qué día quiere su cita
- El usuario puede elegir el horario en que quiere su cita

- La secretaría es la encargada de registrar a los clientes y de clasificar su llegada 
- La secretaría revisa la disponibilidad de salas para poder agendar citas
- La secretaria revisa la disponibilidad de los médicos para poder asignarlo a una cita
- El sistema de pago de los clientes los maneja únicamente la secretaria, dependiendo del caso se solicita el tipo de pago a realizar. 