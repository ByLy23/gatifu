## Sprint #7

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|0| Joel|0 |
|1 |Byron|1 |
|2 |Marvin|2 |
|0 |Chris|0 |
|1 |Audrie|1 |
|__4__|__Total__|__4__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :- | :- |
|obtener Factura - Backend|1|
|Ver detalles de la facturación - Backend|1|
|Obtener factura - Frontend|1|
|Obtener detalle factura - Frontend|1|




***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|obtener Factura - Backend|Done||
|Ver detalles de la facturación - Backend|Done||
|Obtener factura - Frontend|Done||
|Obtener detalle factura - Frontend|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s7_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s7_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Ver detalles de la facturacion|Audrie|feature/detalleFactura-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/detalleFactura-Backend>|
|Obtener factura|Byron|feature/factura-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/factura-Backend>|
|Obtener factura - Frontend|Marvin|feature/obtenerFactura-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/obtenerFactura-frontend>|
|Obtener detalle factura - Frontend|Marvin|feature/obtenerFactura-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/obtenerFactura-frontend>|


***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**
Se completo el patrón de diseño template el cual cumplio con la funcionalidad previa que se tenía. 

- **¿Qué se hizo mal durante el Sprint?**
La tarea se retrasó por falta de conocimiento de la funcionalidad.

- **¿Qué mejoras se deben implementar para el próximo sprint?**
Estar más al pendiente de las tareas realizadas por otros colaboradores.

**Marvin** 

- **¿Qué se hizo bien durante el Sprint?**

Se tenían todas las tareas simplificadas por con siguiente completamos todo a buen tiempo

- **¿Qué se hizo mal durante el Sprint?**

Hubo diferencias de formato para los datos de la fecha 

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Mayor observación a los datos requeridos por parte del desarrollador del frontend


**Joel**

- **¿Qué se hizo bien durante el Sprint?**

Se completaron gran cantidad de tareas en los anteriores sprints por lo que en este sprint no existió mucha presión. 

- **¿Qué se hizo mal durante el Sprint?**

No funcionó el método que se creó para manejar tiempos porque ya habían otros métodos que la manejaban de distinta forma y por no cambiarlos se decidió no aplicarlo.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Nada que agregar en este sprint.


**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron las tareas a tiempo.

- **¿Qué se hizo mal durante el Sprint?**

Hubo una confusión con la estructura de envío de información al frontend.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Fijarse más en la estructura de envío. 

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

Se terminaron todos los tasks planificados para el día, de forma satisfactoria.

- **¿Qué se hizo mal durante el Sprint?**

Poca comunicación y pruebas para la creación de las diferentes tareas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Una mejor comunicación entre el grupo, y ser más óptimos al momento de realizar las pruebas necesarias.