## Casos de Uso

|Nombre CDU|CDU 001 Visualizar puntos|
| :- | :- |
|Descripción |El cliente visualiza  la cantidad de puntos que ha adquirido en sus compras anteriores|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al portal por medio del login</p><p>- El cliente selecciona su perfil</p><p>- El cliente visualiza la cantidad de puntos disponibles para canjear.</p>|
|Excepciones||




|Nombre CDU|CDU 002 Presentar código de usuario|
| :- | :- |
|Descripción |El cliente presenta su código de usuario para poder utilizar el sistema de puntos en la adquisición de servicios|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al portal por medio de login</p><p>- El cliente selecciona su perfil</p><p>- El cliente selecciona la opción de presentar su código personal</p>|
|Excepciones||





|Nombre CDU|CDU 003 Pagar servicios con puntos|
| :- | :- |
|Descripción |El cliente utiliza sus puntos acumulados para canjearlos por servicios de grooming y/o servicios en citas|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al portal</p><p>- El cliente selecciona el tipo de servicio a adquirir</p><p>- El cliente agregar los servicios a adquirir</p><p>- El cliente agrega la cantidad de puntos a canjear por cada servicios (E-1)</p><p>- Se genera la orden de pago</p><p>- El cliente paga la orden de pago</p>|
|Excepciones|- E-1: La cantidad de puntos ingresada es mayor a la cantidad de puntos disponible. El cliente debe de volver a ingresar una cantidad de puntos válida.|
|||




|Nombre CDU|CDU 004 Pagar servicios de forma híbrida|
| :- | :- |
|Descripción |El cliente utiliza puntos acumulados para canjearlos por precios parciales de servicios de grooming y/o servicios de citas, más adelante completa el pago con dinero real. |
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al portal</p><p>- El cliente selecciona el tipo de servicio a adquirir</p><p>- El cliente agregar los servicios a adquirir</p><p>- El cliente agrega la cantidad de puntos a canjear por cada servicios (E-1)</p><p>- Se genera la orden de pago</p><p>- El cliente completa la compra de servicios con el resto del dinero que falte para adquirir el servicio</p>|
|Excepciones|- E-1: La cantidad de puntos ingresada es mayor a la cantidad de puntos disponible. El cliente debe de volver a ingresar una cantidad de puntos válida.|

