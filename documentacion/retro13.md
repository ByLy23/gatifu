## Sprint #9

***
#### WIP por integrante
|**Actividades**|**Integrante**|**Suma**|
| :- | :- | :- |
|4|Joel|8|
|2|Audie|2|
|5|Marvin|9|
|2|Christopper|5|
|1|Byron|5|
|__14__|__Total__|__29__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Actualización de base de datos con nuevas entidades solicitadas|1|
|Creación y relleno de sistema de puntos|2|
|Creación de procs. para el manejo de sistema de puntos|3|
|Actualización de procs. para el nuevo sistema de puntos|2|
|Obtener Puntos del usuario - frontend|1|
|Presentar codigo - frontend|1|
|Obtener puntos a ganar y precio de puntos por servicio - frontend|2|
|Obtener puntos a ganar y precio de puntos por motivo cita - frontend|2|
|Realizar calculo de precio al realizar compra hibrida - frontend|1|
|Realizar la suma de puntos a canjaer y a ganar - frontend|1|
|Actualizar puntos de usuario - frontend|1|
|Post Pagos Puntos|1|
|Pruebas de automatización con Cypress|5|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Obtener Puntos del usuario - frontend|Done||
|Presentar código - frontend|Done||
|Obtener puntos a ganar y precio de puntos por servicio - frontend|Done||
|Obtener puntos a ganar y precio de puntos por motivo cita - frontend|Done||
|Realizar calculo de precio al realizar compra hibrida - frontend|Done||
|Realizar la suma de puntos a canjaer y a ganar - frontend|Done||
|Actualizar puntos de usuario - frontend|Done||
|Realizar pagos con puntos, híbridos y totales|Done||
|Obtener el total de puntos por Cliente|Done||
|Pruebas de automatización con Cypress|Blocked|No se tenían todas las funcionalidades para realizar las pruebas|


***
#### Tablero previo al sprint


![](documentacion/files/s9_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s9_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Actualización de base de datos con nuevas entidades solicitadas|Joel|updateDB3-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/updateDB3-DB>|
|Creación y relleno de sistema de puntos|Joel|createSisPts-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/createSisPts-DB>|
|Creación de procs. para el manejo de sistema de puntos|Joel|createProcsSisPts-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/createProcSisPts-DB>|
|Actualización de procs. para el nuevo sistema de puntos|Joel|updateProcsSisPts-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/updateProcsSisPts-DB>|
|Pruebas de automatizacion|Byron|feature/test-automatizacion|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/test-automatizacion>|
|Obtener Puntos del usuario - frontend|Marvin|feature/manejoPuntos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/manejoPuntos-frontend>|
|Presentar codigo - frontend|Marvin|feature/manejoPuntos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/manejoPuntos-frontend>|
|Obtener puntos a ganar y precio de puntos por servicio - frontend|Marvin|feature/manejoPuntos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/manejoPuntos-frontend>|
|Obtener puntos a ganar y precio de puntos por motivo cita - frontend|Marvin|feature/manejoPuntos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/manejoPuntos-frontend>|
|Realizar calculo de precio al realizar compra hibrida - frontend|Marvin|bugfix/1.8|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/bugfix/1.8>|
|Realizar la suma de puntos a canjaer y a ganar - frontend|Marvin|bugfix/1.8|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/bugfix/1.8>|
|Actualizar puntos de usuario - frontend|Marvin|feature/manejoPuntos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/manejoPuntos-frontend>|
|Pos Pagos Puntos|Audrie|feature/postPagosPuntos|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postPagosPuntos>|
|Puntos cliente|Audire|feature/getPuntosCliente|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getPuntosGanados>|
|Cambio de Puntos|Christtopher|feature/cambioPuntos|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2FcambioPuntos>|
|Cita Cliente Update|Christtopher|feature/citaClientUpdate|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2FcitaClientUpdate>|


***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se trabajo del lado del backend la parte de obtener puntos y se comenzaron las pruebas unitarias con jest. 

- **¿Qué se hizo mal durante el Sprint?**

No se logró completar la tarea de las pruebas unitarias.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Se debe calcular mejor el tiempo para la implementación de cada tarea y comunicar con el equipo los avances de las mismas.

**Marvin:** 

- **¿Qué se hizo bien durante el Sprint?**

Se implementan todas las tareas a tiempo y el análisis del cálculo en el precio, por puntos es acertado. 

- **¿Qué se hizo mal durante el Sprint?**

No realizar pruebas si se completa con éxito la funcionalidad antes de hacer merge a develop.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Mayor comunicación entre desarrolladores.

**Joel:**

- **¿Qué se hizo bien durante el Sprint?**

Las tareas tomadas entraban dentro de la ponderación de WIP manejada en los anteriores ciclos.

- **¿Qué se hizo mal durante el Sprint?**

Se ponderaron mal algunas tareas ya que resultaron con más carga que otras y tuvieron que ser bloqueadas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Mejorar la técnica de ponderación de tareas.



**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se realizaron todas las tareas bien. 

- **¿Qué se hizo mal durante el Sprint?**

No preguntar sobre dudas.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Preguntar más. 



**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

Las ponderaciones de las tareas.

- **¿Qué se hizo mal durante el Sprint?**

La falta de tiempo en las realizaciones de los sprints.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Mejor comunicación.

