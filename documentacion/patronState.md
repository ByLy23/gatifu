## Patrón Strategy


## Patrón State
Es un patrón de diseño de comportamiento que permite a un objeto alterar su comportamiento cuando su estado interno cambia. Parece como si el objeto cambiará su clase.
La idea principal es que, en cualquier momento, un programa pueda encontrarse en un número finito de estados. Dentro de cada estado el programa se comporta de forma diferente y puede cambiar de un estado a otro instantáneamente.


## Implementación
Se tiene una clase producto la cual va a contener el estado en el cual se almacenan las decisiones o estados.


![state1](./files/patronState1.png)


En el estado del producto se coloca el estado en el cual se realizará la consulta, en un lado se puede obtener como un cliente y en otro lado puede obtenerse como un cliente. Esto puede verse de la manera en la que, si se crece y se obtienen más estados, fácilmente se puede realizar alguna prueba y crear diferentes estados en vez de redundar en el código.

![state2](./files/patronState2.png)



Al momento de implementarlo, se obtiene únicamente el tipo que sería el estado y se ejecuta la función con la cual se obtiene el resultado dependiendo del tipo.

![state3](./files/patronState3.png)

