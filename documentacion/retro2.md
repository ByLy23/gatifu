## Sprint #2

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|11| Joel|4 |
|7 |Byron|6 |
|5 |Marvin|5 |
|8 |Chris|4 |
|6 |Audrie|5 |
|47|__Total__|24 | 

***
#### WIP por tareas
|Tarea|Ponderación|
| :- | :- |
|post descuento|2|
|eliminar descuento|1|
|delete estudio|1|
|post estudio|2|
|get médico y estado médico|1|
|manejo de imágenes - backend|3|
|manejo de imagenes - frontend|2|
|ver lista de descuentos|1|
|Crear descuento|2|
|Eliminar descuento|1|
|crear procs. de creación de estudios y obtención de estudios|2|
|crear procs. para el manejo de descuentos|1|
|manejo de imagenes - frontend|2|
|ver lista de descuentos|1|
|Crear descuento|1|
|Eliminar descuento|1|
***

#####  Tabla comparativa del Sprint Backlog 

|Elemento|Estado|Justificación|
| :- | :- | :- |
|crear procs. para la actualización de estados en citas|Validation|Se necesita espacio en el frontend y en el backend para validación.|
|crear procs. de creación de estudios y obtención de estudios|Done||
|crear procs. para el manejo de descuentos|Done||
|post descuento|Done||
|eliminar descuento|Done||
|delete estudio|Validation|Se completó, pero no se validó la integración de la parte del backend con el fron|
|post estudio|Validation|Se completó, pero no se validó la integración de la parte del backend con el fron|
|get medico y estado médico|Validation|Se completó, pero no se validó la integración de la parte del backend con el fron|
|manejo de imagenes - backend|Done||
|manejo de imagenes - frontend|Blocked|No se completo el listado de las mascotas para poder cargar la foto|
|ver lista de descuentos|Done||
|Crear descuento|Done||
|Eliminar descuento|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s2_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s2_t2.png)
***

#### Ramas e historias

| Nombre historia                                              | Nombre de la persona | Nombre de la rama                      | Link hacia la rama                                                                          |
| ------------------------------------------------------------ | -------------------- | -------------------------------------- | ------------------------------------------------------------------------------------------- |
| crear procs. de creación de estudios y obtención de estudios | Joel                 | feature/procs-estudios-DB              | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-estudios-DB              |
| crear procs. para el manejo de descuentos                    | Joel                 | feature/procs-descuentos-DB            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-descuentos-DB            |
| manejo de imagenes - backend                                 | Marvin               | feature/manejoImagenes-backend         | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/manejoImagenes-backend              |
| manejo de imagenes - frontend                                | Marvin               | feature/manejoImagen-frontend          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/manejoImagen-frontend               |
| ver lista de descuentos                                      | Marvin               | feature/manejoDescuentos-Frontend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/manejoDescuentos-Frontend           |
| Crear descuento                                              | Marvin               | feature/manejoDescuentos-Frontend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/manejoDescuentos-Frontend           |
| Eliminar descuento                                           | Marvin               | feature/manejoDescuentos-Frontend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/manejoDescuentos-Frontend           |
| post descuento                                               | Audrie               | feature/postdescuento-backend          | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postdescuento-backend          |
| eliminar descuento                                           | Audrie               | feature/eliminarDescuento-backend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/eliminarDescuento-backend      |
| delete estudio                                               | Audrie               | feature/eliminarDescuento-backend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/eliminarDescuento-backend      |
| post estudio                                                 | Audrie               | feature/postestudio-backend            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postestudio-backend            |
| get medico y estado medico                                   | Audrie               | feature/getmedico_estadomedico-backend | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getmedico_estadomedico-backend |
| post raza mascota                                            | Christtopher         | feature/postRaza-backend               | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=freature/raza_paciente%2Dbackend      |
| post mascota                                                 | Christtopher         | freature/postRaza-backend              | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=freature/raza_paciente%2Dbackend      |
| registrar emergencia                                         | Byron                | feature/creacionEmergencia             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/creacionEmergencia             |
| ver datos del paciente                                       | Byron                | feature/moduloSecretaria               | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/moduloSecretaria               |
| visualizar mascota                                           | Byron                | feature/visualizarMascotas             | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/visualizarMascotas             |


***

#### Conclusiones 

**Audrie**
- ___¿Qué se hizo bien durante el Sprint?___

    Se realizaron las tareas seleccionadas en el sprint.

- ___¿Qué se hizo mal durante el Sprint?___

    No se detallaron todas las tareas a realizar listadas.

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Ser más detallistas respecto a cambios durante la lógica o las tareas a realizar durante el sprint.


**Joel**
- ___¿Qué se hizo bien durante el Sprint?___

    Se lograron terminar las tareas esenciales para poder continuar con el avance del proyecto. También la comunicación entre el equipo mejoró, pero todavía quedan algunos aspectos por mejorar.


- ___¿Qué se hizo mal durante el Sprint?___

    Aparentemente la carga del WIP fue muy alta y no se lograron terminar algunas tareas. 


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    El manejo de variables y la notación de las mismas.

**Christtopher**
- ___¿Qué se hizo bien durante el Sprint?___

    Se logró completar funcionalidades para el control de mascotas.

- ___¿Qué se hizo mal durante el Sprint?___

    Los datos de entrada de los endpoints no concordaban con los enviados del lado del front end así como sus respectivas respuestas.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Se debe de seguir más de cerca el documento en el que se expone la manera en que se solicitan y se envían datos por parte del front end.


**Marvin** 

- ___¿Qué se hizo bien durante el Sprint?___

    Se completa todo el módulo del administrador y se realiza la sesión en la aplicación para poder implementar la funcionalidad principal de la realización de citas.


- ___¿Qué se hizo mal durante el Sprint?___

    Considero que este sprint hubo una mejora al siguiente ya que se implementó documentación para cada solicitud al servidor y asi tener una comunicación entre desarrolladores.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    En el desarrollo del servidor, guiarse por lo solicitado de parte de la interfaz.



**Byron**

- ___¿Qué se hizo bien durante el Sprint?___

    Se conectaron las partes restantes del backend con el frontend y se realizaron las nuevas pantallas de los nuevos requerimientos.


- ___¿Qué se hizo mal durante el Sprint?___

    No conectar nuevamente el backend con el frontend debido a que solo se realizaron los diseños pero no las conexiones a la misma.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Se debe entender mejor los requerimientos que el grupo de desarrollo realiza esto para que exista una mejor comunicación entre todos.


