# Diagramas de casos de uso  

## Productos   
![Casos de uso administrador](documentacion/files/CDU05.png)  

## Oferta-Pago  
![Casos de uso clientes](documentacion/files/CDU06.png)  

## Grooming  
![Casos de uso doctores](documentacion/files/CDU07.png)  

## Compras  
![Casos de uso secretarias](documentacion/files/CDU08.png)  