## Sprint #10

***
#### WIP por integrante
|**Suma**|**Integrante**|**Actividades**|
| :- | :- | :- |
|1|Joel|2|
|1|Audie|3|
|1|Marvin|2|
|2|Christopper|5|
|1|Byron|5|
|__6__|__Total__|__17__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Actualización de procs. para el nuevo sistema de puntos|2|
|Pruebas unitarias - admin|2|
|Pruebas unitarias - clientes|2|
|CI/CD|3|
|Pruebas de automatización con Cypress|5|


***
####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Actualización de procs. para el nuevo sistema de puntos|Done||
|Pruebas de automatización con Cypress|Blocked|Falta de tiempo en implementación de pruebas ya que se terminaron de crear las últimas funcionalidades.|
|Pruebas unitarias - clientes|Done||
|Pruebas unitarias - admin|Done||
|CI/CD|Blocked|No se terminó el flujo de CI/CD|



***
### Tablero previo al sprint


![](documentacion/files/s10_t1.png)


### Tablero en la finalización del sprint


![](documentacion/files/s10_t2.png)
***

### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Actualización de procs. para el nuevo sistema de puntos|Joel|updateProcsSisPts-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/updateProcsSisPts-DB>|
|Pruebas unitarias - clientes|Marvin|feature/unit-tests-server-cliente|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/unit-tests-server-cliente>|
|Pruebas de automatizacion con Cypress|Byron|feature/test-automatizacion|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/test-automatizacion>|
|Pruebas unitarias - Administrador|Christtopher|feature/unit-test-admin|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2Funit-tests-admin>|
|Corregir errores test admin y secretaria|Christtopher|Bugfix/2.0|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=bugfix%2F2.0>|
|CI/CD|Audrie/Christopper|feature/test|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/test-automatizacion>|


***

### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se completó la tarea de las pruebas unitarias y se logró corregir los errores de pruebas aprobadas, se comenzó el trabajo de la integración continua.

- **¿Qué se hizo mal durante el Sprint?**

No se logró completar la tarea de la integración continua y se dejó para el próximo sprint.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Se debe dar prioridad al peso de las tareas que están pendientes y terminarlas.



**Marvin:** 

- **¿Qué se hizo bien durante el Sprint?**

Todo Bien.

- **¿Qué se hizo mal durante el Sprint?**

Nada que agregar.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Nada que agregar.


**Joel:**

- **¿Qué se hizo bien durante el Sprint?**

Las tareas más importantes del  backend y el frontend fueron terminadas en el tiempo estipulado.

- **¿Qué se hizo mal durante el Sprint?**

Muchas tareas quedaron bloqueadas ya que no hubo mucho tiempo para realizarlas y el sprint se acortó mucho.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Los tiempos de los sprint deben de ser divididos de manera equitativa ya que el sprint quedó corto al no quedar mucho tiempo disponible.



**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se arreglaron errores que surgieron por modificaciones diferentes partes del código. 

- **¿Qué se hizo mal durante el Sprint?**

Dejamos muy poco tiempo para el resto de sprints.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Tratar de organizarse mejor con los proyectos de las demás clases y este. 


**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

La comunicación del equipo.

- **¿Qué se hizo mal durante el Sprint?**

La duración de los sprints.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

La planificación de las tareas.