## Requerimientos funcionales

- RQF01 - El sistema permitirá al administrador poder crear un nuevo paquete de servicios 
- RQF02 - El sistema permitirá al administrador poder enlistar todos los paquetes existentes
- RQF03 - El sistema permitirá al administrador editar un paquete existente 
- RQF04 - El sistema permitirá al administrador poder agregar un nuevo servicio al paquete 
- RQF05 - El sistema permitirá al administrador poder eliminar un servicio agregado al paquete
- RQF06 - El sistema permitirá al administrador dar de baja a un paquete activo
- RQF07 - El sistema permitirá al administrador colocar un precio al paquete
- RQF08 - El cliente puede visualizar los servicios y paquetes existentes
- RQF09 - El cliente puede seleccionar uno o más servicios para cada mascota
- RQF10 - El cliente puede cancelar la selección de uno o más servicios previos a pagar la contratación de estos.
- RQF11 - El cliente puede seleccionar paquetes para una o más mascotas
- RQF12 - El cliente puede ver el historial de facturación generado por los productos o servicios que ha adquirido.
- RQF13 - El sistema permitirá al administrador poder visualizar todos los productos registrados en farmacia, desde los que se encuentran en stock hasta los eliminados de farmacia.
- RQF14 - El sistema permitirá al administrador gestionar productos, desde su creación, modificación hasta su eliminación de farmacia.
- RQF15 - El sistema permitirá al cliente comprar productos disponibles en farmacia.
- RQF16 - El sistema permitirá al cliente poder visualizar un historial de compras en farmacia desde su perfil
