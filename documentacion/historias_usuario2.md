# Historias de usuario

### Servicios de Grooming
- Como cliente quiero poder visualizar los servicios y paquetes que ofrecen en Gatifu
- Como cliente quiero poder seleccionar uno o más servicios para cada mascota
- Como cliente quiero poder seleccionar paquetes para cada mascota.
- Como cliente quiero poder cancelar la selección de uno o más servicios, previos a pagar la contratación de estos.
- Como cliente quiero poder ver el historial de facturación, de los servicios y productos que he adquirido.

### Farmacia
- Como cliente quiero poder ver los productos de la farmacia en venta
- Como cliente quiero poder seleccionar uno o varios productos de la farmacia para comprar
- Como cliente quiero poder ver los productos que he comprado para llevar un control y saber que marcas y productos he probado
- Como administrador quiero poder agregar nuevos productos al inventario de la farmacia
- Como administrador quiero poder modificar los precios de los productos en la farmacia
- Como administrador quiero poder ver el inventario disponible de la farmacia
- Como administrador quiero poder ver el inventario total de todos los productos que han estado disponibles en la farmacia

### Ofertas y Pagos
- Como administrador quiero personalizar completamente los paquetes activos en el sistema, agregando y eliminando servicios del paquete.
- Como administrador quiero poder personalizar la cantidad del precio al paquete
- Como administrador quiero ver la suma de los precios de los servicios agregados al servicio
- Como administrador quiero poder agregar un nuevo paquete, en listando todos los servicios que se ofrecen
- Como administrador puedo tener todos los paquetes activos del sistema y poder darlos de baja.
- Como cliente poder observar todos los paquetes detalladamente activos en el sistema

