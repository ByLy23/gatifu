## Casos de Uso

|Nombre CDU|CDU 001 Crear un nuevo paquete |
| :- | :- |
|Descripción |El administrador podrá crear un nuevo paquete, agregando servicios y colocando un precio al paquete |
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo administrador</p><p>- Se dirige al apartado de paquetes</p><p>- Se ingresa para crear un nuevo paquete</p><p>- Se coloca un nombre al paquete </p>|
|Excepciones|- El nombre ya existe|



|Nombre CDU|CDU 002 Agregar servicio al paquete|
| :- | :- |
|Descripción |Se agregan servicios al paquete |
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se enlista todos los servicios </p><p>- Se busca los servicios para agregar</p><p>- Se seleccionan todos los servicios por agregar </p><p>- Se confirman todos los servicios seleccionados</p><p>- Se muestra la suma total de los servicios seleccionados</p>|
|Excepciones||



|Nombre CDU|CDU 003 Eliminar servicio al paquete |
| :- | :- |
|Descripción |Se van eliminar servicios del paquete|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se enlista todos los servicios en el paquete </p><p>- Se selecciona el servicio y se elimina del paquete</p>|
|Excepciones|- No se confirma la eliminación|







|Nombre CDU|CDU 004 Precio al paquete|
| :- | :- |
|Descripción |Se coloca un nuevo precio al paquete|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ve la suma total del precio normal de los servicios</p><p>- Se deja a discreción de los administradores para ingresar un precio completo al paquete </p>|
|Excepciones||



|Nombre CDU|CDU 005 Editar el paquete|
| :- | :- |
|Descripción |Se selecciona el paquete para poder editar|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo administrador</p><p>- Se dirige al apartado de paquetes</p><p>- Se selecciona el paquete a editar</p><p>- Se mueve el paquete que se va editar el módulo de edición</p>|
|Excepciones||



|Nombre CDU|CDU 006 Eliminar paquete |
| :- | :- |
|Descripción ||
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo administrador</p><p>- Se dirige al apartado de paquetes</p><p>- Se selecciona el paquete a eliminar</p><p>- Se confirma la eliminación</p>|
|Excepciones||




|Nombre CDU|CDU 007 Listar paquete |
| :- | :- |
|Descripción |Se enlista todos los paquetes activos o inactivos|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo administrador</p><p>- Se dirige al apartado de paquetes</p><p>- Se enlista todos los paquetes</p>|
|Excepciones||






|Nombre CDU|CDU 008 Ver productos en stock|
| :- | :- |
|Descripción |El cliente visualiza todos los productos que se encuentran en stock en la plataforma|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al módulo de farmacia</p><p>- Se enlistan todos los productos disponibles en farmacia</p>|
|Excepciones||




|Nombre CDU|CDU 009 Ver detalle de producto|
| :- | :- |
|Descripción |El cliente visualiza todos los detalles de un producto seleccionado|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente realiza los pasos de CDU 008</p><p>- El cliente selecciona un producto</p><p>- Se muestra toda la información del producto seleccionado</p>|
|Excepciones||








|Nombre CDU|CDU 010 Ver productos comprados|
| :- | :- |
|Descripción |El cliente visualiza la lista de todos los productos comprados en la plataforma.|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa a su perfil</p><p>- El cliente ingresa al apartado de compras realizadas</p><p>- Se muestra la lista de productos adquiridos por el cliente</p>|
|Excepciones||








|Nombre CDU|CDU 011 Agregar productos|
| :- | :- |
|Descripción |El administrador ingresa nuevos productos al sistema.|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- El administrador ingresa al módulo de gestión de productos</p><p>- El administrador ingresa al apartado de creación de productos</p><p>- El administrador ingresa todos los datos solicitados para la creación de un nuevo producto</p><p>- El administrador confirma la creación del nuevo producto (E-1)</p>|
|Excepciones|- E-1 : El administrador cancela la creación del producto y vuelve a realizar los pasos de CDU 011 o sale del módulo de creación de productos|









|Nombre CDU|CDU 012 Modificar productos|
| :- | :- |
|Descripción |El administrador modifica un producto seleccionado.|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- El administrador ingresa al módulo de gestión de productos</p><p>- Se muestra la lista de productos registrador en farmacia</p><p>- El administrador selecciona un producto</p><p>- El administrador selecciona la opción de modificar producto</p><p>- El administrador ingresa los nuevos datos del producto</p><p>- El administrador confirma la modificación del producto (E-1)</p>|
|Excepciones|- E-1: El administrador cancela la modificación del producto y vuelve a realizar los pasos de CDU 12 o sale del módulo de gestión de productos|








|Nombre CDU|CDU 013 Eliminar productos|
| :- | :- |
|Descripción |El administrador elimina productos seleccionados.|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- El administrador ingresa al módulo de gestión de productos</p><p>- Se muestra la lista de productos registrados en farmacia</p><p>- El administrador selecciona un producto</p><p>- El administrador selecciona la opción de eliminar producto</p><p>- El administrador confirma la eliminación (E-1)</p>|
|Excepciones|- E-1: El administrador cancela la eliminación del producto y repite los pasos de CDU 13 o sale del módulo de gestión de productos|






|Nombre CDU|CDU 014 Ver inventario de Farmacia|
| :- | :- |
|Descripción |El administrador visualiza el inventario de productos en farmacia.|
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- El administrador ingresa al módulo de gestión de productos</p><p>- El administrador visualizar la lista de todos los productos registrados en farmacia</p>|
|Excepciones||




|Nombre CDU|CDU 015 Comprar productos|
| :- | :- |
|Descripción |El cliente selecciona y compra productos disponibles en farmacia.|
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- El cliente ingresa al módulo de farmacia</p><p>- El cliente agrega productos a la orden de compra (E-1)</p><p>- El cliente finaliza la orden de compra</p><p>- El cliente confirma la orden de compra (E-2)</p>|
|Excepciones|<p>- E-1: El cliente quita productos de la orden de compra</p><p>- E-2: El cliente cancela la orden de compra y realiza nuevamente los pasos de CDU 015 o sale del módulo de farmacia</p>|



|Nombre CDU|CDU 016 Ver historial facturación |
| :- | :- |
|Descripción |El Cliente podrá ver sus facturas de manera histórica |
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo cliente</p><p>- Se dirige al apartado de historial</p><p>- Se revisa el historial de los paquetes</p>|
|Excepciones|- No tiene ninguna factura realizada.|



|Nombre CDU|CDU 017 Ver lista de servicios y paquetes|
| :- | :- |
|Descripción |El Cliente podrá ver toda la lista de los servicios y paquetes que puede escoger para agregarlo a su compra |
|Actores|Cliente|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo cliente</p><p>- Se dirige al apartado de paquetes</p><p>- Se ingresa para seleccionar un paquete o servicio</p><p>- Lo coloca en una lista de compras</p><p>- Procede a realizar el pago del paquete o servicio</p><p>- El sistema reconoce el tipo del cliente y la forma de su compra y puede realizar algún descuento para el cliente.</p>|
|Excepciones|- El servicio no está disponible|



|Nombre CDU|CDU 018 Agregar descuento|
| :- | :- |
|Descripción |El administrador podrá crear un nuevo paquete, agregando servicios y colocando un precio al paquete |
|Actores|Administrador|
|Secuencia Normal||
|Pasos |<p>- Se ingresa al módulo administrador</p><p>- Se dirige al apartado de paquetes</p><p>- Se ingresa para crear un nuevo paquete</p><p>- Se coloca un nombre al paquete </p>|
|Excepciones|- El nombre ya existe|
