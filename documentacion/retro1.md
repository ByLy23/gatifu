## Sprint #1

***
#### WIP por integrante
|Suma|Integrante|Actividades|
| :- | :- | :- |
|10| Joel|7 |
|5 |Byron|4 |
|7 |Marvin|5 |
|7 |Chris|3 |
|8 |Audrie|4 |
|37|__Total__|23 | 

***
#### WIP por tareas
|Tarea|Ponderación|
| :- | :- |
|crear datos base en BD|4|
|crear procs. para la actualización de datos de cuentas|2|
|crear procs. de creación de cuentas y obtención de datos de cuentas|2|
|crear procs. de creación de mascotas y obtención de datos de mascotas|2|
|crear base de datos con modelo ER|1|
|post descuento|2|
|eliminar descuento|1|
|get persona|1|
|get lista usuarios|1|
|get lista de espera registro|1|
|update estado de usuario|3|
|inicio de sesión|2|
|registro de usuario|2|
|Registrar Usuario|2|
|Cambiar estado usuarios|2|
|ver lista de usuarios|1|
|Seleccionar usuario en lista de espera|2|
|ver lista de espera|1|

***

#####  Tabla comparativa del Sprint Backlog 

|Elemento|Estado|Justificación|
| :- | :- | :- |
|Inicio Sesión Frontend|Bloqueado|No se tiene la conexión con el backend por lo que el estado quedó bloqueado.|
|Registrar Mascotas Frontend|Bloqueado|No está creado el acceso al cliente con el cual se asigna cada mascota|
|Registrar Cliente Frontend |Bloqueado|Falta el registro de las mascotas para visualización por lo que se consideró que el estado está bloqueado|
|Visualizar mascotas|Bloqueado|Las mascotas no pueden ingresarse desde el frontend por lo que no se puede visualizar|
|Visualizar Usuarios Frontend|Validación ||
|Cambiar estado usuario|Validación |` `Se completó, pero no se validó la integración de la parte del backend con el front|
|Registar Usuario|Validación||
|crear datos base en BD|Done||
|crear procs. para la actualización de datos de cuentas|Done||
|crear procs. de creación de mascotas y obtención de datos de mascotas|Done||
|crear procs. de creación de mascotas y obtención de datos de mascotas|Done||
|crear base de datos con modelo ER|Done||
|get lista de espera registro|Validación|Se completó, pero no se validó la integración de la parte del backend con el fron|
|get lista de usuarios|Validación|Se completó, pero no se validó la integración de la parte del backend con el fron|
|get persona|Validación|Se completó, pero todavía no es requerido en el frontend, por lo que no se validó|


***
#### Tablero previo al sprint


![](documentacion/files/s1_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s1_t2.png)
***

#### Ramas e historias

| Nombre historia                                                       | Nombre de la persona | Nombre de la rama                     | Link hacia la rama                                                                                                          |
| --------------------------------------------------------------------- | -------------------- | ------------------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| crear datos base en BD                                                | Joel                 | feature/creacionDB                    | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/creacionDB                                                     |
| crear procs. para la actualización de datos de cuentas                | Joel                 | feature/procs-CRU-cuentas-mascotas-DB | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-CRU-cuentas-mascotas-DB                                  |
| crear procs. de creación de mascotas y obtención de datos de mascotas | Joel                 | feature/procsMascotas-DB              | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procsMascotas-DB                                               |
| crear base de datos con modelo ER                                     | Joel                 | feature/creacionDB                    | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/creacionDB                                                     |
| crear procs. de creación de cuentas y obtención de datos de cuentas   | Joel                 | feature/procs-CRU-cuentas-mascotas-DB | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/procs-CRU-cuentas-mascotas-DB                                  |
| get persona                                                           | Audrie               | feature/getPersona-Backend            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getPersona-Backend                                             |
| get lista usuarios                                                    | Audrie               | feature/getlistausuarios-Backend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getlistausuarios-Backend                                       |
| get lista de espera registro                                          | Audrie               | feature/getlistausuarios-Backend      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getlistausuarios-Backend                                       |
| update estado de usuario                                              | Audrie               | feature/updateestadodeusuario-Backend | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/updateestadodeusuario-Backend                                  |
| Crear Usuario                                                         | Christtopher         | feature/crear-usuario-backend         | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/postdescuento%2Dbackend-,feature/login,-feature/deleteestudio |
| Inicio Sesion                                                         | Christtopher         | freature/login                        | https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/postdescuento%2Dbackend-,feature/login,-feature/deleteestudio |
| Registrar Usuario                                                     | Marvin               | feature/createUser-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/createUser-frontend                                                 |
| Cambiar estado usuarios                                               | Marvin               | feature/createUser-frontend           | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/createUser-frontend                                                 |
| ver lista de usuarios                                                 | Marvin               | feature/listUsers-frontend            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/listUsers-frontend                                                  |
| Seleccionar usuario en lista de espera                                | Marvin               | feature/listUsers-frontend            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/listUsers-frontend                                                  |
| ver lista de espera                                                   | Marvin               | feature/listUsers-frontend            | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/feature/listUsers-frontend                                                  |
| inicio sesion                                                         | Byron                | feature/inicioSesion_Frontend         | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/InicioSesion_Frontend                                          |
| registrar cliente                                                     | Byron                | moduloSecretaria                      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/moduloSecretaria                                               |
| registrar mascota                                                     | Byron                | registra_mascota                      | https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/registra_mascota                                               |

***

#### Conclusiones 

**Audrie**
- ___¿Qué se hizo bien durante el Sprint?___

    Se realizaron las tareas seleccionadas en el sprint. 

- ___¿Qué se hizo mal durante el Sprint?___

    No hubo buena comunicación respecto a lo requerido por el frontend y los procesos brindados por el backend

- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Definir bien los parámetros a manejar, entre front y la base de datos. Dado que al utilizar procedimientos, no coincide con lo que requiere los procedimientos con lo que brinda el front a la hora de realizar la parte del backend.


**Joel**
- ___¿Qué se hizo bien durante el Sprint?___

    Se trabajó siguiendo la documentación generada previamente.


- ___¿Qué se hizo mal durante el Sprint?___

    Se definieron de manera errónea los nombres de variables utilizadas en las rutas del frontend y el backend, por lo que hubo errores en las pruebas por nombres y por formatos de entrega de resultados.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Se debe mejorar la comunicación entre los encargados del front y el back y definir todos los parámetros antes de programar.

    Mejorar la hoja de cálculo en donde se encuentra toda la información de los parámetros, ya que creó confusión.


**Christtopher**
- ___¿Qué se hizo bien durante el Sprint?___

    Se logró completar la funcionalidad de inicio de sesión del backend que es de suma importancia para la seguridad de cada usuario. 

- ___¿Qué se hizo mal durante el Sprint?___

    El tiempo que se tomó para completar las tareas se extendió más de lo calculado.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Realizar un mejor cálculo del tiempo que toman las tareas y cumplirlas para ese tiempo estipulado.


**Marvin** 

- ___¿Qué se hizo bien durante el Sprint?___

    Se realizó todo el manejo de usuarios del lado del administrador, para que se puedan registrar los médicos, las secretarías y así poder  darle continuidad a las siguientes funcionalidades de la aplicación. 



- ___¿Qué se hizo mal durante el Sprint?___

    Falta de comunicación entre los desarrolladores del frontend y backend ya que es necesario definir rutas y el objeto de envío y de respuesta.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Estar más al pendiente de lo realizado y así poder completar la funcionalidad.



**Byron**

- ___¿Qué se hizo bien durante el Sprint?___

    Se completó el diseño de las pantallas del frontend con respecto a los requerimientos.


- ___¿Qué se hizo mal durante el Sprint?___

    Conectar todos los componentes con el lado del backend generó un retraso para el siguiente sprint.


- ___¿Qué mejoras se deben implementar para el próximo sprint?___

    Estimar mejor el tiempo de desarrollo de cada tarea y optimizar los task.