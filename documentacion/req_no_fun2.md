## Requerimientos no funcionales

- RQFN01 - El sistema dará la suma total del precio de los servicios agregados a la paquete

- RQFN02 - El sistema realiza un descuento del 45% sobre el valor total a pagar, en caso que el cliente requiera de un paquete completo

- RQFN03 - El sistema tendrá la capacidad de soportar múltiples transacciones paralelas. 
