## Sprint #5

***
#### WIP por integrante
|**Suma**|**Integrante**|**Actividades**|
| :- | :- | :- |
|13| Joel|4 |
|4 |Byron|1 |
|12 |Marvin|5 |
|10 |Chris|3 |
|9 |Audrie|4 |
|__48__|__Total__|__17__ | 

***
#### WIP por tareas
|**Tarea**|**Ponderación**|
| :-: | :-: |
|Patron 2: Strategy|3|
|Agregar nuevo producto|3|
|Ver todos los servicios con detalle|2|
|Eliminar Producto|1|
|Actualización de BD con nuevas tablas y relaciones|4|
|Creación de procs. para la gestión de productos|3|
|Creación de procs. para la gestión de servicios|3|
|Gestion de productos Admin|3|
|Patron 1: Builder|4|
|Patron 4: Template|3|
|Ver todos los servicios - Frontend|3|
|Agregar nuevo producto - Frontend|2|
|Seleccion producto - Frontend|1|
|En listar productos - Frontend|3|
|Modificar Producto - Frontend|3|
|Patron State - Backend|4|



***

#####  Tabla comparativa del Sprint Backlog 

|**Elemento**|**Estado**|**Justificación**|
| :- | :- | :- |
|Actualización de BD con nuevas tablas y relaciones|Done||
|Creación de procs. para la gestión de productos|Done||
|Creación de procs. para la gestión de servicios|Done||
|Gestión de productos Admin|Done||
|Ver todos los servicios - Frontend|Done||
|Agregar nuevo producto - Frontend|Done||
|Selección producto - Frontend|Done||
|En listar productos - Frontend|Done||
|Modificar Producto - Frontend|Done||
|Eliminar producto - Frontend|Done||
|Patron 2: Strategy  - Backend|Blocked|Se terminó de crear la estructura del patrón de diseño, pero no se agregaron las peticiones, dado que no se estaban aplicando en este sprint. Por lo que no se pudo terminar.|
|Agregar nuevo producto - Backend|Done||
|Ver todos los servicios con detalle  - Backend|Done||
|Eliminar Producto - Backend|Done||


***
#### Tablero previo al sprint


![](documentacion/files/s5_t1.png)


#### Tablero en la finalización del sprint


![](documentacion/files/s5_t2.png)
***

#### Ramas e historias

|**Nombre historia**|**Nombre de la persona**|**Nombre de la rama**|**Link hacia la rama**|
| :-: | :-: | :-: | :-: |
|Patron 2: Strategy|Audrie|feature/strategyPattern-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/strategyPattern-Backend>|
|Agregar nuevo producto|Audrie|feature/postProducto-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/postProducto-Backend>|
|Ver todos los servicios con detalle|Audrie|feature/getServices-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/getServices-Backend>|
|Eliminar Producto|Audrie|feature/EliminarProducto-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/EliminarProducto-Backend>|
|Actualización de BD con nuevas tablas y relaciones|Joel|feature/actualizacionNuevasTablasBD-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/actualizacionNuevasTablasBD-DB>|
|Creación de procs. para la gestión de productos|Joel|feature/gestionProductos-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/gestionProductos-DB>|
|Creación de procs. para la gestión de servicios|Joel|feature/gestionServicios-DB|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/gestionServicios-DB>|
|Gestion de productos Admin|Joel, Christopher|feature/gestion-productos-admin-backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/gestion-productos-admin-backend>|
|Patron 1: Builder|Christtopher|feature/builderPattern-Backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5#:~:text=feature/builderPattern%2DBackend>|
|Patron 4: Template|Christtopher|feature/updateTemplate-pattern-backend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/refs/switch?destination=tree&ref=feature%2FupdateTemplate-pattern-backend>|
|Ver todos los servicios - Frontend|Marvin |feature/listarProductos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/listarProductos-frontend>|
|Agregar nuevo producto - Frontend|Marvin |feature/crudProducto-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudProducto-frontend>|
|Seleccion producto - Frontend|Marvin |feature/listarProductos-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/listarProductos-frontend>|
|En listar productos - Frontend|Marvin |feature/crudProducto-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudProducto-frontend>|
|Modificar Producto - Frontend|Marvin |feature/crudProducto-frontend|<https://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/crudProducto-frontend>|
|Patron State - Backend|Byron|feature/statePattern|[gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/statePattern](http://gitlab.com/jodyannre/ayd2_proyecto_g5/-/tree/feature/statePattern)|



***

#### Conclusiones 


**Christtopher:**

- **¿Qué se hizo bien durante el Sprint?**

Se logró implementar el patrón Builder, y la gestión de productos del backend. Se logro tener mejor comunicación con los compañeros de trabajo.

- **¿Qué se hizo mal durante el Sprint?**

Se quedó bloqueada la tarea de Template Method, debido a la falta de información de cómo implementar clases abstractas en nodejs

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Calcular mejor los tiempos de entrega para tener las funcionalidades a tiempo para la integración con el front end 

**Marvin** 

- **¿Qué se hizo bien durante el Sprint?**

La comunicación se mejoró bastante al dejar bien detallado y documentado  cada interacción entre el front hacia el backend y del backend hacia la base de datos

- **¿Qué se hizo mal durante el Sprint?**

Se presentó una variación desnivelada entre la tareas asignadas para cada integrante del equipo

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Que todas las tareas que se necesitan para completar las funcionalidades se enlisten y a la vez se asignen para el desarrollo y poder completar todas las funcionalidades previstas a trabajar 

**Joel**

- **¿Qué se hizo bien durante el Sprint?**

Mejora en la comunicación entre el equipo del front y el back, de modo que en las pruebas no hubo mayores complicaciones.

- **¿Qué se hizo mal durante el Sprint?**

Se seleccionaron tareas que no podían ser validadas ya que dependen de otras tareas que se quedaron en el backlog.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Solo tomar tareas que puedan ser validadas.


**Audrie:**

- **¿Qué se hizo bien durante el Sprint?**

Se obtuvo una mejor coordinación en el manejo de datos y envío de estos, entre base de datos, backend y frontend.

- **¿Qué se hizo mal durante el Sprint?**

Se realizaron todos los patrones de diseño que pensábamos aplicar, pero no se consideraron realizar todas las tareas a las cuales se debían aplicar.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Considerar bien el orden de las tareas a tomar, para no elegir alguna que requiera de otra tarea que no se vaya a realizar en el sprint.

**Byron:**

- **¿Qué se hizo bien durante el Sprint?**

La comunicación del equipo entre la forma de trabajar en el frontend y backend, así como la realización del mismo.

- **¿Qué se hizo mal durante el Sprint?**

Tomar diferentes tareas que debían completarse pero no se podían ya que estaban en otra lista.

- **¿Qué mejoras se deben implementar para el próximo sprint?**

Tomar diferentes tareas siempre y cuando se puedan terminar por completo en el mismo sprint
