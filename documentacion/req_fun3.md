## Requerimientos funcionales


- RQF01 - El sistema permitirá registrar puntos por adquisición de servicios
- RQF02 - El sistema permitirá el pago de servicios con puntos
- RQF03 - El sistema permitirá el pago de servicios de manera híbrida, es decir con combinaciones de puntos y dinero
- RQF04 - El sistema realizará la conversión de puntos a dinero para el cálculo del total monetario que deberá pagar un cliente
- RQF05 - El sistema permitirá al usuario tener la opción de presentar su código de cliente para aplicar los beneficios del sistema de puntos.

