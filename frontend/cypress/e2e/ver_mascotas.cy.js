describe('Comprar producto', () => {
  it('Se verificara la compra de paquetes', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')

    //COMPRAR PRODUCTO
    cy.get('[data-test-mascotas="mascotas"]').click()
    cy.get('[data-test-mascota="mi_mascota"]').click()
    cy.get('[data-test-mascota-nombre="nombre_mascota"]').contains('Terry')
  })
})
