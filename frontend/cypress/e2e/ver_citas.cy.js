describe('Ver historial Citas', () => {
  it('Se verificara que exista un historial de citas', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')

    //COMPRAR PRODUCTO
    cy.get('[data-test-citas="citas"]').click()
    cy.get('[data-test-cita="lista_citas"]').click()
    cy.get('[data-test-cita="cita_7"]').click()
    cy.get('[data-test-receta="medicamentos"]').contains('Fipronil')
  })
})
