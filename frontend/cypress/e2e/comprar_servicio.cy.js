describe('Comprar servicio', () => {
  it('Se verificara la compra de servicios de grooming', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')

    //COMPRAR PRODUCTO
    cy.get('[data-test-servicios="servicio"]').click()
    cy.get('[data-test-servicios="listar_servicio"]').click()
    cy.get('[data-test-servicios="select_mascota"]').select('Terry')
    cy.get('[data-test-servicios="fecha_cita"]')
      .type('2022-10-18')
      .should('have.value', '2022-10-18')
    cy.get('[data-test-servicio="servicio_servicio1"]').click()
    cy.get('[data-test-servicio="servicio_servicio3"]').click()
    cy.get('[data-test-servicio="btn_comprar"]').click()
    cy.get('[data-test-paquete="btn_confirmar"]').click()
  })
})
