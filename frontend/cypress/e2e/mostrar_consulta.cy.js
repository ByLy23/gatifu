describe('mostrar consultas', () => {
  it('Se verificara la compra de productos', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('medi').should('have.value', 'medi')
    cy.get('[data-test-pass="password"]').type('medi').should('have.value', 'medi')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/doctor')

    //COMPRAR PRODUCTO
    cy.get('[data-test-citas="citas"]').click()
    cy.get('[data-test-cita="historial_cita"]').click()
    cy.get('[data-test-cita-abrir="cita_abrir_2"]').click()
    cy.get('[data-test-producto="btn_comprar"]').contains('Link')
  })
})
