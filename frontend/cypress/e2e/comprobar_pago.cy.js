describe('Comprobar descargas de pago', () => {
  it('Se descarga correctamente el pago', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')
    cy.get('[data-test-pagos="pagos"]').click()
    cy.get('[data-test-factura="mifactura"]').click()
    cy.get('[data-test-pago="btn_detalle_43"]').click()
    cy.get('[data-test-pago="btn_descargar"]').click()
  })
})
