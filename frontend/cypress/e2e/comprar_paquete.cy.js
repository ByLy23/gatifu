describe('Comprar paquete', () => {
  it('Se verificara la compra de paquetes', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')

    //COMPRAR PRODUCTO
    cy.get('[data-test-paquetes="paquete"]').click()
    cy.get('[data-test-paquetes="lista_paquete"]').click()
    cy.get('[data-test-paquetes="select_mascota"]').select('Terry')
    cy.get('[data-test-paquetes="fecha_cita"]')
      .type('2022-10-18')
      .should('have.value', '2022-10-18')
    cy.get('[data-test-paquete="paquete_1"]').click()
    cy.get('[data-test-paquete="btn_comprar"]').click()
    cy.get('[data-test-paquete="btn_confirmar"]').click()
  })
})
