describe('mostrar consultas', () => {
  it('Se verificara la compra de productos', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    // const newUsuario = `test_usuario_${Date.now().toString().slice(-5)}`
    const newMascota = `test_mascota_${Date.now().toString().slice(-5)}`
    cy.get('[data-test-user="email"]').type('se').should('have.value', 'se')
    cy.get('[data-test-pass="password"]').type('se').should('have.value', 'se')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/secretaria')

    cy.get('[data-test-clientes="pagos_emergencias"]').click()
    cy.get('[data-test-cliente="nuevo-pago"]').click()
    cy.get('[data-test-form-emergencia="select_dueno"]').select('test_usuario_11120')
    cy.get('[data-test-pago="select_pago"]').select('Efectivo')
    cy.get('[data-test-pago="btn_pagar"]').click()
    cy.get('[data-test-clientes="pagos_emergencias"]').click()
    cy.get('[data-test-cliente="nuevo-pago"]').click()
    cy.get('[data-test-form-emergencia="select_dueno"]').select('test_usuario_11120')
  })
})
