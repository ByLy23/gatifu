describe('Registrar nueva mascota para cliente de emergencia', async () => {
  it('crea una cita de emergencia!!', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    // const newUsuario = `test_usuario_${Date.now().toString().slice(-5)}`
    const newMascota = `test_mascota_${Date.now().toString().slice(-5)}`
    cy.get('[data-test-user="email"]').type('se').should('have.value', 'se')
    cy.get('[data-test-pass="password"]').type('se').should('have.value', 'se')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/secretaria')
    // cy.get('[data-test-clientes="clientes"]').click()
    // cy.get('[data-test-cliente="nuevo-cliente"]').click()
    // cy.get('[data-test-form-cliente="nombre"]').type(newUsuario).should('have.value', newUsuario)
    // cy.get('[data-test-form-cliente="usuario"]').type(newUsuario).should('have.value', newUsuario)
    // cy.get('[data-test-form-cliente="correo"]')
    //   .type('prueba@prueba.com')
    //   .should('have.value', 'prueba@prueba.com')
    // cy.get('[data-test-form-cliente="edad"]').type('25').should('have.value', '25')
    // cy.get('[data-test-form-cliente="telefono"]').type('51348795').should('have.value', '51348795')
    // cy.get('[data-test-form-cliente="direccion"]')
    //   .type('casa de 3 niveles la velred')
    //   .should('have.value', 'casa de 3 niveles la velred')
    // cy.get('[data-test-form-cliente="password"]').type('admin123').should('have.value', 'admin123')
    // cy.get('[data-test-form-cliente="rpassword"]').type('admin123').should('have.value', 'admin123')
    // cy.get('[data-test-form-cliente="rpassword"]').should(
    //   'have.value',
    //   '[data-test-form-cliente="rpassword"].value'
    // )
    // cy.get('[data-test-form-cliente="btn_tipo"]').select('Emergencia').should('have.value', '2') // <select> element
    // cy.submit()
    // cy.get('[data-test-clientes="btn_crear_cliente"]').click()
    //REGISTRO MASCOTA
    cy.get('[data-test-clientes="mascotas"]').click()
    cy.get('[data-test-cliente="nueva-mascota"]').click()
    cy.get('[data-test-form-mascota="select_dueno"]').select('test_usuario_11120') // <select> element
    cy.get('[data-test-form-mascota="nombre"]').type(newMascota).should('have.value', newMascota)
    cy.get('[data-test-form-mascota="edad"]').type('2').should('have.value', '2')
    cy.get('[data-test-form-mascota="genero"]').type('M').should('have.value', 'M')
    cy.get('[data-test-form-mascota="select_raza"]').select('Tosa') // <select> element
    // cy.submit()
    cy.get('[data-test-clientes="btn_crear_mascota"]').click()

    //CREACION CITA EMERGENCIA
    cy.get('[data-test-clientes="emergencias"]').click()
    cy.get('[data-test-cliente="nueva-emergencia"]').click()
    cy.get('[data-test-form-cliente="emergencia_dueno"]').select('test_usuario_11120') // <select> element
    cy.get('[data-test-form-cliente="emergencia_dueno"]').select('test_usuario_11120') // <select> element

    cy.get('[data-test-cita="select_mascota"]').select(newMascota)
    cy.get('[data-test-cita="fecha_cita"]').type('2022-10-18').should('have.value', '2022-10-18')
    cy.get('[data-test-cita="btn_crear_cita"]').click()
    cy.get('[data-test-motivo="btn_crear_motivo"]').click()
    cy.get('[data-test-motivo="select_motivos"]').select('Oftalmología')
    cy.get('[data-test-motivo="select_medicos"]').select('medi')
    cy.get('[data-test-motivo="select_hora"]').select('17:00:00')
    cy.get('[data-test-motivo="select_salas"]').select('Laboratorio')
    cy.get('[data-test-motivo="agregar_motivo"]').click()
    cy.get('[data-test-cita="terminar_cita"]').click()
    cy.get('[data-test-cita="terminar_cita"]').click()
    // cy.get('[data-test-form-mascota="select_dueno"]').select('prueba') // <select> element
    // cy.get('[data-test-form-mascota="nombre"]').type('animal_prueba').should('have.value', 'animal_prueba')
    // cy.get('[data-test-form-mascota="edad"]').type('2').should('have.value', '2')
    // cy.get('[data-test-form-mascota="genero"]').type('M').should('have.value', 'M')
    // cy.get('[data-test-form-mascota="select_raza"]').select('Tosa') // <select> element
  })
})

// describe('Registrar cliente y aprobarlo como administrador', () => {
//   it('passes', () => {
//     cy.visit('https://example.cypress.io')
//   })
// })
