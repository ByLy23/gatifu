describe('Comprar producto', () => {
  it('Se verificara la compra de productos', () => {
    window.sessionStorage.clear()
    cy.viewport(1440, 900)
    cy.visit('http://127.0.0.1:5173')
    //REGISTRO USUARIO
    cy.get('[data-test-user="email"]').type('po').should('have.value', 'po')
    cy.get('[data-test-pass="password"]').type('po').should('have.value', 'po')
    cy.get('[data-test-btn="btn-sesion"]').click()
    cy.url().should('include', '/client')

    //COMPRAR PRODUCTO
    cy.get('[data-test-productos="producto"]').click()
    cy.get('[data-test-productos="lista_producto"]').click()
    cy.get('[data-test-producto="producto_11"]').click()
    cy.get('[data-test-producto="txtproducto_11"]').type('2').should('have.value', '2')
    cy.get('[data-test-producto="producto_20"]').click()
    cy.get('[data-test-producto="txtproducto_20"]').type('3').should('have.value', '3')
    cy.get('[data-test-producto="btn_comprar"]').click()
    cy.get('[data-test-paquete="btn_confirmar"]').click()
  })
})
