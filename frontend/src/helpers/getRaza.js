import { useEffect, useState } from 'react'

export const getRaza = async () => {
  const [state, setState] = useState({
    data: null,
    isLoading: true,
    hasError: null,
  })
  const options = {
    method: 'GET',
  }
  const getFech = async () => {
    setState({
      ...state,
      isLoading: true,
    })
    const response = await fetch(
      `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/raza/getRazas`,
      options
    )
    const data = await response.json().then((res) => {
      setState({
        res,
        isLoading: false,
        hasError: null,
      })
    })
  }
  useEffect(() => {
    getFech()
  }, [])
  return { state }
}
