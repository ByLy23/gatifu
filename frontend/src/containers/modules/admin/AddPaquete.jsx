import { useState, useEffect } from "react";
import Container from "react-bootstrap/esm/Container";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import Button from "react-bootstrap/Button";
import TableServices from "../../../components/TableServices";
import {
  createPaquete,
  setErrorPaquetes,
} from "../../../features/paquetes/paqueteSlice";
import { useDispatch, useSelector } from "react-redux";
import AlertDismissible from "../../../components/Alert";

export default function AddPaquete({ servicios }) {
  const [listaServicios, setListaServicios] = useState([]);
  const { loadingPaquetes, errorPaquetes } = useSelector(
    (state) => state.paquetes
  );
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState("");
  const [submit, setSubmit] = useState(false);

  const [vaciar, setVaciar] = useState(false);

  const [paquete, setPaquete] = useState({
    nombre: "",
    precio: "",
    descripcion: "",
  });

  const dispatch = useDispatch();

  const handleChange = (e) => {
    setPaquete({
      ...paquete,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(
      createPaquete({
        ...paquete,
        servicios: listaServicios
      })
    );
    setPaquete({
      nombre: "",
      precio: "",
      descripcion: "",
    });
    setVaciar(true);
    setListaServicios([]);
    setSubmit(true);
  };

  useEffect(() => {
    if (!loadingPaquetes && submit) {
      if (errorPaquetes) {
        setShow(true);
        setMessage("Error al crear el paquete");
        setTimeout(() => {
          setShow(false);
        }, 5000);
        dispatch(setErrorPaquetes(false));
      } else {
        setPaquete({
          nombre: "",
          precio: "",
          descripcion: "",
        });
        setVaciar(true);
        setListaServicios([]);
        setSubmit(false);
      }
    }
  }, [loadingPaquetes]);

  return (
    <>
      <Container style={{ marginTop: "1rem", marginBottom: "2rem" }}>
        <h1>Crear Paquete</h1>
        {show && (
          <AlertDismissible onHide={() => setShow(false)} message={message} />
        )}
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Nombre</Form.Label>
            <Form.Control
              type="text"
              placeholder="Nombre"
              name="nombre"
              onChange={handleChange}
              value={paquete.nombre}
            />
          </Form.Group>
          <Form.Group
            controlId="formBasicPassword"
            style={{ marginTop: "10px" }}
          >
            <Form.Label>Descripción</Form.Label>
            <Form.Control
              type="text"
              placeholder="Descripción"
              name="descripcion"
              onChange={handleChange}
              value={paquete.descripcion}
            />
          </Form.Group>
        </Form>
        <div
          style={{ marginTop: "1rem", border: "3px dotted rgb(238,238,237)" }}
        >
          <h3 style={{ marginTop: "5px", marginLeft: "5px" }}>Servicios</h3>
          <TableServices
            products={servicios}
            admin={true}
            setLista={setListaServicios}
            vaciar={vaciar}
            setVaciar={setVaciar}
          />
        </div>
        <div style={{ marginTop: "1rem" }}>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Precio para el paquete</Form.Label>
              <Form.Control
                type="number"
                placeholder="Precio"
                name="precio"
                onChange={handleChange}
                value={paquete.precio}
              />
            </Form.Group>
          </Form>
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "right",
            justifyContent: "right",
          }}
        >
          <Button
            style={{
              backgroundColor: "#20bbcce1",
              borderColor: "#20bbcce1",
              marginTop: "1rem",
              color: "#fff",
            }}
            onClick={handleSubmit}
            disabled={loadingPaquetes}
          >
            {loadingPaquetes ? (
              <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
            ) : (
              "Crear Paquete"
            )}
          </Button>
        </div>
      </Container>
    </>
  );
}
