import { useEffect, useState } from 'react'
import Header from '../../../components/Header'
import Col from 'react-bootstrap/Col'
import Nav from 'react-bootstrap/Nav'
import Row from 'react-bootstrap/Row'
import Tab from 'react-bootstrap/Tab'
import '../../../styles/admin.css'
import FormUser from '../../../components/FormUser'
import TableUser from '../../../components/TableUser'
import FormDiscount from '../../../components/FormDiscount'
import TableDiscount from '../../../components/TableDiscount'
import FormProduct from '../../../components/FormProduct'
import TableProduct from '../../../components/TableProduct'
import TablePaquetes from '../../../components/TablePaquetes'
import AddPaquete from './AddPaquete'
import { useSelector, useDispatch } from 'react-redux'
import {
  fetchMedicos,
  fetchPacientes,
  fetchSecretarias,
  fetchAspirantes,
} from '../../../features/users/usersSlice'
import {
  getMarcas,
  getCategorias,
  getProductos,
  setProducto,
} from '../../../features/productos/productoSlice'
import { fetchServicios } from '../../../features/servicios/servicioSlice'
import { fetchPaquetes } from '../../../features/paquetes/paqueteSlice'
import { fetchDiscounts } from '../../../features/discounts/discountSlice'
import { useContext } from 'react'
import { AuthContext } from '../../../auth/context/AuthContext'

import { setErrorUnauthorized } from '../../../features/error/errorSlice'

export function Admin() {
  const { medicos, pacientes, secretarias, aspirantes } = useSelector((state) => state.users)
  const { discounts } = useSelector((state) => state.discounts)
  const { errorUnauthorized } = useSelector((state) => state.error)
  const { servicios } = useSelector((state) => state.servicios)
  const { marcas, categorias, productos } = useSelector((state) => state.productos)
  const { paquetes } = useSelector((state) => state.paquetes)
  const [manage_medicos, setManage_medicos] = useState(false)
  const [manage_pacientes, setManage_pacientes] = useState(false)
  const [manage_secretarias, setManage_secretarias] = useState(false)
  const [manage_aspirantes, setManage_aspirantes] = useState(false)
  const [manage_descuentos, setManage_descuentos] = useState(false)
  const [manage_productos, setManage_productos] = useState(false)
  const [manage_paquetes, setManage_paquetes] = useState(false)

  const [EditProduct, setEditProduct] = useState(false)

  const [tab_key, setTab_key] = useState('cero')

  const dispatch = useDispatch()

  const { user, logout } = useContext(AuthContext)

  useEffect(() => {
    dispatch(fetchMedicos())
    dispatch(fetchPacientes())
    dispatch(fetchSecretarias())
    dispatch(fetchAspirantes())
    dispatch(fetchDiscounts())

    dispatch(getMarcas())
    dispatch(getCategorias())
    dispatch(getProductos(1))
    dispatch(fetchServicios())
    dispatch(fetchPaquetes(1))
  }, [dispatch])

  useEffect(() => {
    if (errorUnauthorized) {
      logout()
      dispatch(setErrorUnauthorized(false))
    }
  }, [errorUnauthorized])

  return (
    <>
      <Header action="Cerrar Sesion" link="/" color="#EEEEED" />
      <div style={{ marginRight: '2rem' }}>
        <Tab.Container bg="dark" variant="dark" id="left-tabs-example" activeKey={tab_key}>
          <Row>
            <Col sm={3}>
              <div className="BarAdmin" style={{ backgroundColor: '#fff' }}>
                <Nav variant="pills" className="flex-column" style={{ marginLeft: '5px' }}>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if (tab_key === 'first' || (tab_key === 'first-2' && manage_medicos)) {
                          setTab_key('five')
                        }
                        setManage_medicos(!manage_medicos)
                      }}
                    >
                      🧑‍⚕️ Medicos{' '}
                      {manage_medicos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_medicos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first"
                          onClick={() => setTab_key('first')}
                        >
                          🧔 Crear Medico
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first-2"
                          onClick={() => setTab_key('first-2')}
                        >
                          📖 Listar Medicos
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if (
                          tab_key === 'second' ||
                          (tab_key === 'second-2' && manage_secretarias)
                        ) {
                          setTab_key('five')
                        }
                        setManage_secretarias(!manage_secretarias)
                      }}
                    >
                      👩‍💼 Secretarias{' '}
                      {manage_secretarias ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_secretarias ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second"
                          onClick={() => setTab_key('second')}
                        >
                          👩‍🦰 Registrar Secretaria
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second-2"
                          onClick={() => setTab_key('second-2')}
                        >
                          📔 Listar Secretarias
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if (tab_key === 'three' || (tab_key === 'three-2' && manage_pacientes)) {
                          setTab_key('five')
                        }
                        setManage_pacientes(!manage_pacientes)
                      }}
                    >
                      🧑‍🏫 Clientes{' '}
                      {manage_pacientes ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_pacientes ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="three"
                          onClick={() => setTab_key('three')}
                        >
                          🚶🏻 Registrar Cliente
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('three-2')}
                          eventKey="three-2"
                        >
                          🐾 Listar Clientes
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if (tab_key === 'four' && manage_aspirantes) {
                          setTab_key('five')
                        }
                        setManage_aspirantes(!manage_aspirantes)
                      }}
                    >
                      🙋 Aspirantes{' '}
                      {manage_aspirantes ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_aspirantes ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('four')}
                          eventKey="four"
                        >
                          👥 Listar Aspirantes
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if ((tab_key === 'six' || tab_key === 'six-2') && manage_descuentos) {
                          setTab_key('five')
                        }
                        setManage_descuentos(!manage_descuentos)
                      }}
                    >
                      💰 Descuentos{' '}
                      {manage_descuentos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_descuentos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('six')}
                          eventKey="six"
                        >
                          📝 Crear Descuento
                        </Nav.Link>{' '}
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('six-2')}
                          eventKey="six-2"
                        >
                          📑 Listar Descuentos
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if ((tab_key === 'seven' || tab_key === 'seven-2') && manage_productos) {
                          setTab_key('five')
                        }
                        setManage_productos(!manage_productos)
                      }}
                    >
                      🛒 Productos{' '}
                      {manage_productos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_productos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => {
                            setEditProduct(false)
                            setTab_key('seven')
                            setProducto(null)
                          }}
                          eventKey="seven"
                        >
                          📋 Crear Producto
                        </Nav.Link>{' '}
                        {EditProduct && (
                          <Nav.Link
                            style={{ marginLeft: '20px', fontSize: 20 }}
                            onClick={() => setTab_key('seven-3')}
                            eventKey="seven-3"
                          >
                            📝 Editar Producto
                          </Nav.Link>
                        )}
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => {
                            setEditProduct(false)
                            setTab_key('seven-2')
                            setProducto(null)
                          }}
                          eventKey="seven-2"
                        >
                          🗃 Listar Productos
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/********************************************************/}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if ((tab_key === 'eight' || tab_key === 'eight-2') && manage_paquetes) {
                          setTab_key('five')
                        }
                        setManage_paquetes(!manage_paquetes)
                      }}
                    >
                      📦 Paquetes{' '}
                      {manage_paquetes ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_paquetes ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('eight')}
                          eventKey="eight"
                        >
                          🛍 Crear Paquete
                        </Nav.Link>{' '}
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          onClick={() => setTab_key('eight-2')}
                          eventKey="eight-2"
                        >
                          🗄 Listar Paquetes
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                </Nav>
              </div>
            </Col>
            <Col sm={9}>
              <Tab.Content style={{ marginLeft: '-4rem' }}>
                <Tab.Pane eventKey="cero">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>Bienvenido 👋</h1>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="first">
                  <FormUser tipo="medico" />
                </Tab.Pane>
                <Tab.Pane eventKey="first-2">
                  <TableUser tipo="medico" users={medicos} />
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <FormUser tipo="secretaria" />
                </Tab.Pane>
                <Tab.Pane eventKey="second-2">
                  <TableUser tipo="secretaria" users={secretarias} />
                </Tab.Pane>
                <Tab.Pane eventKey="three">
                  <FormUser tipo="Cliente" />
                </Tab.Pane>
                <Tab.Pane eventKey="three-2">
                  <TableUser tipo="Cliente" users={pacientes} />
                </Tab.Pane>
                <Tab.Pane eventKey="four">
                  <TableUser tipo="aspirante" users={aspirantes} />
                </Tab.Pane>
                <Tab.Pane eventKey="five">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>👈 Seleccione una opcion</h1>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="six">
                  <FormDiscount />
                </Tab.Pane>
                <Tab.Pane eventKey="six-2">
                  <TableDiscount discounts={discounts} />
                </Tab.Pane>
                <Tab.Pane eventKey="seven">
                  <FormProduct categorias={categorias} marcas={marcas} boleano={false} />
                </Tab.Pane>
                <Tab.Pane eventKey="seven-2">
                  <TableProduct
                    productos={productos}
                    setEdit={() => setEditProduct(true)}
                    setTab={() => setTab_key('seven-3')}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="seven-3">
                  <FormProduct
                    categorias={categorias}
                    marcas={marcas}
                    setTab={() => setTab_key('seven-2')}
                    boleano={true}
                    SetEdit={() => setEditProduct(false)}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="eight">
                  <AddPaquete servicios={servicios} />
                </Tab.Pane>
                <Tab.Pane eventKey="eight-2">
                  <TablePaquetes paquetes={paquetes} admin={true} />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    </>
  )
}
