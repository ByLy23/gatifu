export const TableItem = ({ body = [], name = '' }) => {
  return (
    <tr>
      {body.map((item, index) => (
        <td name={name} key={index}>
          {item}
        </td>
      ))}
    </tr>
  )
}
