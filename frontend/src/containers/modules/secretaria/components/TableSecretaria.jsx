import { Table } from 'react-bootstrap'
import { TableHeaders } from '../../../../components/TableHeaders'

export const TableSecretaria = ({ headers, body }) => {
  return (
    <Table striped hover>
      <TableHeaders headers={headers} />
      <tbody>
        {body.map(({ id_usuario, nombre_usuario }, index) => (
          <tr key={index}>
            <td>{id_usuario}</td>
            <td>{nombre_usuario}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  )
}
