import { useState } from 'react'
import { Button, Form, Table } from 'react-bootstrap'
import AlertDismissible from '../../../../components/Alert'
import { TableHeaders } from '../../../../components/TableHeaders'
import { useFetch } from '../../../../hooks/useFetch'
import { TableItem } from './TableItem'

export const TablePago = ({ headers = [], body = [] }) => {
  const [Pago, setPago] = useState({
    id_pago: '',
    forma_pago: '',
  })
  const [show, setShow] = useState({
    show: false,
    message: '',
    variant: '',
  })
  const onChangePay = ({ target }) => {
    setPago({
      ...Pago,
      [target.name]: target.value,
    })
  }
  const onPay = ({ target }) => {
    const body = {
      idPago: target.value,
      tipo: Pago.forma_pago,
      estadoPago: 1,
    }

    useFetch('post', `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/pagos/`, body).then((res) => {
      console.log(res)
      if (res.status === 200) {
        setShow({
          show: true,
          message: 'Pago realizado con exito',
          variant: 'success',
        })
        if (!show.show) alert('Pago realizado con exito')
      } else {
        setShow({
          show: true,
          message: 'Error al realizar el pago',
          variant: 'danger',
        })
      }
    })
    window.location.reload()
  }
  return (
    <>
      <Table striped hover>
        <TableHeaders headers={headers} />
        <tbody>
          {body.map((item, index) => {
            const formaPago = (
              <Form.Select
                aria-label="Default select example"
                onChange={onChangePay}
                name="forma_pago"
                data-test-pago="select_pago"
              >
                <option>Seleccione una opción</option>
                <option value="1">Efectivo</option>
                <option value="2">Tarjeta de crédito/débito</option>
              </Form.Select>
            )
            const buttonPay = (
              <Button variant="dark" value={item.id} onClick={onPay} data-test-pago="btn_pagar">
                Pagar
              </Button>
            )
            const newItem = { ...item, formaPago, buttonPay }
            return <TableItem body={Object.values(newItem)} key={index} />
          })}
        </tbody>
      </Table>
      {show.show && (
        <AlertDismissible
          onHide={setShow(!show.show)}
          message={show.message}
          variant={show.variant}
        />
      )}
    </>
  )
}
