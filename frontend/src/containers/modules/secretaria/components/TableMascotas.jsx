import React from "react";
import { Table } from "react-bootstrap";
import { TableHeaders } from "../../../../components/TableHeaders";

export const TableMascotas = ({ headers, body, razas = [] }) => {
  return (
    <Table striped hover>
      <TableHeaders headers={headers} />
      <tbody>
        {body.map(
          (
            { nombre: nombre_mascota, edad, foto, genero, id_raza: first },
            index
          ) => {
            return (
              <tr key={index}>
                <td>{nombre_mascota}</td>
                <td>{edad}</td>
                <td>{foto}</td>
                <td>{genero === 1 ? "Masculino" : "Femenino"}</td>
                <td>
                  {razas.length > 0
                    ? razas.find(({ id_raza }) => id_raza === first).nombre
                    : ""}
                </td>
              </tr>
            );
          }
        )}
      </tbody>
    </Table>
  );
};
