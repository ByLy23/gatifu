import { useEffect, useState, useMemo, useContext } from 'react'
import Header from '../../../components/Header'
import Col from 'react-bootstrap/Col'
import Nav from 'react-bootstrap/Nav'
import Row from 'react-bootstrap/Row'
import Tab from 'react-bootstrap/Tab'
import '../../../styles/admin.css'
import Form from 'react-bootstrap/Form'
import sha256 from 'crypto-js/sha256'

import FormUser from '../../../components/FormUser'
import { useForm } from '../../../hooks/useForm'
import { Button, Spinner } from 'react-bootstrap'
import { useFetch } from '../../../hooks/useFetch'
import AlertDismissible from '../../../components/Alert'
import { TableSecretaria } from './components/TableSecretaria'
import { TableMascotas } from './components/TableMascotas'
import { fetchCitas, fetchHistoryCitas } from '../../../features/citas/citaSlice'
import { fetchMedicos } from '../../../features/users/usersSlice'
import { fetchMascotas } from '../../../features/mascotas/mascotaSlice'
import { fetchSalas } from '../../../features/salas/salaSlice'
import { fetchOcupados } from '../../../features/salas/ocupados'
import { useDispatch, useSelector } from 'react-redux'
import { AuthContext } from '../../../auth/context/AuthContext'
import CreateCita from '../client/cita/create/CreateCita'
import { TablePago } from './components/TablePago'
import { setErrorUnauthorized } from '../../../features/error/errorSlice'

const { id_usuario: idUsuario } = JSON.parse(sessionStorage.getItem('user')) || {}
const headersCliente = ['ID', 'Usuario']
const headersMascota = ['Nombre', 'Edad', 'Foto', 'Género', 'Raza']
const headersPago = [
  'ID Pago',
  'Costo $USD',
  'Codigo cita',
  'Estado',
  'Fecha',
  'Mascota',
  'Forma Pago',
  'Pagar',
]

export function DashboardSecretaria() {
  const dispatch = useDispatch()
  const [manage_mascotas, setManageMascotas] = useState(false)
  const { errorUnauthorized } = useSelector((state) => state.error)
  const [manage_clientes, setManageClientes] = useState(false)
  const [manage_pagos, setManagePagos] = useState(false)
  const { user, logout } = useContext(AuthContext)
  const [manage_citas, setManageCitas] = useState(false)
  const [showCita, setShowCita] = useState(false)
  const [loading, setLoading] = useState(false)
  const [newUser, setNewUser] = useState(false)
  const [showAlert, setShowAlert] = useState({
    message: '',
    show: false,
    variant: 'danger',
  })
  const {
    form: emergencyForm,
    onInputChange: emergencyChange,
    onResetForm: emergencyReset,
  } = useForm({
    nombre_usuario: '',
    correo_usuario: '',
    direccion_usuario: '',
    telefono_usuario: '',
    edad_usuario: '',
    nombre_mascota: '',
    edad_mascota: '',
    genero_mascota: '',
    raza_mascota: '',
    hora_cita: '',
    fecha_cita: '',
    sala_cita: '',
    motivo_cita: '',
    medico_cita: '',
    username_usuario: '',
    password_usuario: '',
  })
  const {
    form: clientForm,
    onInputChange: clientChange,
    onResetForm: clientReset,
  } = useForm({
    tipo: '3',
    nombre: '',
    usuario: '',
    correo: '',
    edad: '',
    direccion: '',
    telefono: '',
    password: '',
    password_repeat: '',
    especialidad: 0,
    horario: '',
  })
  const {
    form: petForm,
    onInputChange: petChange,
    onResetForm: petReset,
  } = useForm({
    nombre_mascota: '',
    raza_mascota: 0,
    edad_mascota: '',
    genero_mascota: '',
    usuario_mascota: 0,
  })

  const { citas, historyCitas, loadingHistorial, loadingPendientes } = useSelector(
    (state) => state.citas
  )

  const { mascotas } = useSelector((state) => state.mascotas)
  const { medicos } = useSelector((state) => state.users)
  const { salas, ocupados } = useSelector((state) => state.salas)

  const [tipoUsuario, setTipoUsuario] = useState(0)

  const [tab_key, setTab_key] = useState('cero')
  const [users, setUsers] = useState({ data: [], loading: true })
  const [razas, setRazas] = useState({ data: [], loading: true })
  const [pagoPendiente, setPagoPendiente] = useState({
    data: [],
    loading: false,
  })
  const [idUser, setIdUser] = useState(0)
  const [mascotas_, setMascotas] = useState({ data: [], loading: true })
  const [misMascotas, setMisMascotas] = useState({ data: [], loading: false })

  useEffect(() => {
    useFetch('get', `${import.meta.env.VITE_SERVICE_SESION}/api/user/getUsers`).then((res) => {
      setUsers({ data: res, loading: false })
    })
    useFetch('get', `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/raza/getRazas`).then((res) => {
      setRazas({ data: res, loading: false })
    })
    useFetch('get', `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/mascotas/mascota/all`).then(
      (res) => {
        setMascotas({ data: res, loading: false })
      }
    )
  }, [])

  useEffect(() => {
    if (errorUnauthorized) {
      logout()
      dispatch(setErrorUnauthorized(false))
    }
  }, [errorUnauthorized])

  useEffect(() => {
    dispatch(fetchMedicos())
    dispatch(fetchSalas())
    dispatch(fetchOcupados())
  }, [])

  useEffect(() => {
    dispatch(fetchMascotas(idUser))
  }, [idUser])
  //   const upload = async (e) => {
  //     e.preventDefault()
  //     const formData = new FormData()
  //     formData.append('file', e.target.files[0])
  //     const res = await axios.post(
  //       `${import.meta.env.VITE_SERVICE_CLIENT}/api/pet/uploadImage/1`,
  //       formData,
  //       {}
  //     )
  //   }

  const verificarSalas = () => {
    setLoading(true)
    const {
      username_usuario,
      password_usuario,
      nombre_usuario,
      correo_usuario,
      direccion_usuario,
      telefono_usuario,
      nombre_mascota,
      edad_mascota,
      edad_usuario,
      genero_mascota,
      raza_mascota,
    } = emergencyForm
    //TODO: cambiar el usuario y passowrd
    const newPassword = sha256(password_usuario).toString()
    const newUser = {
      nombre: nombre_usuario,
      usuario: username_usuario,
      correo: correo_usuario,
      edad: edad_usuario,
      direccion: direccion_usuario,
      telefono: telefono_usuario,
      password: newPassword,
      password2: newPassword,
      horario: '0',
      especialidad: '0',
      tipo: 4,
    }
    //TODO: cambiar endpoint a /api/user/createUser para que se cree el usuario especifico
    useFetch(
      'post',
      `${import.meta.env.VITE_SERVICE_SESION}/api/user/userEmergencia`,
      newUser
    ).then(({ res }) => {
      if (res === null) {
        setShowAlert({
          message: 'Error al crear usuario',
          show: true,
          variant: 'danger',
        })
        setLoading(false)
        return
      }
      if (res[0][0] !== 2) {
        //obtener al usuario
        const idUser = res[1][0].id_usuario
        const newPet = {
          usuario_id: idUser,
          nombre: nombre_mascota,
          edad: edad_mascota,
          genero: genero_mascota === 'M' ? 1 : 2,
          raza: raza_mascota,
        }
        useFetch(
          'post',
          `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/raza/postMascota`,
          newPet
        ).then(({ res }) => {
          if (res.resultado === 1) {
            console.log(`Usuario ${username_usuario} Contra: ${password_usuario}`)
            // setShowAlert({
            //   message: `USUARIO: ${username} PASSWORD: ${password}`,
            //   show: true,
            //   variant: 'success',
            // })
            //enviar info por correo a usuario
            // const email = {
            //   to: correo_usuario,
            //   subject: `Cita emergencia de ${nombre_mascota}`,
            //   text: `Hola ${nombre_usuario}, su cita de emergencia ha sido agendada con éxito. Su usuario es ${username} y su contraseña es ${password}.`,
            // }
          } else {
            setShowAlert({
              message: `${res.mensaje !== undefined ? res.mensaje : 'Error al crear la mascota'}`,
              show: true,
              variant: 'danger',
            })
          }
        })

        setLoading(false)
        setShowCita(false)
      } else {
        setShowAlert({
          message: `El usuario ${username} ya está registrado`,
          show: true,
          variant: 'danger',
        })
        setLoading(false)
      }
    })
  }

  // const changeMascota = ({ target }) => {
  //   document.getElementById('mascota_id').value = target.value
  //   if (target.value !== '') return setShowCita(false)
  //   else setShowCita(true)
  // }

  const selectMascotas = ({ target }) => {
    document.getElementById('usuario_mascota_id').value = target.value
    useFetch('get', `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/mascotas/${target.value}`).then(
      (res) => {
        if (res.length < 1) return setMisMascotas({ data: [], loading: false })
        setIdUser(target.value)
        setMisMascotas({ data: res, loading: true })
      }
    )
  }

  const comprobarPago = ({ target }) => {
    document.getElementById('dueno_id').value = target.value
    useFetch('get', `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/pagos/${target.value}/1`).then(
      (res) => {
        if (res.length < 1) return setPagoPendiente({ data: [], loading: false })
        setIdUser(target.value)
        setPagoPendiente({ data: res, loading: true })
      }
    )
    //useFetch
  }
  // useEffect(() => {
  //   if (misMascotas.data.length < 1) setMisMascotas((state) => ({ ...state, loading: false }))
  // }, [misMascotas.data])

  const handleCreatePet = () => {
    setLoading(!loading)
    const newPet = {
      usuario_id: petForm.usuario_mascota,
      nombre: petForm.nombre_mascota,
      edad: petForm.edad_mascota,
      genero: petForm.genero_mascota === 'M' ? 1 : 2,
      raza: petForm.raza_mascota,
    }
    useFetch(
      'post',
      `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/raza/postMascota`,
      newPet
    ).then(({ res }) => {
      console.log({ res })
      if (res.resultado === 1) {
        // if (response.status === 200) {
        setShowAlert({
          message: 'Mascota creado correctamente',
          show: true,
          variant: 'success',
        })
        clientReset()
      } else {
        setShowAlert({
          message: `${res.mensaje !== undefined ? res.mensaje : 'Error al crear la mascota'}`,
          show: true,
          variant: 'danger',
        })
      }
      petReset()
      setLoading(false)
    })
  }
  const onSubmit = () => {
    setLoading(!loading)
    console.log(tipoUsuario)
    if (tipoUsuario === 0) {
      alert('Seleccione un tipo de usuario')
      setLoading(false)
      return
    }
    if (clientForm.password !== clientForm.password_repeat) {
      alert('Las contraseñas no coinciden')
      setLoading(false)
      return
    }
    const { password, password_repeat } = {
      password: sha256(clientForm.password).toString(),
      password_repeat: sha256(clientForm.password_repeat).toString(),
    }
    const newUser = {
      nombre: clientForm.nombre,
      usuario: clientForm.usuario,
      correo: clientForm.correo,
      edad: clientForm.edad,
      direccion: clientForm.direccion,
      telefono: clientForm.telefono,
      password: password,
      password2: password_repeat,
      horario: '0',
      especialidad: '0',
      tipo: 4,
    }
    useFetch(
      'post',
      tipoUsuario == 1
        ? `${import.meta.env.VITE_SERVICE_SESION}/api/user/`
        : `${import.meta.env.VITE_SERVICE_SESION}/api/user/userEmergencia`,
      newUser
    ).then(({ res }) => {
      console.log(res)
      if (res === null) {
        setShowAlert({
          message: 'Error al crear usuario',
          show: true,
          variant: 'danger',
        })
        setLoading(false)
        return
      }
      if (res[0][0].resultado !== 2) {
        setShowAlert({
          message: 'Usuario creado correctamente',
          show: true,
          variant: 'success',
        })
        clientReset()
      } else {
        setShowAlert({
          message: 'Error al crear usuario',
          show: true,
          variant: 'danger',
        })
      }
      setLoading(false)
    })
  }
  return (
    <>
      <Header action="Cerrar Sesion" link="/" color="#FEEEFF" />
      <div style={{ marginRight: '2rem' }}>
        <Tab.Container bg="dark" variant="dark" id="left-tabs-example" activeKey={tab_key}>
          <Row>
            <Col sm={3}>
              <div className="BarSecre" style={{ backgroundColor: '#fff' }}>
                <Nav variant="pills" className="flex-column" style={{ marginLeft: '5px' }}>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      data-test-clientes="clientes"
                      style={{ fontSize: 22 }}
                      onClick={() => {
                        if (tab_key === 'first' || (tab_key === 'first-2' && manage_clientes)) {
                          setTab_key('five')
                        }
                        setManageClientes(!manage_clientes)
                      }}
                    >
                      👨 Clientes{' '}
                      {manage_clientes ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_clientes ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first"
                          onClick={() => setTab_key('first')}
                          data-test-cliente="nuevo-cliente"
                        >
                          👩‍🦲 Crear Cliente
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first-2"
                          onClick={() => setTab_key('first-2')}
                        >
                          📃 Listar Clientes
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-clientes="mascotas"
                      onClick={() => {
                        if (tab_key === 'second' || (tab_key === 'second-2' && manage_mascotas)) {
                          setTab_key('five')
                        }
                        setManageMascotas(!manage_mascotas)
                      }}
                    >
                      🐶 Pacientes{' '}
                      {manage_mascotas ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_mascotas ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second"
                          data-test-cliente="nueva-mascota"
                          onClick={() => setTab_key('second')}
                        >
                          🧸 Crear Mascotas
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second-2"
                          onClick={() => setTab_key('second-2')}
                        >
                          🐾 Ver Mascotas
                        </Nav.Link>{' '}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-clientes="emergencias"
                      onClick={() => {
                        if (
                          tab_key === 'third' ||
                          (tab_key === 'third-2' && manage_citas) ||
                          (tab_key === 'third-3' && manage_citas)
                        ) {
                          setTab_key('five')
                        }
                        setManageCitas(!manage_citas)
                      }}
                    >
                      📁 Citas{' '}
                      {manage_citas ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_citas ? (
                      <>
                        {/* <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="third"
                          onClick={() => setTab_key("third")}
                        >
                          📆 Crear Citas
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: "20px", fontSize: 20 }}
                          eventKey="third-2"
                          onClick={() => setTab_key("third-2")}
                        >
                          📒 Ver Citas
                        </Nav.Link> */}
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="third-3"
                          data-test-cliente="nueva-emergencia"
                          onClick={() => setTab_key('third-3')}
                        >
                          🚨 Cita Emergencia
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-clientes="pagos_emergencias"
                      onClick={() => {
                        if (
                          tab_key === 'third' ||
                          (tab_key === 'third-2' && manage_pagos) ||
                          (tab_key === 'third-3' && manage_pagos) ||
                          (tab_key === 'third-4' && manage_pagos)
                        ) {
                          setTab_key('five')
                        }
                        setManagePagos(!manage_pagos)
                      }}
                    >
                      📠 Pagos{' '}
                      {manage_pagos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_pagos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="third-4"
                          data-test-cliente="nuevo-pago"
                          onClick={() => setTab_key('third-4')}
                        >
                          💰 Realizar Pago
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                </Nav>
              </div>
            </Col>
            <Col sm={9}>
              <Tab.Content style={{ marginLeft: '-4rem' }}>
                <Tab.Pane eventKey="cero">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>Bienvenido 👋</h1>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="first">
                  <Form.Group>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control
                          type="text"
                          name="nombre"
                          className="form-control"
                          placeholder="Ingresa tu nombre"
                          data-test-form-cliente="nombre"
                          value={clientForm.nombre}
                          onChange={clientChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Nombre de Usuario</Form.Label>
                        <Form.Control
                          type="text"
                          name="usuario"
                          className="form-control"
                          placeholder="Ingresa tu nombre de usuario"
                          data-test-form-cliente="usuario"
                          value={clientForm.usuario}
                          onChange={clientChange}
                        />
                      </Col>
                    </Row>

                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Correo</Form.Label>
                        <Form.Control
                          type="text"
                          name="correo"
                          className="form-control"
                          placeholder="Ingresar Correo"
                          data-test-form-cliente="correo"
                          value={clientForm.correo}
                          onChange={clientChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Edad</Form.Label>
                        <Form.Control
                          type="number"
                          name="edad"
                          className="form-control"
                          placeholder="Edad"
                          data-test-form-cliente="edad"
                          value={clientForm.edad}
                          onChange={clientChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Telefono</Form.Label>
                        <Form.Control
                          type="phone"
                          name="telefono"
                          className="form-control"
                          placeholder="Telefono"
                          data-test-form-cliente="telefono"
                          value={clientForm.telefono}
                          onChange={clientChange}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Direccion</Form.Label>
                        <Form.Control
                          type="text"
                          name="direccion"
                          className="form-control"
                          placeholder="Direccion completa"
                          data-test-form-cliente="direccion"
                          value={clientForm.direccion}
                          onChange={clientChange}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Password"
                          data-test-form-cliente="password"
                          value={clientForm.password}
                          onChange={clientChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Repetir Password</Form.Label>
                        <Form.Control
                          type="password"
                          name="password_repeat"
                          className="form-control"
                          placeholder="Repetir Password"
                          data-test-form-cliente="rpassword"
                          value={clientForm.password_repeat}
                          onChange={clientChange}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Tipo de cliente</Form.Label>
                        <Form.Control
                          as="select"
                          name="tipo"
                          className="form-control"
                          data-test-form-cliente="btn_tipo"
                          onChange={(e) => {
                            setTipoUsuario(e.target.value)
                          }}
                        >
                          <option value="0">Selecciona un tipo de cliente</option>
                          <option value="1">Normal</option>
                          <option value="2">Emergencia</option>
                        </Form.Control>
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Button
                          style={{
                            backgroundColor: '#20bbcce1',
                            borderColor: '#20bbcce1 ',
                          }}
                          size="lg"
                          className="mt-3"
                          data-test-clientes="btn_crear_cliente"
                          onClick={onSubmit}
                        >
                          {loading ? (
                            <Spinner animation="border" variant="light" />
                          ) : (
                            `Crear Cliente`
                          )}
                        </Button>
                        {showAlert.show && (
                          <AlertDismissible
                            onHide={() => setShowAlert(!showAlert.show)}
                            message={showAlert.message}
                            variant={showAlert.variant}
                          />
                        )}
                      </Col>
                    </Row>
                  </Form.Group>
                  {/* <Form.Group
                    controlId="formFile"
                    className="mb-3"
                    style={{ marginTop: '1rem', marginLeft: '1rem' }}
                  >
                    <Form.Label>Actualizar foto mascota</Form.Label>
                    <Form.Control onChange={upload} type="file" size="sm" />
                  </Form.Group> */}
                </Tab.Pane>
                <Tab.Pane eventKey="first-2">
                  <Form.Group>
                    <Row className="mt-3">
                      {!users.loading && (
                        <TableSecretaria headers={headersCliente} body={users.data} />
                      )}
                    </Row>
                  </Form.Group>
                </Tab.Pane>

                <Tab.Pane eventKey="five">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>👈 Seleccione una opcion</h1>
                  </div>
                </Tab.Pane>

                <Tab.Pane eventKey="second">
                  <Form.Group>
                    <Row className="mt-3">
                      {/*Formulario con los siguientes datos:
                      un select con todos los usuarios, un input para nombre, edad, genero y raza */}
                      <Col>
                        <Form.Label>Dueno de mascota</Form.Label>
                        <Form.Control
                          as="select"
                          name="usuario_mascota"
                          className="form-control"
                          data-test-form-mascota="select_dueno"
                          onChange={petChange}
                        >
                          <option value="">Seleccionar</option>
                          {!users.loading &&
                            users.data
                              .filter((item) => item.id_t_us === 4)
                              .map((user) => (
                                <option key={user.id_usuario} value={user.id_usuario}>
                                  {user.nombre_usuario}
                                </option>
                              ))}
                        </Form.Control>
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Nombre Mascota</Form.Label>
                        <Form.Control
                          type="text"
                          name="nombre_mascota"
                          className="form-control"
                          data-test-form-mascota="nombre"
                          placeholder="Ingresa el nombre de la mascota"
                          value={petForm.nombre_mascota}
                          onChange={petChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Edad mascota</Form.Label>
                        <Form.Control
                          type="number"
                          name="edad_mascota"
                          className="form-control"
                          placeholder="Ingresa edad de la mascota"
                          data-test-form-mascota="edad"
                          value={petForm.edad_mascota}
                          onChange={petChange}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Genero Mascota</Form.Label>
                        <Form.Control
                          type="text"
                          name="genero_mascota"
                          className="form-control"
                          placeholder="M/F"
                          data-test-form-mascota="genero"
                          value={petForm.genero_mascota}
                          onChange={petChange}
                        />
                      </Col>
                      <Col>
                        <Form.Label>Raza Mascota</Form.Label>
                        <Form.Control
                          as="select"
                          name="raza_mascota"
                          className="form-control"
                          data-test-form-mascota="select_raza"
                          value={petForm.raza_mascota}
                          onChange={petChange}
                        >
                          <option value="">Seleccionar</option>
                          {!razas.loading &&
                            razas.data.map(({ id_raza, nombre }) => (
                              <option key={id_raza} value={id_raza}>
                                {nombre}
                              </option>
                            ))}
                        </Form.Control>
                      </Col>
                    </Row>
                    <Row className="mt-3">
                      <Col>
                        <Button
                          style={{
                            backgroundColor: '#20bbcce1',
                            borderColor: '#20bbcce1 ',
                          }}
                          size="lg"
                          className="mt-3"
                          data-test-clientes="btn_crear_mascota"
                          onClick={handleCreatePet}
                        >
                          {loading ? (
                            <Spinner animation="border" variant="light" />
                          ) : (
                            `Crear Mascota`
                          )}
                        </Button>
                        {showAlert.show && (
                          <AlertDismissible
                            onHide={() => setShowAlert(!showAlert.show)}
                            message={showAlert.message}
                            variant={showAlert.variant}
                          />
                        )}
                      </Col>
                    </Row>
                  </Form.Group>

                  {/* <Form.Group
                    controlId="formFile"
                    className="mb-3"
                    style={{ marginTop: '1rem', marginLeft: '1rem' }}
                  >
                    <Form.Label>Actualizar foto mascota</Form.Label>
                    <Form.Control onChange={upload} type="file" size="sm" />
                  </Form.Group> */}
                </Tab.Pane>
                <Tab.Pane eventKey="second-2">
                  <Form.Group>
                    <Row className="mt-3">
                      {!(mascotas_.loading && razas.loading) && (
                        <TableMascotas
                          headers={headersMascota}
                          body={mascotas_.data}
                          razas={razas.data}
                        />
                      )}
                    </Row>
                  </Form.Group>
                </Tab.Pane>
                <Tab.Pane eventKey="third-3">
                  {/*Creacion de formulario de emergencia
                  nombre_usuario: '',
    correo_usuario: '',
    direccion_usuario: '',
    telefono_usuario: '',
    nombre_mascota: '',
    edad_mascota: '',
    genero_mascota: '',
    raza_mascota: '',
    hora_cita: '',
    fecha_cita: '',
    sala_cita: '',
    motivo_cita: '',
    medico_cita: '',
                  datos a ingresar, nombre del usuario, correo del usuario, edad (generar usuario y contrasena), telefno de contacto, direccion,
                  nombre de su mascota,edad mascota, genero mascota, raza mascota 
                  ver disponibilidad de medicos y sala
                  despues de ver esas opciones se guarda la informacion y se envia un correo al usuario con la informacion de la emergencia
                  y su usuario y contrasena para que pueda ingresar a la plataforma y ver su informacion
                  */}

                  <Form.Group>
                    {/*<Button
                      style={{
                        backgroundColor: "#20bbcce1",
                        borderColor: "#20bbcce1 ",
                      }}
                      size="lg"
                      className="mt-3"
                      onClick={() => setNewUser(!newUser)}
                    >
                      {!newUser ? "Nuevo Usuario" : "Usuario Antiguo"}
                    </Button>*/}
                    {/*newUser ? (
                      <>
                        <Row className="mt-3">
                          <Col>
                            <Form.Label>Nombre Usuario</Form.Label>
                            <Form.Control
                              type="text"
                              name="nombre_usuario"
                              className="form-control"
                              placeholder="Ingresa el nombre del usuario"
                              value={emergencyForm.nombre_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                          <Col>
                            <Form.Label>Correo Usuario</Form.Label>
                            <Form.Control
                              type="email"
                              name="correo_usuario"
                              className="form-control"
                              placeholder="Ingresa el correo del usuario"
                              value={emergencyForm.correo_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                              type="text"
                              name="username_usuario"
                              className="form-control"
                              placeholder="Ingresa el nombre de usuario"
                              value={emergencyForm.username_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                          <Col>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                              type="email"
                              name="password_usuario"
                              className="form-control"
                              placeholder="Ingresa la contrasena del usuario"
                              value={emergencyForm.password_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <Form.Label>Direccion Usuario</Form.Label>
                            <Form.Control
                              type="text"
                              name="direccion_usuario"
                              className="form-control"
                              placeholder="Ingresa la direccion del usuario"
                              value={emergencyForm.direccion_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                          <Col>
                            <Form.Label>Telefono Usuario</Form.Label>
                            <Form.Control
                              type="number"
                              name="telefono_usuario"
                              className="form-control"
                              placeholder="Ingresa el telefono del usuario"
                              value={emergencyForm.telefono_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                          <Col>
                            <Form.Label>Edad Usuario</Form.Label>
                            <Form.Control
                              type="number"
                              name="edad_usuario"
                              className="form-control"
                              placeholder="Ingresa la edad del usuario"
                              value={emergencyForm.edad_usuario}
                              onChange={emergencyChange}
                            />
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <Form.Label>Nombre Mascota</Form.Label>
                            <Form.Control
                              type="text"
                              name="nombre_mascota"
                              className="form-control"
                              placeholder="Ingresa el nombre de la mascota"
                              value={emergencyForm.nombre_mascota}
                              onChange={emergencyChange}
                            />
                          </Col>
                          <Col>
                            <Form.Label>Edad Mascota</Form.Label>
                            <Form.Control
                              type="number"
                              name="edad_mascota"
                              className="form-control"
                              placeholder="Ingresa la edad de la mascota"
                              value={emergencyForm.edad_mascota}
                              onChange={emergencyChange}
                            />
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <Form.Label>Genero Mascota</Form.Label>
                            <Form.Control
                              as="select"
                              name="genero_mascota"
                              className="form-control"
                              placeholder="Ingresa el genero de la mascota"
                              value={emergencyForm.genero_mascota}
                              onChange={emergencyChange}
                            >
                              <option value="">Selecciona una opcion</option>
                              <option value="M">Macho</option>
                              <option value="H">Hembra</option>
                            </Form.Control>
                          </Col>
                          <Col>
                            <Form.Label>Raza Mascota</Form.Label>
                            <Form.Control
                              as="select"
                              name="raza_mascota"
                              className="form-control"
                              placeholder="Ingresa la raza de la mascota"
                              value={emergencyForm.raza_mascota}
                              onChange={emergencyChange}
                            >
                              <option value="">Selecciona una opcion</option>
                              {!razas.loading &&
                                razas.data.map((raza, index) => (
                                  <option
                                    key={raza.id_raza}
                                    value={raza.id_raza}
                                  >
                                    {raza.nombre}
                                  </option>
                                ))}
                            </Form.Control>
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <Button
                              variant="primary"
                              type="button"
                              onClick={verificarSalas}
                            >
                              {loading ? (
                                <Spinner animation="border" variant="light" />
                              ) : (
                                `Ingresar informacion`
                              )}
                            </Button>
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            {!showCita && (
                              <CreateCita
                                mascotas={mascotas}
                                medicos={medicos}
                                ocupados={ocupados}
                                salas={salas}
                                tipoCita={2}
                                id={1}
                              />
                            )}
                          </Col>
                        </Row>
                      </>
                    ) : (*/}
                    <>
                      <Row className="mt-3">
                        {/*Formulario con los siguientes datos:
                      un select con todos los usuarios, un input para nombre, edad, genero y raza */}
                        <Col>
                          <Form.Label>Dueno de mascota</Form.Label>
                          <Form.Control
                            as="select"
                            id="usuario_mascota_id"
                            name="usuario_mascota_id"
                            className="form-control"
                            data-test-form-cliente="emergencia_dueno"
                            value={emergencyForm.usuario_mascota}
                            onChange={selectMascotas}
                          >
                            <option value="">Seleccionar</option>
                            {!users.loading &&
                              users.data
                                .filter((item) => item.id_t_us === 4)
                                .map((user) => (
                                  <option key={user.id_usuario} value={user.id_usuario}>
                                    {user.nombre_usuario}
                                  </option>
                                ))}
                          </Form.Control>
                        </Col>
                      </Row>
                      {misMascotas.loading ? (
                        <>
                          {/* <Row className="mt-3">
                              <Col>
                                <Form.Label>Mascota</Form.Label>
                                <Form.Control
                                  as="select"
                                  id="mascota_id"
                                  name="mascota_id"
                                  className="form-control"
                                  value={emergencyForm.mascota_id}
                                  onChange={changeMascota}
                                >
                                  <option value="">Seleccionar</option>
                                  {misMascotas.loading &&
                                    misMascotas.data.map((mascota) => (
                                      <option key={mascota.id} value={mascota.id}>
                                        {mascota.nombre}
                                      </option>
                                    ))}
                                </Form.Control>
                              </Col>
                            </Row> */}
                          <Row className="mt-3">
                            <Col>
                              {!showCita && (
                                <CreateCita
                                  mascotas={mascotas}
                                  medicos={medicos}
                                  ocupados={ocupados}
                                  salas={salas}
                                  id={idUser}
                                  tipoCita={2}
                                />
                              )}
                            </Col>
                          </Row>
                        </>
                      ) : (
                        <h3>No hay mascotas asociadas</h3>
                      )}
                    </>
                  </Form.Group>
                </Tab.Pane>
                <Tab.Pane eventKey="third-4">
                  <Form.Group>
                    <Row className="mt-3">
                      <Col>
                        <Form.Label>Dueno de mascota</Form.Label>
                        <Form.Control
                          as="select"
                          id="dueno_id"
                          name="usuario_mascota_id"
                          className="form-control"
                          data-test-form-emergencia="select_dueno"
                          value={emergencyForm.usuario_mascota}
                          onChange={comprobarPago}
                        >
                          <option value="">Seleccionar</option>
                          {!users.loading &&
                            users.data
                              .filter((item) => item.id_t_us === 4)
                              .map((user) => (
                                <option key={user.id_usuario} value={user.id_usuario}>
                                  {user.nombre_usuario}
                                </option>
                              ))}
                        </Form.Control>
                      </Col>
                    </Row>
                    {pagoPendiente.loading && (
                      <>
                        <Row className="mt-3">
                          <Col>
                            <h3>
                              El usuario tiene {pagoPendiente.data.length} pago(s) pendiente(s)
                            </h3>
                          </Col>
                        </Row>
                        <Row className="mt-3">
                          <Col>
                            <TablePago headers={headersPago} body={pagoPendiente.data} />
                          </Col>
                        </Row>
                      </>
                    )}
                  </Form.Group>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    </>
  )
}
