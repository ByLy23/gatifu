import Offcanvas from "react-bootstrap/Offcanvas";
import { useContext } from "react";
import { AuthContext } from "../../../../auth/context/AuthContext";
import { Button } from "react-bootstrap";
import { HiOutlineMail } from "react-icons/hi";
import { HiOutlinePhone } from "react-icons/hi";
import { HiOutlineLocationMarker } from "react-icons/hi";
import { HiOutlineUser } from "react-icons/hi";
import { HiOutlineIdentification } from "react-icons/hi";
import { RiExchangeDollarFill } from "react-icons/ri"; //puntos
import { ImBarcode } from "react-icons/im"; //codigo
import {IoMdBarcode} from "react-icons/io"; //codigo de barras

export default function Profile({ show, handleClose }) {
  const { user } = useContext(AuthContext);

  return (
    <>
      <Offcanvas
        show={show}
        onHide={handleClose}
        placement="end"
        backdrop={false}
        style={{
          width: "350px",
          height: "100%",
          backgroundColor: "#f8f9fa",
          color: "#000",
          padding: "20px",
          borderRadius: "0px",
          border: "none",
          boxShadow: "0px 0px 8px 0px rgba(0,0,0,0.75)",
        }}
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Mis Datos</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <p>
            <HiOutlineUser /> <strong>Nombre: </strong>
            {user.name}
          </p>
          <p>
            <HiOutlineMail scale={1.5} /> <strong>Correo: </strong>{" "}
            {user.correo}
          </p>
          <p>
            <HiOutlineIdentification scale={1.5} /> <strong>Usuario: </strong>{" "}
            {user.usuario}
          </p>
          <p>
            <HiOutlinePhone scale={1.5} /> <strong>Telefono: </strong>{" "}
            {user.telefono}
          </p>
          <p>
            <HiOutlineLocationMarker scale={1.5} /> <strong>Direccion: </strong>{" "}
            {user.direccion}
          </p>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              padding: "20px",
              borderRadius: "1px",
              border: "none",
              boxShadow: "0px 0px 8px 0px rgba(0,0,0,0.75)",
              marginTop: "4rem",
            }}
          >
            {user.codigo === 0 ? (
              <Button
                style={{
                  backgroundColor: "#f8f9fa",
                  color: "#000",
                  border: "1px solid rgba(0,0,0,0.75)",
                  borderRadius: "5px",
                }}
                onClick={() => {
                  user.codigo = "GTF" + user.id_usuario; //generar codigo de usuario
                  sessionStorage.setItem("user", JSON.stringify(user));
                  window.location.reload();
                }}
              >
                <IoMdBarcode scale={1.5} /> Presentar codigo
              </Button>
            ) : (
              <div>
                <p>
                  <ImBarcode scale={1.5} /> {" "} <strong>Codigo: </strong> {user.codigo}
                </p>
                <p>
                  <RiExchangeDollarFill scale={1.5} /> <strong>Puntos: </strong>{" "}
                  {user.puntos}
                </p>
              </div>
            )}
          </div>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}
