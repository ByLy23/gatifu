import { Form, Button } from 'react-bootstrap'
import Card from 'react-bootstrap/Card'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { TableHeaders } from '../../../../components/TableHeaders'
import axios from 'axios'
import Swal from 'sweetalert2'
import { useDispatch } from 'react-redux'

import { updateMascota } from '../../../../features/mascotas/mascotaSlice'

export const TablaMascotasCliente = ({ headers = [], body = [], user }) => {
  const dispatch = useDispatch()

  const upload = async (e, id) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('file', e.target.files[0])
    const res = await axios
      .post(`${import.meta.env.VITE_SERVICE_CLIENT}/api/pet/uploadImage/${id}/mascota`, formData, {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem('token')}`,
        },
      })
      .then((res) => {
        Swal.fire({
          icon: 'info',
          title: 'Estas seguro de subir esta imagen?',
          showCancelButton: true,
          confirmButtonText: `Subir`,
        }).then(async (result) => {
          if (result.isConfirmed) {
            dispatch(
              updateMascota(
                {
                  id: id,
                  foto: `${id}-${e.target.files[0].name}`,
                },
                user.id_usuario
              )
            )
          }
        })
      })
  }

  return (
    <Row>
      {body.map((mascota, index) => (
        <Col key={index} sm={4}>
          <Card style={{ width: '18rem', marginTop: '2rem' }} border="light">
            {mascota.foto !== '' && (
              <Card.Img
                variant="top"
                style={{ height: '14rem' }}
                src={`${import.meta.env.VITE_SERVICE_CLIENT}/mascota/${mascota.foto}`}
              />
            )}

            <Card.Body>
              <Card.Title data-test-mascota-nombre="nombre_mascota" style={{ textAlign: 'center' }}>
                {mascota.nombre}
              </Card.Title>
              <Card.Text>
                <p>
                  <b>Edad:</b> {mascota.edad}
                </p>
                <p>
                  <b>Sexo:</b> {mascota.genero}
                </p>
                <p>
                  <b>Raza:</b> {mascota.raza}
                </p>
                <p>
                  <b>Actualizar foto: </b>
                  <Form.Control
                    onChange={(e) => upload(e, mascota.id)}
                    accept="image/*"
                    type="file"
                    size="sm"
                  />
                </p>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  )
}
