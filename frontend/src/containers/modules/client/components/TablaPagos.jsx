import React, { useState } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/esm/Container'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Moment from 'moment'
import { ScaleLoader } from 'react-spinners'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDetalleFactura } from '../../../../features/facturas/facturaSlice'
import Pdf from 'react-to-pdf'

const ref1 = React.createRef()

function TablaPagos({ data, factura = false, height = '35rem' }) {
  const columns = data[0] && Object.keys(data[0])

  const style = {
    top: 0,
    left: 0,
    zIndex: 10,
    height: '2.5rem',
    position: 'sticky',
    color: '#fff',
    backgroundColor: '#24242c ',
  }

  const [selectfactura, setFactura] = useState({
    fecha: '',
    total: '',
  })

  const [show, setShow] = useState(false)

  const dispatch = useDispatch()

  return (
    <>
      <Container style={{ marginTop: '2.5rem' }}>
        <div style={{ overflowY: 'auto', height: height }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                {data[0] && columns.map((heading) => <th style={style}>{heading}</th>)}
                {factura && <th style={style}>Acciones</th>}
              </tr>
            </thead>
            <tbody>
              {data.map((row) => (
                <tr>
                  {columns.map((column) => (
                    <td>
                      {column === 'fecha' ? Moment(row[column]).format('DD/MM/YYYY') : row[column]}
                    </td>
                  ))}
                  {factura && (
                    <td>
                      <Button
                        style={{
                          backgroundColor: '#ff902f',
                          borderColor: '#ff902f',
                        }}
                        size="sm"
                        data-test-pago={`btn_detalle_${row.id}`}
                        onClick={() => {
                          dispatch(fetchDetalleFactura(row.id))
                          setShow(true)
                          setFactura({
                            fecha: Moment(row.fecha).format('DD/MM/YYYY'),
                            total: row.total,
                          })
                        }}
                      >
                        Ver Detalle
                      </Button>
                    </td>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Container>
      <ModalDetalle show={show} onHide={() => setShow(false)} factura={selectfactura} />
    </>
  )
}

function ModalDetalle({ show, onHide, factura }) {
  const { detalleFactura, loadingFactura } = useSelector((state) => state.facturas)

  return (
    <Modal show={show} onHide={onHide} centered size="xl">
      <Modal.Header closeButton>
        <Modal.Title>Detalle de la factura</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {loadingFactura ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: '60px',
            }}
          >
            <ScaleLoader color={'#20bbcce1'} loading={loadingFactura} height={60} />
          </div>
        ) : (
          <>
            <div
              style={{
                textAlign: 'center',
              }}
              ref={ref1}
            >
              <h2>Factura</h2>
              <h5>Fecha: {factura.fecha}</h5>
              <TablaPagos data={detalleFactura} height={'25rem'} />
              <h5
                style={{
                  marginTop: '1rem',
                }}
              >
                Total: {factura.total}
              </h5>
            </div>
            <div
              style={{
                display: 'flex',
                alignItems: 'right',
                justifyContent: 'right',
              }}
            >
              <Pdf
                targetRef={ref1}
                filename={'factura_' + factura.fecha + '_' + factura.total + '.pdf'}
                options={{
                  orientation: 'landscape',
                  unit: 'in',
                }}
                x={0.5}
                y={0.5}
                scale={0.9}
              >
                {({ toPdf }) => (
                  <Button
                    style={{
                      backgroundColor: '#20bbcce1',
                      borderColor: '#20bbcce1',
                      marginTop: '1rem',
                      color: '#fff',
                    }}
                    data-test-pago="btn_descargar"
                    onClick={toPdf}
                  >
                    Descargar PDF
                  </Button>
                )}
              </Pdf>
            </div>
          </>
        )}
      </Modal.Body>
    </Modal>
  )
}

export default TablaPagos
