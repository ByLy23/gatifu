import { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/esm/Button'
import { useDispatch, useSelector } from 'react-redux'
import AlertDismissible from '../../../../../components/Alert'
import Row from 'react-bootstrap/esm/Row'
import Col from 'react-bootstrap/esm/Col'
import AddMotivo from './AddMotivo'
import Spinner from 'react-bootstrap/Spinner'
import Moment from 'moment'
import Table from 'react-bootstrap/esm/Table'
import { createCita } from '../../../../../features/citas/citaSlice'

function CreateCita({
  mascotas,
  medicos,
  salas,
  ocupados,
  id,
  tipoCita = 1,
  user = {
    codigo: 0,
  },
}) {
  const { loadingCita } = useSelector((state) => state.citas)
  const [show, setShow] = useState(false)
  const [message, setMessage] = useState('')
  const [showButton, setShowButton] = useState(true)
  const [addM, setAddM] = useState(false)

  const [listaMotivos, setMotivos] = useState([])
  const [ocupadosF, setOcupados] = useState([])

  const [mascota, setMascota] = useState('0')
  const [fecha, setFecha] = useState('')

  const [terminar, setTerminar] = useState(false)

  const dispatch = useDispatch()

  const handleSubmit = (e) => {
    e.preventDefault()
    if (fecha === '' || mascota === '0') {
      setShow(true)
      setMessage('Todos los campos son obligatorios')
      setTimeout(() => {
        setShow(false)
      }, 5000)
      return
    }
    console.log(fecha)

    setOcupados(
      ocupados.filter((o) => {
        return Moment(o.fecha).format('YYYY-MM-DD') === fecha
      })
    )
    setShowButton(false)
  }
  const [sumaPuntos, setSumaPuntos] = useState(0)
  const [addPuntos, setAddPuntos] = useState(0)

  const handleCita = () => {
    var cita = {
      fecha: Moment(fecha).format('DD-MM-YYYY'),
      idMascota: mascota,
      motivos: listaMotivos,
      tipo: tipoCita,
      puntos: sumaPuntos,
      addPuntos: addPuntos,
    }
    console.log(cita)
    dispatch(createCita(cita, id, 'Cliente', user, sumaPuntos))
    setAddPuntos(0)
    setSumaPuntos(0)
    setTerminar(false)
  }

  const handleTerminar = () => {
    let sumaPuntos = 0
    let addPuntos = 0
    listaMotivos.forEach((motivo) => {
      sumaPuntos += parseInt(motivo.cantidad)
      addPuntos += parseInt(motivo.addPuntos)
    })
    if (parseInt(sumaPuntos) > parseInt(user.puntos)) {
      setShow(true)
      setMessage('No tienes suficientes puntos')
      setTimeout(() => {
        setShow(false)
      }, 5000)
      return
    }
    if (user.codigo !== 0) {
      setAddPuntos(addPuntos)
    }
    setSumaPuntos(sumaPuntos)
    // se hace una regla de 3 para calcular el precio final por cada motivo
    listaMotivos.forEach((motivo) => {
      if (motivo.cantidad > 0) {
        motivo.resultado = parseFloat(
          motivo.precio - (motivo.precio / motivo.puntos) * motivo.cantidad
        ).toFixed(2)
      } else {
        motivo.resultado = motivo.precio
      }
    })
    setTerminar(true)
  }

  useEffect(() => {
    if (loadingCita === false) {
      setMotivos([])
      setShowButton(true)
      setMascota('0')
      setFecha('')
    }
  }, [loadingCita])

  return (
    <Container>
      <h1>Crear Cita</h1>
      {show && <AlertDismissible onHide={() => setShow(false)} message={message} />}
      {user.codigo === 0 && tipoCita === 1 && (
        <AlertDismissible
          onHide={() => setShow(false)}
          message="Presenta tu codigo de lealtad ➜"
          variant="warning"
        />
      )}
      <Form style={{ marginTop: '1rem' }}>
        <Form.Group className="mb-3">
          <Form.Label>Mascota: </Form.Label>
          <Form.Select
            disabled={showButton === false}
            aria-label="Default select example"
            data-test-cita="select_mascota"
            onChange={(e) => setMascota(e.target.value)}
            size="sm"
          >
            <option>Seleccionar Mascota</option>
            {mascotas.map((mascota) => (
              <option value={mascota.id}>{mascota.nombre}</option>
            ))}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Fecha: </Form.Label>
          <Form.Control
            disabled={showButton === false}
            type="date"
            data-test-cita="fecha_cita"
            min={new Date().toISOString().split('T')[0]}
            onChange={(e) => setFecha(e.target.value)}
            size="sm"
          />
        </Form.Group>
        {showButton ? (
          <Button
            style={{ backgroundColor: '#20bbcce1', borderColor: '#20bbcce1 ' }}
            size="lg"
            data-test-cita="btn_crear_cita"
            onClick={handleSubmit}
          >
            Crear Cita
          </Button>
        ) : !addM ? (
          <>
            {listaMotivos.length > 0 && (
              <TableMotivos
                motivos={listaMotivos}
                user={user}
                terminar={terminar}
                sumaPuntos={sumaPuntos}
              />
            )}
            <div
              style={{
                marginTop: '2rem',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Row>
                <Col>
                  <Button
                    style={{
                      backgroundColor: '#20bbcce1',
                      borderColor: '#20bbcce1 ',
                      width: '12rem',
                      visibility: terminar ? 'hidden' : 'visible', // si terminar es true, el boton se oculta, si es false, se muestra
                    }}
                    data-test-motivo="btn_crear_motivo"
                    disabled={loadingCita}
                    size="lg"
                    onClick={() => setAddM(true)}
                  >
                    Agregar Motivo
                  </Button>
                </Col>
                <Col>
                  <Button
                    style={{
                      backgroundColor: '#ff902f',
                      borderColor: '#ff902f',
                    }}
                    disabled={loadingCita}
                    size="lg"
                    onClick={() => {
                      setMotivos([])
                      setShowButton(true)
                      setTerminar(false)
                    }}
                  >
                    Cancelar
                  </Button>
                </Col>
                <Col>
                  <Button
                    style={{
                      backgroundColor: '#20bbcce1',
                      borderColor: '#20bbcce1 ',
                      width: '12rem',
                      visibility: listaMotivos.length > 0 ? 'visible' : 'hidden', // si listaMotivos es mayor a 0, el boton se muestra, si es menor, se oculta
                    }}
                    disabled={loadingCita}
                    size="lg"
                    data-test-cita="terminar_cita"
                    onClick={
                      () => (terminar ? handleCita() : handleTerminar()) // si terminar es true, se hace handleCita, si es false, se hace handleTerminar
                    }
                  >
                    {loadingCita ? (
                      <Spinner
                        as="span"
                        animation="border"
                        size="lg"
                        role="status"
                        aria-hidden="true"
                      />
                    ) : (
                      <>{terminar ? 'Agendar Cita' : 'Terminar Cita'} </>
                    )}
                  </Button>
                </Col>
              </Row>
            </div>
          </>
        ) : (
          <></>
        )}
        {addM && (
          <AddMotivo
            listaMotivos={listaMotivos}
            medicos={medicos}
            salas={salas}
            ocupados={ocupadosF}
            onHide={() => setAddM(false)}
          />
        )}
      </Form>
    </Container>
  )
}

function TableMotivos({ motivos, user, terminar, sumaPuntos }) {
  return (
    <>
      <h5>Motivos Seleccionados</h5>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Motivo</th>
            {user.codigo !== 0 && <th>Puntos a sumar</th>}
            <th>Hora</th>
            <th>Precio</th>
            {user.codigo !== 0 && (
              <>
                {' '}
                <th>Precio por puntos</th> <th>Cantidad de puntos a canjear</th>{' '}
              </>
            )}
            {terminar && <th>Resultado del precio</th>}
          </tr>
        </thead>
        <tbody>
          {motivos.map((motivo) => (
            <tr>
              <td>{motivo.motivo}</td>
              {user.codigo !== 0 && <td>{motivo.addPuntos}</td>}
              <td>{motivo.inicio}</td>
              <td>{motivo.precio}</td>
              {user.codigo !== 0 && (
                <>
                  <td>{motivo.puntos}</td>
                  <td>
                    <Form.Control
                      type="number"
                      size="sm"
                      disabled={terminar} // si terminar es true, el input se deshabilita, si es false, se habilita
                      onChange={(e) => {
                        if (e.target.value > motivo.puntos) {
                          e.target.value = motivo.puntos
                          motivo.cantidad = motivo.puntos
                        } else {
                          motivo.cantidad = parseInt(e.target.value)
                        }
                      }}
                    />{' '}
                  </td>
                </>
              )}
              {terminar && <td>{motivo.resultado}</td>}
            </tr>
          ))}
        </tbody>
      </Table>
      {terminar &&
        sumaPuntos > 0 && ( // si terminar es true se muestra la suma de puntos
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <h5>Suma de puntos a canjear: {sumaPuntos}</h5>
          </div>
        )}{' '}
    </>
  )
}

export default CreateCita
