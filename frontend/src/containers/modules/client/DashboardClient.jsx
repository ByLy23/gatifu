import { useEffect, useState } from 'react'
import { useContext } from 'react'
import { AuthContext } from '../../../auth/context/AuthContext'
import { useDispatch, useSelector } from 'react-redux'
import { fetchCitas, fetchHistoryCitas } from '../../../features/citas/citaSlice'
import { fetchMedicos } from '../../../features/users/usersSlice'
import { fetchMascotas } from '../../../features/mascotas/mascotaSlice'
import { fetchSalas } from '../../../features/salas/salaSlice'
import { fetchOcupados } from '../../../features/salas/ocupados'
import { TablaMascotasCliente } from './components/TablaMascotasCliente'
import { fetchPagos } from '../../../features/citas/pagosSlice'
import { fetchServicios } from '../../../features/servicios/servicioSlice'
import { getProductos } from '../../../features/productos/productoSlice'
import TablePaquetes from '../../../components/TablePaquetes'
import Header from '../../../components/Header'
import Col from 'react-bootstrap/Col'
import Nav from 'react-bootstrap/Nav'
import Row from 'react-bootstrap/Row'
import Tab from 'react-bootstrap/Tab'
import ClockLoader from 'react-spinners/ClockLoader'
import ClipLoader from 'react-spinners/ClipLoader'
import CreateCita from './cita/create/CreateCita'
import TableHistoryCitas from '../../../components/TableHistoryCitas'
import CardCitas from '../../../components/CardCitas'
import TableServices from '../../../components/TableServices'
import TableProduct from '../../../components/TableProduct'
import { fetchPaquetes } from '../../../features/paquetes/paqueteSlice'
import '../../../styles/admin.css'
import { setErrorUnauthorized } from '../../../features/error/errorSlice'
import TablaPagos from './components/TablaPagos'
import { fetchFacturas } from '../../../features/facturas/facturaSlice'
import Profile from './components/Profile'

const { id_usuario: idUsuario } = JSON.parse(sessionStorage.getItem('user')) || {}
const headersMascota = ['Nombre', 'Edad', 'Foto', 'Genero', 'Raza', 'Nueva Foto']

export function DashboardClient() {
  const { citas, historyCitas, loadingHistorial, loadingPendientes } = useSelector(
    (state) => state.citas
  )
  const { medicos } = useSelector((state) => state.users)
  const { mascotas } = useSelector((state) => state.mascotas)
  const { salas, ocupados } = useSelector((state) => state.salas)
  const { pagos } = useSelector((state) => state.pagos)
  const { servicios } = useSelector((state) => state.servicios)
  const { productos } = useSelector((state) => state.productos)
  const { paquetes } = useSelector((state) => state.paquetes)
  const { facturas } = useSelector((state) => state.facturas)
  const [manage_mascotas, setManageMascotas] = useState(false)
  const [manage_citas, setManageCitas] = useState(false)
  const [manage_pagos, setManagePagos] = useState(false)
  const [manage_servicios, setManageServicios] = useState(false)
  const [manage_productos, setManageProductos] = useState(false)
  const [manage_paquetes, setManagePaquetes] = useState(false)

  const [tab_key, setTab_key] = useState('cero')
  const { errorUnauthorized } = useSelector((state) => state.error)
  const { user, logout } = useContext(AuthContext)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchCitas(user.id_usuario, 'Cliente'))
    dispatch(fetchHistoryCitas(user.id_usuario, 'Cliente'))
    dispatch(fetchMedicos())
    dispatch(fetchMascotas(user.id_usuario))
    dispatch(fetchSalas())
    dispatch(fetchOcupados())
    dispatch(fetchPagos(user.id_usuario, 2))

    dispatch(fetchServicios())
    dispatch(getProductos(2))
    dispatch(fetchPaquetes(2))
    dispatch(fetchFacturas(user.id_usuario))
  }, [])

  useEffect(() => {
    if (errorUnauthorized) {
      logout()
      dispatch(setErrorUnauthorized(false))
    }
  }, [errorUnauthorized])

  const [showProfile, setShowProfile] = useState(false)

  return (
    <>
      <Header action="Cerrar Sesion" tipo={1} click={() => setShowProfile(true)} />
      <Profile show={showProfile} handleClose={() => setShowProfile(false)} />
      <div style={{ marginRight: '2rem' }}>
        <Tab.Container bg="dark" variant="dark" id="left-tabs-example" activeKey={tab_key}>
          <Row>
            <Col sm={3}>
              <div className="BarCliente" style={{ backgroundColor: '#fff' }}>
                <Nav variant="pills" className="flex-column" style={{ marginLeft: '5px' }}>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-mascotas="mascotas"
                      onClick={() => {
                        if (tab_key === 'first' || (tab_key === 'first-2' && manage_mascotas)) {
                          setTab_key('five')
                        }
                        setManageMascotas(!manage_mascotas)
                      }}
                    >
                      🐶 Mascotas{' '}
                      {manage_mascotas ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_mascotas ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first"
                          data-test-mascota="mi_mascota"
                          onClick={() => setTab_key('first')}
                        >
                          🧸 Mis Mascotas
                        </Nav.Link>
                        {/* <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="first-2"
                          onClick={() => setTab_key('first-2')}
                        >
                          🐾
                        </Nav.Link>{' '} */}
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-citas="citas"
                      onClick={() => {
                        if (
                          (tab_key === 'second' ||
                            tab_key === 'second-2' ||
                            tab_key === 'second-3') &&
                          manage_citas
                        ) {
                          setTab_key('five')
                        }
                        setManageCitas(!manage_citas)
                      }}
                    >
                      🖊 Citas{' '}
                      {manage_citas ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_citas ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second"
                          onClick={() => setTab_key('second')}
                        >
                          🐕‍🦺 Agendar Cita
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second-2"
                          data-test-cita="lista_citas"
                          onClick={() => setTab_key('second-2')}
                        >
                          🗃 Historial Citas
                        </Nav.Link>{' '}
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="second-3"
                          onClick={() => setTab_key('second-3')}
                        >
                          🗓 Citas Pendientes
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/******************************************* */}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-servicios="servicio"
                      onClick={() => {
                        if (tab_key === 'four' || (tab_key === 'four-2' && manage_servicios)) {
                          setTab_key('five')
                        }
                        setManageServicios(!manage_servicios)
                      }}
                    >
                      🪒 Servicio Groaming{' '}
                      {manage_servicios ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_servicios ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="four"
                          data-test-servicios="listar_servicio"
                          onClick={() => setTab_key('four')}
                        >
                          🧼 Servicios
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/******************************************* */}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-productos="producto"
                      onClick={() => {
                        if (tab_key === 'six' || (tab_key === 'six-2' && manage_productos)) {
                          setTab_key('five')
                        }
                        setManageProductos(!manage_productos)
                      }}
                    >
                      💊 Farmacia{' '}
                      {manage_productos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_productos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="six"
                          data-test-productos="lista_producto"
                          onClick={() => setTab_key('six')}
                        >
                          🛒 Productos
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/******************************************* */}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-paquetes="paquete"
                      onClick={() => {
                        if ((tab_key === 'seven' || tab_key === 'seven-2') && manage_paquetes) {
                          setTab_key('five')
                        }
                        setManagePaquetes(!manage_paquetes)
                      }}
                    >
                      📦 Paquetes{' '}
                      {manage_paquetes ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_paquetes ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="seven"
                          data-test-paquetes="lista_paquete"
                          onClick={() => setTab_key('seven')}
                        >
                          🗄 Listar Paquetes
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                  {/******************************************* */}
                  <Nav.Item style={{ marginTop: '10px' }}>
                    <Nav.Link
                      style={{ fontSize: 22 }}
                      data-test-pagos="pagos"
                      onClick={() => {
                        if (tab_key === 'third' || (tab_key === 'third-2' && manage_pagos)) {
                          setTab_key('five')
                        }
                        setManagePagos(!manage_pagos)
                      }}
                    >
                      💳 Pagos{' '}
                      {manage_pagos ? (
                        <span style={{ fontSize: 16 }}>❌</span>
                      ) : (
                        <span style={{ fontSize: 16 }}>⬅️</span>
                      )}
                    </Nav.Link>
                    {manage_pagos ? (
                      <>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="third"
                          onClick={() => setTab_key('third')}
                        >
                          💳 Mis Citas Pagadas
                        </Nav.Link>
                        <Nav.Link
                          style={{ marginLeft: '20px', fontSize: 20 }}
                          eventKey="third-2"
                          data-test-factura="mifactura"
                          onClick={() => setTab_key('third-2')}
                        >
                          💳 Mis Facturas
                        </Nav.Link>
                      </>
                    ) : (
                      <></>
                    )}
                  </Nav.Item>
                </Nav>
              </div>
            </Col>
            <Col sm={9}>
              <Tab.Content style={{ marginLeft: '-4rem' }}>
                <Tab.Pane eventKey="cero">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>Bienvenidos 👋</h1>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="first">
                  <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Mis Mascotas</h2>
                  <TablaMascotasCliente headers={headersMascota} body={mascotas} user={user} />
                </Tab.Pane>
                <Tab.Pane eventKey="first-2"></Tab.Pane>
                <Tab.Pane eventKey="second">
                  <div style={{ marginTop: '1.5rem' }}>
                    <CreateCita
                      mascotas={mascotas}
                      medicos={medicos}
                      salas={salas}
                      ocupados={ocupados}
                      id={user.id_usuario}
                      user={user}
                    />
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="second-2">
                  {loadingHistorial ? (
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: '12rem',
                      }}
                    >
                      <ClockLoader
                        color={'#C4FFC4'}
                        loading={true}
                        size={200}
                        speedMultiplier={0.5}
                      />
                    </div>
                  ) : (
                    <TableHistoryCitas history={historyCitas} tipo={'Cliente'} />
                  )}
                </Tab.Pane>
                <Tab.Pane eventKey="second-3">
                  {loadingPendientes ? (
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: '12rem',
                      }}
                    >
                      <ClipLoader
                        color={'#C4FFC4'}
                        loading={true}
                        size={200}
                        speedMultiplier={0.5}
                      />
                    </div>
                  ) : (
                    <CardCitas citas={citas} tipo={'Cliente'} />
                  )}
                </Tab.Pane>
                <Tab.Pane eventKey="third">
                  <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Mis Citas Pagadas</h2>
                  <TablaPagos data={pagos} />
                </Tab.Pane>
                <Tab.Pane eventKey="third-2">
                  <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Facturas</h2>
                  <TablaPagos data={facturas} factura={true} />
                </Tab.Pane>
                <Tab.Pane eventKey="four">
                  <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Servicios</h2>
                  <TableServices
                    products={servicios}
                    mascotas={mascotas}
                    id={user.id_usuario}
                    user={user}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="six">
                  <TableProduct productos={productos} cliente={true} id={user.id_usuario} />
                </Tab.Pane>
                <Tab.Pane eventKey="seven">
                  <TablePaquetes
                    paquetes={paquetes}
                    admin={false}
                    id={user.id_usuario}
                    mascotas={mascotas}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="five">
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '18rem',
                    }}
                  >
                    <h1>👈 Seleccione una opcion</h1>
                  </div>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    </>
  )
}
