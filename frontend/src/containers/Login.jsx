import { useState } from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/esm/Container'
import AlertDismissible from '../components/Alert'
import FloatingLabel from 'react-bootstrap/FloatingLabel'
import Spinner from 'react-bootstrap/Spinner'
import '../styles/login.css'
import { useForm } from '../hooks/useForm'
import { useContext } from 'react'
import { AuthContext } from '../auth/context/AuthContext'
import { setErrorUnauthorized } from '../features/error/errorSlice'
import { useDispatch, useSelector } from 'react-redux'
import Footer from '../components/Footer'

function Login() {
  const userInfo = {
    email: '',
    password: '',
  }
  const { login, user } = useContext(AuthContext)
  const { email, password, onInputChange, onResetForm } = useForm(userInfo)
  const [show, setShow] = useState(false)
  const imgGatifu = './assets/gatifu_hor_transparente.png'
  const [loading, setLoading] = useState(false) // loading button
  const dispatch = useDispatch()

  const [message, setMessage] = useState('')

  const handleSubmit = async (e) => {
    setLoading(true) // loading button
    dispatch(setErrorUnauthorized(false))
    e.preventDefault()
    const resp = await login(email, password)
    if (resp) {
      setShow(true)
      setMessage('Usuario o contraseña incorrectos')
      setTimeout(() => {
        setShow(false)
      }, 5000)
    }
    setLoading(false) // loading button
    onResetForm()
    //verificar tipo de usuario\
    //funcion para validar el usuario
    //si el usuario es valido, redireccionar a la pagina principal
    //si el usuario no es valido, mostrar un mensaje de error
  }
  return (
    <>
      {/* <Header /> */}
      <Container className="d-flex justify-content-center align-items-center">
        <div className="background_wavy"></div>
      </Container>
      {/* <Header /> */}
      <Container
        style={{
          width: '100%',
          height: '100vh',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Card style={{ width: '50%', background: '' }}>
          <Card.Img variant="top" src={imgGatifu} />
          <Card.Body>
            {show && <AlertDismissible onHide={() => setShow(false)} message={message} />}
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <FloatingLabel controlId="floatingInput" label="Usuario" className="mb-3">
                  <Form.Control
                    style={{ height: '50px' }}
                    name="email"
                    value={email}
                    onChange={onInputChange}
                    type="text"
                    data-test-user="email" // data-text es usada para testear el usuario
                    placeholder="Ingrese su correo"
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <FloatingLabel controlId="floatingPassword" label="Contraseña">
                  <Form.Control
                    style={{ height: '50px' }}
                    name="password"
                    value={password}
                    onChange={onInputChange}
                    type="password"
                    data-test-pass="password" // data-text es usada para testear el usuario
                    placeholder="Ingrese su contraseña"
                  />
                </FloatingLabel>
              </Form.Group>
              <div
                style={{
                  marginTop: '25px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Button
                  variant="primary"
                  disabled={loading} // loading button
                  type="submit"
                  size="lg"
                  onClick={handleSubmit}
                  data-test-btn="btn-sesion" // data-text es usada para testear el usuario
                  style={{
                    width: '100%',
                    height: '50px',
                    background: '#19b9cc',
                    border: 'none',
                  }}
                >
                  {
                    loading ? ( // loading button
                      <Spinner animation="border" size="lg" /> // loading button
                    ) : (
                      // loading button
                      "Login!" 
                    ) // loading button
                  }
                </Button>
              </div>
            </Form>
          </Card.Body>
        </Card>
      </Container>
      <Footer />
    </>
  )
}

export default Login
