import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import axios from "axios";
import { setErrorUnauthorized } from "../error/errorSlice";
import { fetchFacturas } from "../facturas/facturaSlice";


const initialState = {
  productos: [],
  producto: null,
  categorias: [],
  marcas: [],
  loadingProducto: false,
  errorProducto: null,
};

export const productoSlice = createSlice({
  name: "producto",
  initialState,
  reducers: {
    setProductos: (state, action) => {
      state.productos = action.payload;
    },
    setProducto: (state, action) => {
      state.producto = action.payload;
    },
    setCategorias: (state, action) => {
      state.categorias = action.payload;
    },
    setMarcas: (state, action) => {
      state.marcas = action.payload;
    },
    setLoadingProducto: (state, action) => {
      state.loadingProducto = action.payload;
    },
    setErrorProducto: (state, action) => {
      state.errorProducto = action.payload;
    },
  },
});

export const {
  setProductos,
  setProducto,
  setLoadingProducto,
  setErrorProducto,
  setCategorias,
  setMarcas,
} = productoSlice.actions;

export default productoSlice.reducer;

export const getMarcas = () => async (dispatch) => {
  dispatch(setLoadingProducto(true));
  dispatch(setErrorProducto(null));
  try {
    const res = await axios.get(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/marca`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setMarcas(res.data));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setErrorProducto(error));
    dispatch(setLoadingProducto(false));
  }
};

export const getCategorias = () => async (dispatch) => {
  dispatch(setLoadingProducto(true));
  try {
    const res = await axios.get(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/categoria`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setCategorias(res.data));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setErrorProducto(error));
    dispatch(setLoadingProducto(false));
  }
};

export const getProductos = (tipo) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    const res = await axios.get(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/tipo/${tipo}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setProductos(res.data));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setLoadingProducto(false));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const createProducto = (producto) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    console.log(producto);
    const res = await axios.post(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos`,
      producto,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Producto creado correctamente",
      text: res.data.message,
      confirmButtonText: "Aceptar",
    });
    dispatch(getProductos(1));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setLoadingProducto(false));
    console.log(error);
    dispatch(setErrorProducto(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const updateProducto = (id, producto) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    const res = await axios.put(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/${id}`,
      producto,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Producto actualizado correctamente",
      text: res.data.message,
      confirmButtonText: "Aceptar",
    });
    dispatch(getProductos(1));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setLoadingProducto(false));
    dispatch(setErrorProducto(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const deleteProducto = (id) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    await axios.delete(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/${id}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(getProductos(1));
    dispatch(setLoadingProducto(false));
    Swal.fire({
      icon: "success",
      title: "Producto eliminado correctamente",
      confirmButtonText: "Aceptar",
    });
  } catch (error) {
    dispatch(setLoadingProducto(false));
    dispatch(setErrorProducto(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const updateStock = (producto) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    console.log(producto);
    const res = await axios.put(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/productos/stock/${
        producto.id
      }`,
      producto,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Stock actualizado correctamente",
      text: res.data.message,
      confirmButtonText: "Aceptar",
    });
    dispatch(getProductos(1));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setLoadingProducto(false));
    dispatch(setErrorProducto(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const comprarProducto = (id, producto) => async (dispatch) => {
  try {
    dispatch(setLoadingProducto(true));
    console.log(id, producto);
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const res = await axios.post(
      `${import.meta.env.VITE_SERVICE_CLIENT}/api/compra/producto/${id}`,
      {
        ...producto,
        producto: [producto.producto],
      },
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Compra realizada correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchFacturas(id));
    dispatch(getProductos(2));
    dispatch(setLoadingProducto(false));
  } catch (error) {
    dispatch(setLoadingProducto(false));
    dispatch(setErrorProducto(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
    Swal.fire({
      icon: "error",
      title: "Error al realizar la compra",
      text: error.response.data.message,
      confirmButtonText: "Aceptar",
    });
  }
};
