import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import axios from "axios";
import { setErrorUnauthorized } from "../error/errorSlice";

import { fetchFacturas } from "../facturas/facturaSlice";

const initialState = {
  servicios: [],
  servicio: {},
  loadingServico: false,
  errorServico: null,
};

const servicioSlice = createSlice({
  name: "servicio",
  initialState,
  reducers: {
    setServicios(state, action) {
      state.servicios = action.payload;
    },
    setServicio(state, action) {
      state.servicio = action.payload;
    },
    setLoadingServicio(state, action) {
      state.loadingServico = action.payload;
    },
    setErrorServicio(state, action) {
      state.errorServico = action.payload;
    },
  },
});

export const {
  setServicios,
  setServicio,
  setLoadingServicio,
  setErrorServicio,
} = servicioSlice.actions;

export default servicioSlice.reducer;

export const fetchServicios = () => async (dispatch) => {
  try {
    dispatch(setLoadingServicio(true));
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/servicios`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    console.log(response.data);
    dispatch(setServicios(response.data));
    dispatch(setLoadingServicio(false));
  } catch (error) {
    dispatch(setLoadingServicio(false));
    dispatch(setErrorServicio(error));
  }
};

export const fetchServicio = (id) => async (dispatch) => {
  try {
    dispatch(setLoadingServicio(true));
    const response = await axios.get(`/api/servicios/${id}`);
    dispatch(setServicio(response.data));
    dispatch(setLoadingServicio(false));
  } catch (error) {
    dispatch(setLoadingServicio(false));
    dispatch(setErrorServicio(error));
  }
};

export const comprarServicio = (id, servicios, user) => async (dispatch) => {
  try {
    dispatch(setLoadingServicio(true));
    console.log(id, servicios);
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const response = await axios.post(
      `${import.meta.env.VITE_SERVICE_CLIENT}/api/compra/servicio/${id}`,
      servicios,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(fetchFacturas(id));
    dispatch(setLoadingServicio(false));
    Swal.fire({
      title: "¡Servicio comprado!",
      text: "El servicio ha sido comprado correctamente",
      icon: "success",
      confirmButtonText: "Aceptar",
    });
    if (user.codigo != 0) {
      console.log("entro");
      const { data } = await axios.get(
        `${import.meta.env.VITE_SERVICE_CLIENT}/api/puntos/${user.id_usuario}`,
        {
          headers: {
            authorization: `Bearer ${sessionStorage.getItem("token")}`,
          },
        }
      );
      console.log(data);
      user.puntos = data.puntos;
      sessionStorage.setItem("user", JSON.stringify(user));
      window.location.reload();
    }
  } catch (error) {
    dispatch(setLoadingServicio(false));
    dispatch(setErrorServicio(error));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};
