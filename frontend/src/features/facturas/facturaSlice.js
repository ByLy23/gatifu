import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import axios from "axios";
import { setErrorUnauthorized } from "../error/errorSlice";

const initialState = {
  facturas: [],
  factura: {},
  detalleFactura: [],
  loadingFactura: false,
  errorFactura: null,
};

const facturaSlice = createSlice({
  name: "factura",
  initialState,
  reducers: {
    setFacturas(state, action) {
      state.facturas = action.payload;
    },
    setFactura(state, action) {
      state.factura = action.payload;
    },
    setDetalleFactura(state, action) {
      state.detalleFactura = action.payload;
    },
    setLoadingFactura(state, action) {
      state.loadingFactura = action.payload;
    },
    setErrorFactura(state, action) {
      state.errorFactura = action.payload;
    },
  },
});

export const {
  setFacturas,
  setFactura,
  setDetalleFactura,
  setLoadingFactura,
  setErrorFactura,
} = facturaSlice.actions;

export default facturaSlice.reducer;

export const fetchFacturas = (id) => async (dispatch) => {
  try {
    dispatch(setLoadingFactura(true));
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_CLIENT}/api/facturas/${id}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    console.log(response.data);
    dispatch(setFacturas(response.data));
    dispatch(setLoadingFactura(false));
  } catch (error) {
    dispatch(setLoadingFactura(false));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const fetchDetalleFactura = (id) => async (dispatch) => {
  try {
    dispatch(setLoadingFactura(true));
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_CLIENT}/api/facturas/detalle/${id}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setDetalleFactura(response.data));
    dispatch(setLoadingFactura(false));
  } catch (error) {
    dispatch(setLoadingFactura(false));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};
