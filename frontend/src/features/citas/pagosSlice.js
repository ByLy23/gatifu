import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import axios from "axios";

export const pagosSlice = createSlice({
  name: "pagos",
  initialState: {
    pagos: [],
    loadingPagos: false,
    errorPagos: false,
  },
  reducers: {
    setPagos: (state, action) => {
      state.pagos = action.payload;
    },
    setLoadingPagos: (state, action) => {
      state.loadingPagos = action.payload;
    },
  },
});

export const { setPagos, setLoadingPagos } = pagosSlice.actions;

export default pagosSlice.reducer;

export const fetchPagos = (id, tipo) => async (dispatch) => {
  dispatch(setLoadingPagos(true));
  try {
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/pagos/${id}/${tipo}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setPagos(response.data));
    dispatch(setLoadingPagos(false));
  } catch (error) {
    console.log(error);
    dispatch(setLoadingPagos(false));
  }
};
