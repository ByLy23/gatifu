import { setOcupados } from "./salaSlice";
import axios from "axios";
import { setErrorUnauthorized } from "../error/errorSlice";
import Swal from "sweetalert2";

export const fetchOcupados = () => async (dispatch) => {
  try {
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_SECRETARIA}/api/medico/salas`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    dispatch(setOcupados(response.data));
  } catch (error) {
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
    console.log(error);
  }
};
