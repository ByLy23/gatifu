import { createSlice } from "@reduxjs/toolkit";

export const errorSlice = createSlice({
    name: "error",
    initialState: {
        errorUnauthorized: false,
    },
    reducers: {
        setErrorUnauthorized: (state, action) => {
            state.errorUnauthorized = action.payload;
        }
    }
});

export const { setErrorUnauthorized } = errorSlice.actions;

export default errorSlice.reducer;
