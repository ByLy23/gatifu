import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Swal from "sweetalert2";
import { setErrorUnauthorized } from "../error/errorSlice";
import { fetchFacturas } from "../facturas/facturaSlice";

const paqueteSlice = createSlice({
  name: "paquete",
  initialState: {
    paquetes: [],
    paquete: {},
    loadingPaquetes: false,
    errorPaquetes: false,
  },
  reducers: {
    setPaquetes: (state, action) => {
      state.paquetes = action.payload;
    },
    setPaquete: (state, action) => {
      state.paquete = action.payload;
    },
    setLoadingPaquetes: (state, action) => {
      state.loadingPaquetes = action.payload;
    },
    setErrorPaquetes: (state, action) => {
      state.errorPaquetes = action.payload;
    },
  },
});

export const { setPaquetes, setPaquete, setLoadingPaquetes, setErrorPaquetes } =
  paqueteSlice.actions;

export default paqueteSlice.reducer;

export const fetchPaquetes = (tipo) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    const response = await axios.get(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/paquetes/${tipo}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    //console.log(response.data)
    dispatch(setPaquetes(response.data));
    dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const createPaquete = (paquete) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    console.log(paquete);
    const response = await axios.post(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/paquetes`,
      paquete,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Paquete creado correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchPaquetes(1));
    //dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const updatePaquete = (paquete) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    console.log(paquete);
    const response = await axios.put(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/paquetes/${paquete.id}`,
      paquete,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Paquete actualizado correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchPaquetes(1));
    //dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const deletePaquete = (id) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    console.log(id);
    await axios.delete(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/paquetes/${id}`,
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Paquete eliminado correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchPaquetes(1));
    //dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const updateServiciosPaquetes = (id, servicios) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    console.log(id, { servicios: servicios });
    const response = await axios.put(
      `${import.meta.env.VITE_SERVICE_ADMIN}/api/paquetes/servicios/${id}`,
      { servicios: servicios },
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Servicios actualizados correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchPaquetes(1));
    //dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};

export const comprarPaquete = (id, paquete) => async (dispatch) => {
  try {
    dispatch(setLoadingPaquetes(true));
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    console.log(id, paquete);
    const response = await axios.post(
      `${import.meta.env.VITE_SERVICE_CLIENT}/api/compra/servicio/${id}`,
      {
        ...paquete,
        addPuntos: 0,
        puntos: 0,
      },
      {
        headers: {
          authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "Paquete comprado correctamente",
      confirmButtonText: "Aceptar",
    });
    dispatch(fetchFacturas(id));
    dispatch(setLoadingPaquetes(false));
  } catch (error) {
    dispatch(setLoadingPaquetes(false));
    dispatch(setErrorPaquetes(true));
    if (error.response.status === 403) {
      Swal.fire({
        icon: "error",
        title: "No tienes permisos para acceder a esta sección",
        text: "Porfavor vuelve a iniciar sesión",
        confirmButtonText: "Aceptar",
      }).then(() => {
        dispatch(setErrorUnauthorized(true));
      });
    }
  }
};
