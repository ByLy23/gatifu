import { configureStore } from "@reduxjs/toolkit";
import usersSlice from "../features/users/usersSlice";
import discountsSlice from "../features/discounts/discountSlice";
import mascotaSlice from "../features/mascotas/mascotaSlice";
import medicamentosSlice from "../features/medicamentos/medicamentosSlice";
import salaSlice from "../features/salas/salaSlice";
import citaSlice from "../features/citas/citaSlice";
import errorSlice from "../features/error/errorSlice";
import pagosSlice from "../features/citas/pagosSlice";
import productosSlice from "../features/productos/productoSlice";
import serviciosSlice from "../features/servicios/servicioSlice";
import paqueteSlice from "../features/paquetes/paqueteSlice";
import facturaSlice from "../features/facturas/facturaSlice";

export const store = configureStore({
  reducer: {
    discounts: discountsSlice,
    users: usersSlice,
    mascotas: mascotaSlice,
    medicamentos: medicamentosSlice,
    salas: salaSlice,
    citas: citaSlice,
    error: errorSlice,
    pagos: pagosSlice,
    productos: productosSlice,
    servicios: serviciosSlice,
    paquetes: paqueteSlice,
    facturas: facturaSlice,
  }
});
