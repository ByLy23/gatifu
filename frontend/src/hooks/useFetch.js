const getFech = async (url) => {
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${sessionStorage.getItem('token')}`,
    },
  }
  const data = await fetch(`${url}`, options)
  const info = await data.json()
  // const response = info.filter((item) => item.id_t_us === 4)
  return info
}

const postFech = async (url, body) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${sessionStorage.getItem('token')}`,
    },
    body: JSON.stringify(body),
  }
  try {
    const data = await fetch(`${url}`, options)
    const info = await data.json()
    return { res: info, status: data.status }
  } catch (e) {
    return null
  }
}
export const useFetch = async (type, url, body = {}) => {
  switch (type) {
    case 'get':
      return await getFech(url)
    case 'post':
      return await postFech(url, body)
    default:
      return null
  }
}
