import { useState, useEffect } from "react";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/esm/Container";
import Button from "react-bootstrap/esm/Button";
import Swal from "sweetalert2";
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";
import { MdDelete } from "react-icons/md";
import { IoPush } from "react-icons/io5";

import {
  deleteDiscount,
  updateDiscount,
} from "../features/discounts/discountSlice";

function TableDiscount({ discounts }) {
  const dispatch = useDispatch();
  const [submit, setSubmit] = useState(false);
  const { loading } = useSelector((state) => state.discounts);

  const handleDelete = (id, nombre) => {
    Swal.fire({
      title: "¿Estas seguro?",
      text: `¿Deseas eliminar el descuento *${nombre}*?`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#20bbcce1",
      cancelButtonColor: "#ff902f",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.value) {
        dispatch(deleteDiscount(id));
      }
    });
  };

  const handleUpdate = (discount) => {
    Swal.fire({
      title: "¿Estas seguro?",
      text: `¿Deseas activar el descuento *${discount.nombre}*?`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#20bbcce1",
      cancelButtonColor: "#ff902f",
      confirmButtonText: "Si, actualizar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.value) {
        dispatch(updateDiscount(discount));
      }
    });
  };

  useEffect(() => {
    if (loading && submit) {
      Swal.fire({
        title: "Cargando...",
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
    } else {
      setSubmit(false);
      Swal.close();
    }
  }, [loading]);

  const style = {
    top: 0,
    left: 0,
    zIndex: 10,
    height: "2.5rem",
    position: "sticky",
    color: "#000",
    backgroundColor: "#fff",
  };

  return (
    <>
      <Container style={{ marginTop: "1rem" }}>
        <h1>Lista de descuentos</h1>
        <div
          style={{
            marginTop: "40px",
            overflowY: "auto",
            height: "calc(85vh - 100px)",
          }}
        >
          <Table hover size="lg">
            <thead>
              <tr>
                <th style={style}>#</th>
                <th style={style}>Nombre</th>
                <th style={style}>Cantidad</th>
                <th style={style}>Estado</th>
                <th style={style}>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {discounts.map((discount, index) => (
                <tr key={discount.id}>
                  <td>{index + 1} </td>
                  <td>{discount.nombre}</td>
                  <td>{discount.cantidad}</td>
                  <td>
                    {discount.estado === 1
                      ? "Activo"
                      : discount.estado === 2
                      ? "Inactivo"
                      : "Eliminado"}
                  </td>
                  <td>
                    <Button
                      style={{
                        backgroundColor: "#fff",
                        borderColor: "#20bbcce1 ",
                        marginRight: "5px",
                        color: "#000",
                      }}
                      onClick={() => {
                        handleUpdate(discount);
                      }}
                      size="sm"
                      disabled={discount.estado === 1}
                    >
                      Activar{" "}
                      <IoPush
                        style={{
                          marginLeft: "5px",
                          fontSize: "1.2rem",
                          color: "#20bbcce1",
                        }}
                      />
                    </Button>
                    <Button
                      size="sm"
                      disabled={discount.estado === 3}
                      style={{
                        marginLeft: "5px",
                        backgroundColor: "#fff",
                        borderColor: "#ff902f",
                        color: "#000",
                      }}
                      onClick={() => handleDelete(discount.id, discount.nombre)}
                    >
                      <MdDelete
                        style={{
                          fontSize: "1.5rem",
                          color: "#ff902f",
                        }}
                      />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Container>
    </>
  );
}

export default TableDiscount;
