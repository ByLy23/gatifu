import { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/esm/Container'
import Modal from 'react-bootstrap/Modal'
import { MdDelete } from 'react-icons/md'
import { BiEdit } from 'react-icons/bi'
import { Button, Accordion, Form } from 'react-bootstrap'
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io'
import { HiCursorClick } from 'react-icons/hi'
import { useDispatch, useSelector } from 'react-redux'
import { MdOutlineClose } from 'react-icons/md'
import { ScaleLoader } from 'react-spinners'
import Swal from 'sweetalert2'
import {
  deletePaquete,
  updatePaquete,
  updateServiciosPaquetes,
} from '../features/paquetes/paqueteSlice'
import ModalCompra from './ModalCompra'

export default function TablePaquetes({ paquetes, admin = false, mascotas = [], id = null }) {
  console.log(paquetes)
  const [paqueteSelect, setPaquete] = useState({
    id: 0,
  })
  const [show, setShow] = useState(false)
  const [showServicios, setShowServicios] = useState(false)
  const [paqueteEdit, setPaqueteEdit] = useState({
    servicios: [],
  })
  const [compra, setCompra] = useState({
    mascota: '0',
    paquete: {},
    total: 0,
    fecha: '',
  })
  const [showCompra, setShowCompra] = useState(false)

  const { loadingPaquetes } = useSelector((state) => state.paquetes)

  const dispatch = useDispatch()

  const handleDelete = (id, nombre) => {
    Swal.fire({
      title: '¿Estás seguro?',
      text: `Estás a punto de eliminar el paquete ${nombre}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#20bbcce1',
      cancelButtonColor: '#ff902f',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deletePaquete(id))
      }
    })
  }

  const style = {
    top: 0,
    left: 0,
    zIndex: 10,
    height: '2.5rem',
    position: 'sticky',
    color: '#000',
    backgroundColor: '#fff',
  }

  const handleSubmit = () => {
    if (compra.mascota === '0') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Seleccione una mascota',
      })
    } else if (compra.fecha === '') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Seleccione una fecha',
      })
    } else if (paqueteSelect.id == 0) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Seleccione un paquete',
      })
    } else {
      setCompra({
        ...compra,
        paquete: paqueteSelect,
        total: paqueteSelect.precio,
      })
      setShowCompra(true)
    }
  }

  return (
    <>
      <Container
        style={{
          marginTop: '20px',
        }}
      >
        <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Paquetes</h2>
        {!admin && (
          <Form>
            <Form.Group>
              <Form.Label>Mascota</Form.Label>
              <Form.Select
                data-test-paquetes="select_mascota"
                onChange={(e) => {
                  setCompra({ ...compra, mascota: e.target.value })
                }}
                id="mascota2"
              >
                <option value={0}>Seleccione una mascota</option>
                {mascotas.map((mascota) => (
                  <option value={mascota.id}>{mascota.nombre}</option>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group style={{ marginTop: '15px', marginBottom: '50px' }}>
              <Form.Label>Fecha </Form.Label>
              <Form.Control
                type="date"
                data-test-paquetes="fecha_cita"
                min={new Date().toISOString().split('T')[0]}
                onChange={(e) => {
                  setCompra({ ...compra, fecha: e.target.value })
                }}
                id="fecha2"
              />
            </Form.Group>
          </Form>
        )}
        {loadingPaquetes ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: '60px',
            }}
          >
            <ScaleLoader color={'#20bbcce1'} loading={loadingPaquetes} height={60} />
          </div>
        ) : (
          <div
            style={{
              marginTop: '20px',
              overflowY: 'auto',
              height: '38rem',
            }}
          >
            <Table>
              <thead>
                <tr
                  style={{
                    fontSize: '1.2rem',
                  }}
                >
                  <th style={style}>#</th>
                  <th style={style}>Nombre</th>
                  <th style={style}>Precio</th>
                  <th style={style}>Descripcion</th>
                  <th style={style}>Servicios</th>
                  <th style={style}>Acciones</th>
                </tr>
              </thead>
              <tbody>
                {paquetes.map((paquete, index) => (
                  <>
                    <tr
                      key={index}
                      style={{
                        backgroundColor: paquete.estado === 0 ? '#FFD6D6' : '#fff',
                      }}
                    >
                      <td>
                        <b>{index + 1}</b>
                      </td>
                      <td>{paquete.nombre}</td>
                      <td>{paquete.precio}</td>
                      <td>{paquete.descripcion}</td>
                      <td>
                        <IoIosArrowDown
                          id={`paquete${index}icon`}
                          style={{
                            cursor: 'pointer',
                            fontSize: '2rem',
                            color: '#000',
                          }}
                          onClick={() => {
                            document.getElementById(`paquete${index}`).style.display = 'contents'
                            document.getElementById(`paquete${index}icon`).style.display = 'none'
                            document.getElementById(`paquete${index}icon2`).style.display = 'block'
                          }}
                        />
                        <IoIosArrowUp
                          style={{
                            cursor: 'pointer',
                            fontSize: '2rem',
                            display: 'none',
                            color: '#ff902f',
                          }}
                          id={`paquete${index}icon2`}
                          onClick={() => {
                            document.getElementById(`paquete${index}`).style.display = 'none'
                            document.getElementById(`paquete${index}icon`).style.display = 'block'
                            document.getElementById(`paquete${index}icon2`).style.display = 'none'
                          }}
                        />
                      </td>
                      {admin ? (
                        <>
                          <td>
                            <Button
                              size="sm"
                              disabled={paquete.estado === 0}
                              style={{
                                backgroundColor: '#fff',
                                borderColor: '#20bbcce1 ',
                                color: '#000',
                                marginRight: '10px',
                              }}
                              onClick={() => {
                                setPaqueteEdit(paquete)
                                setShow(true)
                              }}
                            >
                              <BiEdit
                                style={{
                                  fontSize: '1.5rem',
                                  color: '#20bbcce1',
                                }}
                              />
                            </Button>
                            <Button
                              disabled={paquete.estado === 0}
                              size="sm"
                              style={{
                                backgroundColor: '#fff',
                                borderColor: '#ff902f ',
                                color: '#000',
                                marginRight: '10px',
                              }}
                              onClick={() => {
                                handleDelete(paquete.id, paquete.nombre)
                              }}
                            >
                              <MdDelete
                                style={{
                                  fontSize: '1.5rem',
                                  color: '#ff902f',
                                }}
                              />
                            </Button>
                          </td>
                        </>
                      ) : (
                        <>
                          <td>
                            <Form.Check
                              type="radio"
                              name="paquete"
                              data-test-paquete={`paquete_${index}`}
                              value={paquete.id}
                              id={`radio${index}`}
                              onChange={() => {
                                setPaquete(paquete)
                              }}
                              label={
                                paqueteSelect.id != paquete.id ? (
                                  <HiCursorClick
                                    style={{
                                      fontSize: '1.4rem',
                                      color: '#767676',
                                    }}
                                  />
                                ) : (
                                  <MdOutlineClose
                                    style={{
                                      fontSize: '1.8rem',
                                      color: '#767676',
                                    }}
                                  />
                                )
                              }
                              onClick={() => {
                                if (paqueteSelect.id == paquete.id) {
                                  setPaquete({
                                    id: 0,
                                  })
                                  document.getElementById(`radio${index}`).checked = false
                                }
                              }}
                            />
                          </td>
                        </>
                      )}
                    </tr>
                    <>
                      <tr id={`paquete${index}`} style={{ display: 'none', width: '100%' }}>
                        <td colSpan="5">
                          <h6 style={{ marginTop: '5px' }}>Servicios de {paquete.nombre}</h6>
                          <Table striped bordered size="sm" style={{ marginTop: '15px' }}>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Precio</th>
                              </tr>
                            </thead>
                            <tbody>
                              {paquete.servicios.map((servicio, index) => (
                                <tr key={index}>
                                  <td>{index + 1}</td>
                                  <td>{servicio.nombre}</td>
                                  <td>{servicio.descripcion}</td>
                                  <td>{servicio.precio}</td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        </td>
                        <td>
                          {admin && (
                            <>
                              <td>
                                <Button
                                  size="sm"
                                  disabled={paquete.estado === 0}
                                  style={{
                                    backgroundColor: '#fff',
                                    borderColor: '#000',
                                    color: '#000',
                                    marginLeft: '2rem',
                                    marginTop: '100%',
                                  }}
                                  onClick={() => {
                                    setPaqueteEdit(paquete)
                                    setShowServicios(true)
                                  }}
                                >
                                  <BiEdit
                                    style={{
                                      fontSize: '1.5rem',
                                      color: '#000',
                                    }}
                                  />
                                </Button>
                              </td>
                            </>
                          )}
                        </td>
                      </tr>
                    </>
                  </>
                ))}
              </tbody>
            </Table>
            {!admin && (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'right',
                  justifyContent: 'right',
                }}
              >
                <Button
                  data-test-paquete="btn_comprar"
                  style={{
                    backgroundColor: '#20bbcce1',
                    borderColor: '#20bbcce1',
                    marginTop: '1rem',
                    color: '#fff',
                  }}
                  onClick={handleSubmit}
                >
                  Comprar paquete
                </Button>
              </div>
            )}
          </div>
        )}
      </Container>
      <ModalEdit
        show={show}
        setShow={setShow}
        paqueteEdit={paqueteEdit}
        setPaqueteEdit={setPaqueteEdit}
      />
      <ModalServicios
        show={showServicios}
        setShow={setShowServicios}
        paqueteEdit={paqueteEdit}
        setPaqueteEdit={setPaqueteEdit}
      />
      <ModalCompra
        show={showCompra}
        handleClose={() => setShowCompra(false)}
        compra={compra}
        tipo={'Paquete'}
        id={id}
        setDisCheck={() => {
          document.getElementById('mascota2').value = '0'
          document.getElementById('fecha2').value = ''
          ;[...document.querySelectorAll('input[type=radio]')].forEach((item) => {
            item.checked = false
          })
          setCompra({ mascota: '0', paquete: {}, total: 0, fecha: '' })
          setPaquete({ id: 0 })
        }}
      />
    </>
  )
}

function ModalEdit({ show, setShow, paqueteEdit, setPaqueteEdit }) {
  const dispatch = useDispatch()
  return (
    <Modal
      show={show}
      centered
      onHide={() => {
        setShow(false)
        setPaqueteEdit({
          servicios: [],
        })
      }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Editar Paquete </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Nombre</Form.Label>
            <Form.Control
              type="text"
              placeholder="Nombre"
              name="nombre"
              value={paqueteEdit.nombre}
              disabled
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Descripcion</Form.Label>
            <Form.Control
              type="text"
              placeholder="Descripcion"
              value={paqueteEdit.descripcion}
              onChange={(e) => {
                setPaqueteEdit({
                  ...paqueteEdit,
                  descripcion: e.target.value,
                })
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Precio</Form.Label>
            <Form.Control
              type="text"
              placeholder="Precio"
              value={paqueteEdit.precio}
              onChange={(e) => {
                setPaqueteEdit({
                  ...paqueteEdit,
                  precio: e.target.value,
                })
              }}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setShow(false)
            setPaqueteEdit({
              servicios: [],
            })
          }}
        >
          Cerrar
        </Button>
        <Button
          style={{
            backgroundColor: '#20bbcce1',
            borderColor: '#20bbcce1',
            color: '#fff',
          }}
          onClick={() => {
            setShow(false)
            dispatch(updatePaquete(paqueteEdit))
            setPaqueteEdit({
              servicios: [],
            })
          }}
        >
          Actualizar Paquete
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

function ModalServicios({ show, setShow, paqueteEdit, setPaqueteEdit }) {
  const dispatch = useDispatch()
  //const [serviciosSelect, setServicios] = useState(paqueteEdit.servicios);
  const { servicios } = useSelector((state) => state.servicios)

  return (
    <Modal
      show={show}
      centered
      onHide={() => {
        setShow(false)
        setPaqueteEdit({
          servicios: [],
        })
      }}
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title>Editar Servicios del Paquete </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form style={{ marginTop: '5px', marginBottom: '25px' }}>
          <Form.Group>
            <Form.Label>Nombre</Form.Label>
            <Form.Control
              type="text"
              placeholder="Nombre"
              name="nombre"
              value={paqueteEdit.nombre}
              disabled
            />
          </Form.Group>
        </Form>
        <Table bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>Precio</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
            {servicios.map((servicio, index) => (
              <tr
                key={index}
                style={{
                  cursor: 'pointer',
                  backgroundColor: paqueteEdit.servicios.find(
                    (servicioSelect) => servicioSelect.id === servicio.id
                  )
                    ? '#20bbcc65'
                    : '#fff',
                }}
                onClick={() => {
                  if (
                    paqueteEdit.servicios.find(
                      (servicioSelect) => servicioSelect.id === servicio.id
                    )
                  ) {
                    setPaqueteEdit({
                      ...paqueteEdit,
                      servicios: paqueteEdit.servicios.filter(
                        (servicioSelect) => servicioSelect.id !== servicio.id
                      ),
                    })
                  } else {
                    setPaqueteEdit({
                      ...paqueteEdit,
                      servicios: [...paqueteEdit.servicios, servicio],
                    })
                  }
                }}
              >
                <td>{index + 1}</td>
                <td>{servicio.nombre}</td>
                <td>{servicio.descripcion}</td>
                <td>{servicio.precio}</td>
                <td>
                  {paqueteEdit.servicios.find(
                    (servicioSelect) => servicioSelect.id === servicio.id
                  ) ? (
                    <MdOutlineClose
                      style={{
                        fontSize: '1.5rem',
                        color: '#000',
                      }}
                    />
                  ) : (
                    <HiCursorClick
                      style={{
                        fontSize: '1.2rem',
                        color: '#000',
                      }}
                    />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setShow(false)
            setPaqueteEdit({
              servicios: [],
            })
          }}
        >
          Cerrar
        </Button>
        <Button
          style={{
            backgroundColor: '#20bbcce1',
            borderColor: '#20bbcce1',
            color: '#fff',
          }}
          onClick={() => {
            setShow(false)
            dispatch(updateServiciosPaquetes(paqueteEdit.id, paqueteEdit.servicios))
            setPaqueteEdit({
              servicios: [],
            })
          }}
        >
          Actualizar Servicios
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
