import { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/esm/Container'
import { Form, Card } from 'react-bootstrap'
import Button from 'react-bootstrap/esm/Button'
import Swal from 'sweetalert2'
import Spinner from 'react-bootstrap/Spinner'
import { useDispatch, useSelector } from 'react-redux'
import { HiCursorClick } from 'react-icons/hi'
import { MdOutlineClose } from 'react-icons/md'
import ModalCompra from './ModalCompra'
import AlertDismissible from './Alert'

function TableServices({
  products,
  admin = false,
  setLista,
  vaciar = false,
  setVaciar,
  mascotas = [], // para el cliente
  id = null, // para el cliente
  user = {
    codigo: 0,
  }, // para el cliente
}) {
  const [listaServicios, setListaServicios] = useState([])
  const [compra, setCompra] = useState({
    mascota: '0',
    servicios: [],
    total: 0,
    fecha: '',
  })
  const [show, setShow] = useState(false)

  const style = {
    top: 0,
    left: 0,
    zIndex: 10,
    height: '2.5rem',
    position: 'sticky',
    color: '#000',
    backgroundColor: '#fff',
  }

  useEffect(() => {
    if (vaciar) {
      setListaServicios([])
      setVaciar(false)
    }
  }, [vaciar])

  return (
    <>
      <Container>
        {!admin && (
          <Form>
            {user.codigo === 0 && (
              <AlertDismissible
                variant="warning"
                message="Presente codigo de lealtad para canjear y obtener puntos ➜"
              />
            )}
            <Form.Group>
              <Form.Label>Mascota</Form.Label>
              <Form.Select
                data-test-servicios="select_mascota"
                onChange={(e) => {
                  setCompra({ ...compra, mascota: e.target.value })
                }}
                id="mascota"
              >
                <option value={0}>Seleccione una mascota</option>
                {mascotas.map((mascota) => (
                  <option value={mascota.id}>{mascota.nombre}</option>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group style={{ marginTop: '15px', marginBottom: '20px' }}>
              <Form.Label>Fecha </Form.Label>
              <Form.Control
                type="date"
                data-test-servicios="fecha_cita"
                min={new Date().toISOString().split('T')[0]}
                onChange={(e) => {
                  setCompra({ ...compra, fecha: e.target.value })
                }}
                id="fecha"
              />
            </Form.Group>
          </Form>
        )}
        <div
          style={{
            marginTop: '10px',
            overflowY: 'auto',
            height: '450px',
          }}
        >
          <Table hover size="md">
            <thead>
              <tr>
                <th style={style}>#</th>
                <th style={style}>Nombre</th>
                <th style={style}>Precio</th>
                {user.codigo !== 0 && (
                  <th style={{ ...style, width: '10rem' }}>Precio por puntos</th>
                )}
                <th style={style}>Descripcion</th>
                {user.codigo !== 0 && <th style={{ ...style, width: '10rem' }}>Puntos a ganar</th>}
                <th style={style}>Seleccionar</th>
                {user.codigo !== 0 && (
                  <th style={style}>
                    <text
                      style={{
                        visibility: listaServicios.length === 0 ? 'hidden' : 'visible',
                      }}
                    >
                      Cantidad de puntos a Canjear
                    </text>
                  </th>
                )}
              </tr>
            </thead>
            <tbody>
              {products.map((product, index) => (
                <tr
                  key={index}
                  style={{
                    backgroundColor: listaServicios
                      .map((servicio) => servicio.id)
                      .includes(product.id)
                      ? '#ff902f79'
                      : 'white',
                  }}
                >
                  <td>{index + 1}</td>
                  <td>{product.nombre}</td>
                  <td>{product.precio}</td>
                  {user.codigo !== 0 && (
                    <td style={{ textAlign: 'center' }}>{product.ptsPrecio}</td>
                  )}
                  <td>{product.descripcion}</td>
                  {user.codigo !== 0 && (
                    <td style={{ textAlign: 'center' }}> {product.ptsGanar}</td>
                  )}
                  {listaServicios.map((servicio) => servicio.id).includes(product.id) ? (
                    <>
                      <td
                        onClick={() => {
                          if (listaServicios.map((servicio) => servicio.id).includes(product.id)) {
                            setListaServicios(
                              listaServicios.filter((item) => item.id !== product.id)
                            )
                            if (admin) {
                              setLista(listaServicios.filter((item) => item !== product))
                            }
                          } else {
                            setListaServicios([...listaServicios, product])
                            if (admin) {
                              setLista([...listaServicios, product])
                            }
                          }
                        }}
                        style={{ cursor: 'pointer' }}
                      >
                        <MdOutlineClose
                          style={{
                            fontSize: '1.8rem',
                            color: '#ffff',
                          }}
                        />
                      </td>
                      {user.codigo !== 0 && (
                        <td>
                          <Form.Control
                            type="number"
                            min={1}
                            max={product.ptsPrecio}
                            defaultValue={0}
                            onChange={(e) => {
                              if (e.target.value > product.ptsPrecio) {
                                e.target.value = product.ptsPrecio
                              }
                              setListaServicios(
                                listaServicios.map((item) => {
                                  if (item.id === product.id) {
                                    return {
                                      ...item,
                                      cantidad: e.target.value,
                                    }
                                  }
                                  return item
                                })
                              )
                            }}
                          />
                        </td>
                      )}
                    </>
                  ) : (
                    <>
                      <td
                        onClick={() => {
                          if (listaServicios.map((servicio) => servicio.id).includes(product.id)) {
                            setListaServicios(
                              listaServicios.filter((item) => item.id !== product.id)
                            )
                            if (admin) {
                              setLista(listaServicios.filter((item) => item !== product))
                            }
                          } else {
                            setListaServicios([...listaServicios, product])
                            if (admin) {
                              setLista([...listaServicios, product])
                            }
                          }
                        }}
                        data-test-servicio={`servicio_servicio${index + 1}`}
                        style={{ cursor: 'pointer' }}
                      >
                        <HiCursorClick
                          style={{
                            color: '#ff902f',
                            fontSize: '1.5rem',
                          }}
                        />
                      </td>
                      <td></td>
                    </>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <div
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              marginTop: '1rem',
            }}
          >
            <div>
              <h3>Total:</h3>{' '}
            </div>
            <div>
              <h3>
                {listaServicios.reduce(
                  (acumulador, item) => acumulador + parseFloat(item.precio),
                  0
                )}
              </h3>
            </div>
          </div>
          {!admin && (
            <div
              style={{
                display: 'flex',
                alignItems: 'right',
                justifyContent: 'right',
                marginBottom: '1.5rem',
              }}
            >
              <Button
                style={{
                  backgroundColor: '#20bbcce1',
                  borderColor: '#20bbcce1',
                  marginTop: '1rem',
                  color: '#fff',
                }}
                data-test-servicio="btn_comprar"
                onClick={() => {
                  if (compra.mascota === '0') {
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Seleccione una mascota',
                    })
                  } else if (compra.fecha === '') {
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Seleccione una fecha',
                    })
                  } else if (listaServicios.length === 0) {
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Seleccione al menos un servicio',
                    })
                  } else {
                    let sumaPuntos = 0
                    if (user.codigo !== 0) {
                      sumaPuntos = listaServicios.reduce(
                        (acumulador, item) => acumulador + parseFloat(item.cantidad),
                        0
                      )
                      if (sumaPuntos > user.puntos) {
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'No tiene suficientes puntos',
                        })
                        return
                      }
                    }
                    let addPuntos = 0
                    if (user.codigo !== 0) {
                      addPuntos = listaServicios.reduce(
                        (acumulador, item) => acumulador + parseFloat(item.ptsGanar),
                        0
                      )
                    }
                    let total = 0
                    setCompra({
                      ...compra,
                      servicios: listaServicios.map((item) => {
                        if (user.codigo === 0 || item.cantidad === 0) {
                          total += parseFloat(item.precio)
                          return {
                            ...item,
                            resultado: item.precio,
                          }
                        }
                        let res = parseFloat(
                          item.precio - (item.precio / item.ptsPrecio) * item.cantidad
                        ).toFixed(2)
                        total += parseFloat(res)
                        return {
                          ...item,
                          resultado: res,
                        }
                      }),
                      total: total,
                      addPuntos: addPuntos,
                      puntos: sumaPuntos,
                    })
                    setShow(true)
                  }
                }}
              >
                Comprar
              </Button>
            </div>
          )}
        </div>
      </Container>
      <ModalCompra
        show={show}
        handleClose={() => setShow(false)}
        compra={compra}
        tipo={'Servicios'}
        setLista={setListaServicios}
        id={id}
        user={user}
        setDisCheck={() => {
          document.getElementById('mascota').value = '0'
          document.getElementById('fecha').value = ''
          setCompra({ mascota: '0', servicios: [], total: 0, fecha: '' })
        }}
      />
    </>
  )
}

export default TableServices
