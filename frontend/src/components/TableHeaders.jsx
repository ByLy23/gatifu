import React from 'react'

export const TableHeaders = ({ headers }) => {
  return (
    <thead>
      <tr>
        {headers.map((nombre, index) => (
          <th key={index}>{nombre}</th>
        ))}
      </tr>
    </thead>
  )
}
