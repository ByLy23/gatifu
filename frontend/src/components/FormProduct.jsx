import { useState, useEffect } from "react";
import { Form, Container, Button, Spinner } from "react-bootstrap";
import AlertDismissible from "./Alert";
import { useDispatch, useSelector } from "react-redux";
import {
  createProducto,
  setErrorProducto,
  updateProducto,
  setProducto
} from "../features/productos/productoSlice";

function FormProduct({ marcas, categorias, setTab, SetEdit, boleano = false }) {
  const { producto } = useSelector((state) => state.productos);

  const [NewProduct, setNewProduct] = useState({
    nombre: "",
    precio: "",
    descripcion: "",
    stock: "",
    marca: "0",
    categoria: "0",
  });

  useEffect(() => {
    if (producto && boleano) {
      setNewProduct({
        nombre: producto.nombre,
        precio: producto.precio,
        descripcion: producto.descripcion,
        stock: producto.stock,
        marca: producto.id_marca,
        categoria: producto.id_categoria,
      });
    }
  }, [producto]);

  const { loadingProducto } = useSelector((state) => state.productos);
  const { errorProducto } = useSelector((state) => state.productos);
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState("");
  const [submit, setSubmit] = useState(false);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    setNewProduct({
      ...NewProduct,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      NewProduct.nombre === "" ||
      NewProduct.precio === "" ||
      NewProduct.descripcion === "" ||
      NewProduct.stock === ""
    ) {
      setShow(true);
      setMessage("Todos los campos son obligatorios");
      setTimeout(() => {
        setShow(false);
      }, 5000);
      return;
    } else if (NewProduct.precio <= 0) {
      setShow(true);
      setMessage("El precio debe ser mayor a 0");
      setTimeout(() => {
        setShow(false);
      }, 5000);
      return;
    } else if (NewProduct.stock <= 0) {
      setShow(true);
      setMessage("El stock debe ser mayor a 0");
      setTimeout(() => {
        setShow(false);
      }, 5000);
      return;
    } else if (NewProduct.marca === "0") {
      setShow(true);
      setMessage("Debe seleccionar una marca");
      setTimeout(() => {
        setShow(false);
      }, 5000);
      return;
    } else if (NewProduct.categoria === "0") {
      setShow(true);
      setMessage("Debe seleccionar una categoria");
      setTimeout(() => {
        setShow(false);
      }, 5000);
      return;
    }
    setSubmit(true);
    if (boleano) {
      dispatch(updateProducto(producto.id, NewProduct));
    } else {
      console.log(NewProduct);
      dispatch(createProducto(NewProduct));
    }
  };

  useEffect(() => {
    if (submit && !loadingProducto) {
      if (errorProducto) {
        setShow(true);
        setMessage("Error al enviar producto");
        setTimeout(() => {
          setShow(false);
        }, 5000);
        dispatch(setErrorProducto(null));
      } else {
        setNewProduct({
          nombre: "",
          precio: "",
          descripcion: "",
          stock: "",
          categoria: "0",
          marca: "0",
        });
        setTab();
        SetEdit();
      }
      setSubmit(false);
    }
  }, [loadingProducto]);

  return (
    <Container style={{ marginTop: "2rem" }}>
      {boleano && (
        <div
          style={{
            float: "right",
          }}
        >
          <Button
            size="xl"
            variant="outline-danger"
            aria-label="Hide"
            onClick={() => {
              setTab();
              SetEdit();
              setProducto(null);
            }}
          >
            ❌
          </Button>
        </div>
      )}
      <h1>{boleano ? "Editar Producto" : "Crear Producto"} </h1>
      {show && (
        <AlertDismissible onHide={() => setShow(false)} message={message} />
      )}
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            disabled={boleano}
            placeholder="Ingrese el nombre del producto"
            name="nombre"
            value={NewProduct.nombre}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Precio</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el precio del producto"
            name="precio"
            value={NewProduct.precio}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingrese la descripcion del producto"
            name="descripcion"
            value={NewProduct.descripcion}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Stock</Form.Label>
          <Form.Control
            type="number"
            placeholder="Ingrese el stock del producto"
            disabled={boleano}
            name="stock"
            value={NewProduct.stock}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Marca</Form.Label>
          <Form.Select
            name="marca"
            value={NewProduct.marca}
            onChange={handleChange}
          >
            <option value="0">Seleccione una marca</option>
            {marcas.map((marca) => (
              <option key={marca.id} value={marca.id}>
                {marca.nombre}
              </option>
            ))}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Categoria</Form.Label>
          <Form.Select
            name="categoria"
            value={NewProduct.categoria}
            onChange={handleChange}
          >
            <option value="0">Seleccione una categoria</option>
            {categorias.map((categoria) => (
              <option key={categoria.id} value={categoria.id}>
                {categoria.nombre}
              </option>
            ))}
          </Form.Select>
        </Form.Group>
        <div
          style={{
            marginTop: "40px",
            display: "flex",
            alignItems: "right",
            justifyContent: "right",
          }}
        >
          <Button
            style={{ backgroundColor: "#20bbcce1", borderColor: "#20bbcce1 " }}
            size="lg"
            onClick={handleSubmit}
            disabled={loadingProducto}
          >
            {loadingProducto ? (
              <Spinner animation="border" variant="light" />
            ) : boleano ? (
              "Editar Producto"
            ) : (
              "Crear Producto"
            )}
          </Button>
        </div>
      </Form>
    </Container>
  );
}

export default FormProduct;
