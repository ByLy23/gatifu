import React from "react";
import Alert from "react-bootstrap/Alert";

function AlertDismissible({onHide, message, variant = "danger"}) {
  return (
    <Alert variant={variant} onClose={onHide} dismissible>
      {message}
    </Alert>
  );
}

export default AlertDismissible;
