import { Modal, Button, Table } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { comprarProducto } from '../features/productos/productoSlice'
import { comprarServicio } from '../features/servicios/servicioSlice'
import { comprarPaquete } from '../features/paquetes/paqueteSlice'
import Moment from 'moment'
import { fetchFacturas } from '../features/facturas/facturaSlice'

function ModalCompra({
  show,
  handleClose,
  compra,
  tipo,
  setLista = () => {},
  setDisCheck,
  id,
  user = {},
}) {
  const dispatch = useDispatch()

  const handleSubmit = () => {
    if (tipo === 'Productos') {
      dispatch(comprarProducto(id, compra))
    } else if (tipo === 'Servicios') {
      dispatch(
        comprarServicio(
          id,
          {
            ...compra,
            fecha: Moment(compra.fecha).format('DD-MM-YYYY'),
          },
          user
        )
      )
    } else if (tipo === 'Paquete') {
      dispatch(
        comprarPaquete(id, {
          ...compra,
          fecha: Moment(compra.fecha).format('DD-MM-YYYY'),
          paquete: [compra.paquete],
        })
      )
    }
    dispatch(fetchFacturas(id))
    setLista([])
    handleClose()
    setDisCheck()
  }

  return (
    <Modal show={show} onHide={handleClose} size="lg" centered>
      <Modal.Header closeButton>
        <Modal.Title>Resumen de la compra de {tipo} </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {tipo !== 'Productos' && (
          <div
            style={{
              marginTop: '0.5rem',
              marginBottom: '0.5rem',
              textAlign: 'center',
            }}
          >
            {/*<h6>Mascota: {compra.mascota}</h6>*/}
            <h6>Fecha: {compra.fecha}</h6>
          </div>
        )}
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Precio</th>
              {tipo === 'Productos' ? (
                <th>Cantidad</th>
              ) : tipo === 'Paquete' ? (
                <th>Cantidad de Servicios</th>
              ) : compra.addPuntos !== 0 ? (
                <>
                  <th>Puntos a canjear</th>
                  <th>Resultado del nuevo Precio</th>
                </>
              ) : null}
            </tr>
          </thead>
          <tbody>
            {tipo === 'Productos' ? (
              compra.productos.map((item, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.nombre}</td>
                  <td>{item.precio}</td>
                  <td>{item.cantidad}</td>
                </tr>
              ))
            ) : tipo === 'Servicios' ? (
              compra.servicios.map((item, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.nombre}</td>
                  <td>{item.precio}</td>
                  {compra.addPuntos !== 0 ? (
                    <>
                      <td>{item.cantidad}</td>
                      <td>{item.resultado}</td>
                    </>
                  ) : null}
                </tr>
              ))
            ) : (
              <tr>
                <td>1</td>
                <td>{compra.paquete.nombre}</td>
                <td>{compra.paquete.precio}</td>
                <td>{compra.paquete.servicios ? compra.paquete.servicios.length : 0}</td>
              </tr>
            )}
          </tbody>
        </Table>
        <div
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              marginTop: '1rem',
            }}
          >
            <div>
              <h6>Total:</h6>{' '}
            </div>
            <div>
              <h6>{compra.total}</h6>
            </div>
          </div>
          {compra.addPuntos !== 0 ? (
            <>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginTop: '1rem',
                }}
              >
                <div>
                  <h6>Puntos a ganar:</h6>{' '}
                </div>
                <div>
                  <h6>{compra.addPuntos}</h6>
                </div>
              </div>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginTop: '1rem',
                }}
              >
                <div>
                  <h6>Puntos a canjear:</h6>{' '}
                </div>
                <div>
                  <h6>{compra.puntos}</h6>
                </div>
              </div>
            </>
          ) : null}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cerrar
        </Button>
        <Button data-test-paquete="btn_confirmar" variant="dark" onClick={handleSubmit}>
          Confirmar
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ModalCompra
