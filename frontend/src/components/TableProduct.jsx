import { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/esm/Container'
import { Form, Row, Col, Modal } from 'react-bootstrap'
import Button from 'react-bootstrap/esm/Button'
import Swal from 'sweetalert2'
import { useDispatch, useSelector } from 'react-redux'
import AlertDismissible from './Alert'
import { deleteProducto, setProducto, updateStock } from '../features/productos/productoSlice'
import { MdDelete } from 'react-icons/md'
import { BiEdit } from 'react-icons/bi'
import { HiCursorClick } from 'react-icons/hi'
import { RiEditCircleFill } from 'react-icons/ri'
import ModalCompra from './ModalCompra'

function TableProduct({ productos, setEdit, setTab, cliente = false, id = 0 }) {
  const dispatch = useDispatch()
  const [submit, setSubmit] = useState(false)
  const { loadingProducto } = useSelector((state) => state.productos)
  const [selectProductos, setSelectProductos] = useState([])
  const [show, setShow] = useState(false)
  const [message, setMessage] = useState('')
  const [productosStock, setProductosStock] = useState({})
  const [showModal, setShowModal] = useState(false)
  const [showCompra, setShowCompra] = useState(false)
  const [compra, setCompra] = useState({
    productos: [],
    servicios: [],
  })
  const [variant, setVariant] = useState('danger')

  const handleDelete = (id, nombre) => {
    Swal.fire({
      title: '¿Estas seguro?',
      text: `¿Deseas eliminar el producto *${nombre}*?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#20bbcce1',
      cancelButtonColor: '#ff902f',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        dispatch(deleteProducto(id))
      }
    })
  }

  const handleUpdate = (product) => {
    dispatch(setProducto(product))
    setEdit()
    setTab()
  }

  useEffect(() => {
    if (loadingProducto && submit) {
      Swal.fire({
        title: 'Cargando...',
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading()
        },
        willClose: () => {
          setSubmit(false)
        },
      })
    }
  }, [loadingProducto, submit])

  const style = {
    top: 0,
    left: 0,
    zIndex: 10,
    height: '2.5rem',
    position: 'sticky',
    color: '#000',
    backgroundColor: '#fff',
  }

  return (
    <Container style={{ marginTop: '1rem' }}>
      <h2 style={{ marginTop: '1.5rem', textAlign: 'center' }}>Lista de productos</h2>
      {show && (
        <div
          style={{
            marginTop: '24px',
          }}
        >
          <AlertDismissible onHide={() => setShow(false)} variant={variant} message={message} />
        </div>
      )}
      <div
        style={{
          marginTop: '40px',
          overflowY: 'auto',
          height: 'calc(85vh - 100px)',
        }}
      >
        <Table size="lg">
          <thead>
            <tr>
              <th style={style}>#</th>
              <th style={style}>Nombre</th>
              <th style={style}>Descripcion</th>
              <th style={style}>Precio</th>
              <th style={style}>Stock</th>
              <th style={style}>Marca</th>
              <th style={style}>Categoria</th>
              <th style={style}>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {productos.map((product, index) => (
              <tr
                key={product.id}
                style={{
                  backgroundColor: product.estado === 0 ? '#FFD6D6' : '#fff',
                }}
              >
                <td>{index + 1}</td>
                <td>{product.nombre}</td>
                <td>{product.descripcion}</td>
                <td
                  style={{
                    fontSize: '1.3rem',
                  }}
                >
                  {product.precio}
                </td>

                <td>
                  {product.stock}
                  {!cliente && (
                    <Button
                      variant="outline-black"
                      style={{ marginLeft: '10px' }}
                      size="sm"
                      disabled={loadingProducto || product.estado === 0}
                      onClick={() => {
                        setProductosStock(product)
                        setShowModal(true)
                      }}
                    >
                      <RiEditCircleFill />
                    </Button>
                  )}
                </td>
                <td>{product.marca}</td>
                <td>{product.categoria}</td>
                <td>
                  {cliente ? (
                    <>
                      <Form.Check
                        type="checkbox"
                        data-test-producto={`producto_${product.id}`}
                        label={
                          <HiCursorClick
                            style={{
                              fontSize: '1.5rem',
                              color: selectProductos.find((item) => item.id === product.id)
                                ? '#fff'
                                : '#FF8419',
                            }}
                          />
                        }
                        onChange={(e) => {
                          if (e.target.checked) {
                            setSelectProductos([
                              ...selectProductos,
                              {
                                id: product.id,
                                nombre: product.nombre,
                                precio: product.precio,
                              },
                            ])
                          } else {
                            setSelectProductos(
                              selectProductos.filter((item) => item.id !== product.id)
                            )
                          }
                        }}
                      />
                      {selectProductos.find((producto) => producto.id === product.id) && (
                        <Form.Control
                          type="number"
                          min="1"
                          size="sm"
                          data-test-producto={`txtproducto_${product.id}`}
                          placeholder="Cantidad"
                          onChange={(e) => {
                            if (e.target.value > product.stock) {
                              setMessage(
                                `No hay suficiente stock para el producto *${product.nombre}*`
                              )
                              setShow(true)
                              setVariant('warning')
                              setTimeout(() => {
                                setShow(false)
                              }, 5000)
                              e.target.value = product.stock
                            }
                            setSelectProductos(
                              selectProductos.map((item) =>
                                item.id === product.id
                                  ? { ...item, cantidad: e.target.value }
                                  : item
                              )
                            )
                          }}
                          style={{
                            borderColor: selectProductos.find(
                              (item) => item.id === product.id && !item.cantidad
                            )
                              ? 'red'
                              : '#ced4da',
                          }}
                        />
                      )}
                    </>
                  ) : (
                    <>
                      <Button
                        disabled={loadingProducto || product.estado === 0}
                        onClick={() => handleUpdate(product)}
                        size="sm"
                        style={{
                          backgroundColor: '#fff',
                          borderColor: '#20bbcce1 ',
                          color: '#000',
                          marginRight: '10px',
                        }}
                      >
                        <BiEdit
                          style={{
                            fontSize: '1.5rem',
                            color: '#20bbcce1',
                          }}
                        />
                      </Button>
                      <Button
                        disabled={loadingProducto || product.estado === 0}
                        onClick={() => handleDelete(product.id, product.nombre)}
                        size="sm"
                        style={{
                          backgroundColor: '#fff',
                          borderColor: '#ff902f ',
                          color: '#000',
                          marginRight: '10px',
                        }}
                      >
                        <MdDelete
                          style={{
                            fontSize: '1.5rem',
                            color: '#ff902f',
                          }}
                        />
                      </Button>
                    </>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        {cliente && (
          <>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                marginTop: '2rem',
              }}
            >
              <div>
                <h3>Total:</h3>
              </div>
              <div>
                <h3>
                  {selectProductos
                    .reduce(
                      (acc, item) =>
                        acc + item.cantidad * productos.find((p) => p.id === item.id).precio,
                      0
                    )
                    .toFixed(2)}
                </h3>
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                alignItems: 'right',
                justifyContent: 'right',
              }}
            >
              <Button
                style={{
                  backgroundColor: '#20bbcce1',
                  borderColor: '#20bbcce1',
                  marginTop: '1rem',
                  color: '#fff',
                }}
                data-test-producto="btn_comprar"
                onClick={() => {
                  if (selectProductos.length === 0) {
                    setMessage('Debe seleccionar al menos un producto')
                    setShow(true)
                    setVariant('danger')
                    setTimeout(() => {
                      setShow(false)
                    }, 5000)
                    return
                  }
                  if (selectProductos.find((item) => !item.cantidad)) {
                    setMessage('Debe ingresar la cantidad de los productos')
                    setShow(true)
                    setVariant('danger')
                    setTimeout(() => {
                      setShow(false)
                    }, 5000)
                    return
                  }
                  setCompra({
                    productos: selectProductos,
                    servicios: [],
                    total: selectProductos
                      .reduce(
                        (acc, item) =>
                          acc + item.cantidad * productos.find((p) => p.id === item.id).precio,
                        0
                      )
                      .toFixed(2),
                  })
                  setShowCompra(true)
                }}
              >
                Comprar
              </Button>
            </div>
          </>
        )}
      </div>
      <ModalStock show={showModal} onHide={() => setShowModal(false)} product={productosStock} />
      <ModalCompra
        show={showCompra}
        handleClose={() => setShowCompra(false)}
        compra={compra}
        tipo={'Productos'}
        setLista={setSelectProductos}
        setDisCheck={() =>
          [...document.querySelectorAll('input[type=checkbox]')].forEach((item) => {
            item.checked = false
          })
        }
        id={id}
      />
    </Container>
  )
}

function ModalStock({ product = { stock: 0 }, show, onHide }) {
  const [result, setResult] = useState(0)
  const [newStock, setNewStock] = useState(0)
  const [operacion, setOperacion] = useState('0')

  const dispatch = useDispatch()

  const close = () => {
    setResult(0)
    setNewStock(0)
    document.getElementById('operacion').value = '0'
    setOperacion('0')
    onHide()
  }

  return (
    <Modal show={show} onHide={close} centered size="xl">
      <Modal.Header closeButton>
        <Modal.Title>Actualizar Stock de {product.nombre}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Stock Actual</Form.Label>
                <Form.Control type="number" value={product.stock} disabled size="sm" />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Sumar o Restar</Form.Label>
                <Form.Select
                  size="sm"
                  id="operacion"
                  onChange={(e) => {
                    setOperacion(e.target.value)
                    setNewStock(0)
                    setResult(0)
                  }}
                >
                  <option value="0">Seleccionar operacion</option>
                  <option value="Sumar">+</option>
                  <option value="Restar">-</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>
                  {operacion !== '0' ? `Cantidad a ${operacion}` : 'Cantidad'}
                </Form.Label>
                <Form.Control
                  type="number"
                  size="sm"
                  value={newStock}
                  disabled={operacion === '0'}
                  onChange={(e) => {
                    setNewStock(e.target.value)
                    setResult(
                      operacion === 'Sumar'
                        ? parseInt(product.stock) + parseInt(e.target.value)
                        : operacion === 'Restar'
                        ? parseInt(product.stock) - parseInt(e.target.value)
                        : 0
                    )
                  }}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Resultado</Form.Label>
                <Form.Control
                  type="number"
                  size="sm"
                  value={result}
                  disabled
                  style={{
                    borderColor: result <= 0 ? '#ff902f' : '#ced4da',
                  }}
                />
              </Form.Group>
            </Col>
          </Row>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={close}>
          Cerrar
        </Button>
        <Button
          style={{
            backgroundColor: '#20bbcce1',
            borderColor: '#20bbcce1',
          }}
          onClick={() => {
            if (operacion !== '0' && result > 0 && newStock > 0) {
              dispatch(updateStock({ id: product.id, stock: result }))
              close()
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Debe seleccionar una operacion y el resultado debe ser mayor a 0',
              })
            }
          }}
        >
          Actualizar
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default TableProduct
