const db = require('../db/database');
const Cita = require('./cita.class.js');

/**
 * Cita normal
 * @extends { Cita }
 * 
 */

class citaNormal extends Cita{
    
    /**
     * @params
     * { fecha : fecha de la cita }
     * { idMascota: mascota a la que se le creara la cita }
     * { tipo: tipo de cita que se le creara
     *   1: Cita Normal
     *   2: Cita Emergencia }
     */
    async create(fecha,idMascota,tipo,puntos,addPuntos){
        try {
            //console.log("+++++++++++++++++++++++CREAR CITA++++++++++++++++++++++++++++++++++++++");
            let sql = `CALL crearCita('${fecha}',${idMascota},${tipo},${puntos},${addPuntos})`;
            const result = await db.query(sql);
            const { idCita } = result[0][0]
            if( idCita == null){
                return 0
            }
            //console.log("-----------------------FIN CREAR CITA--------------------------------------"); 
            return idCita;
        } catch (error) {
            console.log(error);
            return error;
        }
    }

    async agregarMotivo(motivos,idCita){
        try {
            //console.log("+++++++++++++++++++++++AGREGAR MOTIVO++++++++++++++++++++++++++++++++++++++");
            //console.log(motivos);
            for(let index = 0; index < motivos.length; index++){
                var motivo = motivos[index];
                console.log(motivo);
                const resultMotivo = await db.query(
                    `CALL agregarMotivo ('${motivo.inicio}',${motivo.idMotivo},${motivo.idSala},${idCita},${motivo.idMedico},${motivo.resultado},${motivo.cantidad})`
                );
                console.log(resultMotivo[0][0].resultado);
            }

            //console.log("-----------------------FIN AGREGAR MOTIVO--------------------------------------");
        } catch (error) {
            console.log(error);
            return error;
        }
    }

    async 

}

module.exports = citaNormal;

