/**
 * Abstract Class Cita
 */

class Cita {
  constructor() {
    if (this.constructor == Cita) {
      throw new Error("Abstract classes can't be instantiated!!!");
    }
  }

  create() {
    throw new createTransport("El metodo crear cita debe ser implementado");
  }

  agregarMotivo() {}
}

module.exports = Cita;
