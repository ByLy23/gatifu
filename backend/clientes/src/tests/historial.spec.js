const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("POST /historyCitas", () => {
  // obtenter token de autenticacion para el usuario
  let token = "";

  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        // datos de usuario medico
        usuarioIN: "po",
        pass: SHA256("po").toString(),
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.accessToken; // save the token!
        done();
      });
  });

  // test to get historyCitas
  it("should return 200 OK", () => {
    return req("http://localhost:3002")
      .get("/api/citas/history/17/Cliente/nulo") // idCliente = 17
      .set("Authorization", `Bearer ${token}`)
      .expect(200);
  });

  // test to get history with error
  it("should return 400 Bad Request", () => {
    return req("http://localhost:3002")
      .get("/api/citas/history/17") // idCliente = 17
      .set("Authorization", `Bearer ${token}`)
      .expect(400);
  });

  // test to get history with error
  /*it("should return 401 Unauthorized", () => {
    return req("http://localhost:3002")
      .get("/api/citas/history/17/Cliente/nulo") // idCliente = 17
      .expect(401);
  });*/
});
