const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("POST /adquirirServicio", () => {
  // obtenter token de autenticacion para el usuario
  let token = "";

  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        // datos de usuario medico
        usuarioIN: "po",
        pass: SHA256("po").toString(),
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.accessToken; // save the token!
        done();
      });
  });

  // test to post adquirirServicio
  it("should return 200 OK", () => {
    return req("http://localhost:3002")
      .post("/api/compra/servicio/17")
      .set("Authorization", `Bearer ${token}`)
      .send({
        total: 100,
        mascota: 14,
        fecha: "24-12-2022",
        puntos: 0,
        addPuntos: 0,
        servicios: [
          {
            id: 1,
            resultado: 100,
            cantidad: 0,
          },
        ],
      })
      .expect(200);
  });

  it("should return 500 error", () => {
    return req("http://localhost:3002")
      .post("/api/compra/servicio/17")
      .set("Authorization", `Bearer ${token}`)
      .send({
        total: 100,
        mascota: 14,
        fecha: "24-12-2022",
        servicios: [
          {
            id: 1,
            resultado: 100,
            cantidad: 0,
          },
        ],
      })
      .expect(500);
  });

  // test to post adquirirServicio with error
  /*it("should return 401 Unauthorized", () => {
        return req("http://localhost:3002")
        .post("/api/compra/servicio/17")
      .set("Authorization", `Bearer ${token}`)
      .send({
        total: 100,
        mascota: 14,
        fecha: "24-12-2022",
        puntos: 0,
        addPuntos: 0,
        servicios: [{
            id: 1,
            resultado: 100,
            cantidad: 0
        }]
      })
        .expect(401);
    });*/
});
