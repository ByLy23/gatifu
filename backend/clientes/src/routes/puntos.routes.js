const express  = require("express");
const router = express.Router();

const puntos = require('../controllers/puntos.controllers');

router.get('/:id', puntos.getPuntos);

module.exports = router