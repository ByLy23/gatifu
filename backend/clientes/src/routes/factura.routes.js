const express = require('express')
const router = express.Router()

const factura = require('../controllers/factura.controllers')

router.get('/:id', factura.getFactura)
router.get('/detalle/:id', factura.getDetalleFactura)

module.exports = router
