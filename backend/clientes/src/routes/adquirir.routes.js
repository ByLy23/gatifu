var express = require("express");
var router = express.Router();
const adquirirContext = require("../adquirir_strategy/adquirirContext");
const productoStrategy = require("../adquirir_strategy/productoStrategy");
const servicioStrategy = require("../adquirir_strategy/servicioStrategy");

router.post("/producto/:id", async (req, res, next) => {
  try {
    const data = req.body;
    const id = req.params;
    const adquirir = new adquirirContext(
      new productoStrategy(),
      data.total,
      id.id,
      data.productos,
      0,
      0
    );
    const respuesta = await adquirir.adquirirProdServ();
    req.data = { message: "Producto Comprado!" };
    req.error = "No";
    next();
    return res.json({ message: respuesta });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
});

router.post("/servicio/:id", async (req, res, next) => {
  try {
    const data = req.body;
    const id = req.params;
    let listado = null;
    if (data.servicios != undefined) {
      listado = data.servicios;
    } else {
      listado = data.paquete;
    }

    console.log(listado);
    const adquirir = new adquirirContext(
      new servicioStrategy(data.mascota, data.fecha),
      data.total,
      id.id,
      listado,
      data.puntos,
      data.addPuntos
    );
    const respuesta = await adquirir.adquirirProdServ();
    /*req.data = { message: "Comprado!" };
    req.error = "No";
    next();*/
    return res.json({ message: respuesta });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: error });
  }
});

module.exports = router;
