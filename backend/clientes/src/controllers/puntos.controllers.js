const db = require("../db/database");

exports.getPuntos = async (req, res, next) => {
  try {
    const { id } = req.params;
    req.body = req.params;
    ///console.log(req.params)
    let sql = `CALL obtenerPuntosUsuario(${id})`;
    const result = await db.query(sql);
    req.data = result[0][0];
    req.error = "No";
    next();
    return res.status(200).json(result[0][0]);
  } catch (error) {
    console.log(error);
    req.data = { message: error };
    req.error = "Si";
    next();
    return res.status(400).json({
      message: "Error al obtener los puntos",
      description: error
    });
  }
};
