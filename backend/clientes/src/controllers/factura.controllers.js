const db = require('../db/database')

//getDetalleFactura--- Se va en listar el detalle de una factura especifica
exports.getDetalleFactura = async (req, res, next) => {
  try {
    const { id } = req.params
    req.body = req.params
    let arrayParams = [id]
    if (!notUndefinedNull(arrayParams))
      return res.status(400).json({
        message: 'Error, datos incompletos. Se requiere id Factura',
      })

    const result = await db.query(`CALL obtenerDetalleFactura (${id})`)
    console.log(result)
    req.data = { message: 'Detalle Factura obtenidos con éxito!' }
    req.error = 'No'
    next()
    return res.status(200).json(result[0])
  } catch (error) {
    req.data = {
      message: 'Error al obtener detalle Factura. ',
      descirption: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: 'Error al obtener detalle Factura. ',
      descirption: error,
    })
  }
}

//getFactura--- Se va en listar las facturas de un cliente especifico
exports.getFactura = async (req, res, next) => {
  try {
    const { id } = req.params
    req.body = req.params
    let arrayParams = [id]
    if (!notUndefinedNull(arrayParams))
      return res.status(400).json({
        message: 'Error, datos incompletos. Se requiere id Cliente',
      })

    const result = await db.query(`CALL obtenerFacturas(${id})`)
    console.log(result[0])
    req.data = { message: 'Factura obtenidos con éxito!' }
    req.error = 'No'
    next()
    return res.status(200).json(result[0])
  } catch (error) {
    req.data = {
      message: 'Error al obtener Factura. ',
      descirption: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: 'Error al obtener Factura. ',
      descirption: error,
    })
  }
}

// Verifica que no existan nulos, indefinidos, 0 ó cadenas vacías
function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index]
    if (element == undefined || element == null || element == '' || element == ' ') {
      return false
    }
  }
  return true
}
