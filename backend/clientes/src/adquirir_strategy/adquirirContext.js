function adquirirContext(
  strategy,
  total,
  idUsuario,
  listado,
  puntos,
  addPuntos
) {
  this.strategy = strategy;
  this.total = total;
  this.idUsuario = idUsuario;
  this.listado = listado;
  this.puntos = puntos;
  this.addPuntos = addPuntos;

  this.setStrategy = function (strategy) {
    this.strategy = strategy;
  };

  this.adquirirProdServ = function () {
    return this.strategy.adquirir(
      this.total,
      this.idUsuario,
      this.listado,
      this.puntos,
      this.addPuntos
    );
  };
}

module.exports = adquirirContext;
