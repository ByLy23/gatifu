const db = require("../db/database");

function servicioStrategy(mascota, fecha) {
  try {
    this.mascota = mascota;
    this.fecha = fecha;
    this.adquirir = async (total, idUsuario, listado, puntos, addPuntos) => {
      console.log(total, idUsuario, listado, puntos, addPuntos, fecha, mascota);

      const idFactura = await db.query("CALL crearFactura (?,?,?,?)", [
        idUsuario,
        total,
        puntos,
        addPuntos,
      ]);

      for (let index = 0; index < listado.length; index++) {
        const servicio = listado[index];

        const result = await db.query(
          "CALL agregarServicioPaqueteFactura (?,?,?,?,?,?)",
          [
            idFactura[0][0].resultado,
            servicio.id,
            mascota,
            fecha,
            servicio.resultado == null ? 0 : servicio.resultado,
            servicio.cantidad == null ? 0 : servicio.cantidad
          ]
        );

        if (result[0][0].resultado != 1) {
          return "Error,al adquirir Servicio/Paquete";
        }
      }
      return "Servicio/Paquete Comprado!";
    };
  } catch (error) {
    return error;
  }
}

function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index];
    if (
      element == undefined ||
      element == null ||
      element == "" ||
      element == " "
    ) {
      return false;
    }
  }
  return true;
}
module.exports = servicioStrategy;
