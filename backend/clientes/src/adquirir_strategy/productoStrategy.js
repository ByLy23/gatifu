const db = require("../db/database");

// function productoStrategy() {
//   this.quote = function (respuesta, idAdquisicion, cantidad, idUsuario) {
//     //Aquí va la consulta para obtener total y guardar la información

//     respuesta ="Respuesta";
//     return "";
//   };
// }

function productoStrategy() {
  try {
    this.adquirir = async (total, idUsuario, listado, puntos, addPuntos) => {
      console.log(total, idUsuario, listado, puntos, addPuntos);

      const idFactura = await db.query("CALL crearFactura (?,?,?,?)", [
        idUsuario,
        total,
        puntos,
        addPuntos,
      ]);

      for (let index = 0; index < listado.length; index++) {
        const producto = listado[index];

        const result = await db.query("CALL agregarProductoFactura (?,?,?)", [
          idFactura[0][0].resultado,
          producto.id,
          producto.cantidad,
        ]);

        if (result[0][0].resultado != 1) {
          return "Error,al adquirir Producto";
        }
      }
      return "Producto Comprado!";
    };
  } catch (error) {
    return error;
  }
}

function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index];
    if (
      element == undefined ||
      element == null ||
      element == "" ||
      element == " "
    ) {
      return false;
    }
  }
  return true;
}

module.exports = productoStrategy;
