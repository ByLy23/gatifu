const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const jwt = require("jsonwebtoken");
require("dotenv/config");
const db = require("./db/database");
const PORT = process.env.PORT || 3002;

//import Router from 'rutaDelRouter'
//const person = require("./routes/person.routes");
const pacientes = require("./routes/pacientes.routes");
const receta = require("./routes/receta.routes");
const citas = require("./routes/cita.routes");
const historial = require("./routes/hisotrial.routes");
const pagoAuth = require("./routes/pago.routes");
const nuevoPago = require("./routes/nuevoPago.routes");
const adquirir = require("./routes/adquirir.routes");
const factura = require("./routes/factura.routes");
const puntos = require("./routes/puntos.routes");

const app = express();
app.use(cors());
app.use(express.json());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

//Routes
//app.use("/api/user", person);

app.use("/static", express.static("src/assets/exp"));

app.use("/mascota", express.static("src/assets"));

app.use("/api/pago", authlinkPago, pagoAuth);

app.use("/api/nuevoPago/", nuevoPago);

//app.use(authenticateToken);

app.use("/api/pet", pacientes, logsIn_midlwr);

app.use("/api/cita", citas, logsIn_midlwr);

app.use("/api/receta", receta, logsIn_midlwr);

app.use("/api/citas", historial, logsIn_midlwr);

app.use("/api/compra/", adquirir, logsIn_midlwr);

app.use("/api/facturas", factura, logsIn_midlwr);

app.use("/api/puntos",puntos,logsIn_midlwr)

//app.use(logsIn_midlwr);

app.get("/", function (req, res) {
  res.send("Hola mundo Paciente");
});

async function logsIn_midlwr(req, res, next) {
  try {
    let route = req.originalUrl;
    //loop a json object
    // for(var attributename in data_in){
    //    cad += attributename+": "+data_in[attributename];
    // }
    let sql = `CALL insert_log("${route}",'${JSON.stringify(
      req.body
    )}','${JSON.stringify(req.data)}',"${req.error}");`;
    const id_log = await db.query(sql);
    console.log(id_log);
    console.log(id_log[0][0].resp);
  } catch (error) {
    console.log(error);
  }
}

//incio app
app.listen(PORT, () => {
  console.log("Server running on port", PORT);
});

function authenticateToken(req, res, next) {
  console.log(req.headers.authorization);
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  console.log("cheking token");
  console.log(token);
  if (token == null) return res.sendStatus(401); //there is no token to be valueted

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403); // the token is not longer value
    //here I can check from de data base
    req.user = user;
    console.log(user);
    next();
  });
}

function authlinkPago(req, res, next) {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null)
    return res.sendStatus(401).json({ message: "No hay Token de pago" });
  jwt.verify(token, process.env.PAGO_TOKEN_SECRET, (err, cita) => {
    if (err) return res.sendStatus(403).json({ message: "Token Vencido" }); // the token is not longer value
    req.cita = cita;
    next();
  });
}
