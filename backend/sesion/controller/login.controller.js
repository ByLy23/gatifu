require("dotenv").config();
const personaBuilder = require("../usuario/usuario.js");
const bcrypt = require("bcrypt");
const db = require("../conexionDB/conexion");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

exports.getUsers = async (req, res) => {
  try {
    let { id_user } = req.body;
    let sql = "";

    sql = `select * from USUARIO`;

    const result = await db.query(sql);
    return res.json(result);
  } catch (err) {
    console.log(err);
    return { msg: "error getting users" };
  }
};

/**
 *  tipoUsuarioIN INT,
    nombreIN VARCHAR(100),
    correoIN VARCHAR(100),
    edadIN INT,
    direccionIN VARCHAR(100),
    telefonoIN VARCHAR(100),
    passIN VARCHAR(256),
    nombreUsuarioIN VARCHAR(100),
    especialidadIN INT,
    horarioIN INT
    tipoUsuario
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */

exports.createUser = async (req, res) => {
  //console.log(req.body)
  try {
    let {
      nombre,
      usuario,
      correo,
      edad,
      direccion,
      telefono,
      password,
      horario,
      especialidad,
      tipo,
    } = req.body;

    const newUser = await new personaBuilder(
      nombre,
      correo,
      edad,
      direccion,
      telefono
    )
      .setPassword(password)
      .setUsuario(usuario)
      .setEspecialidad(especialidad)
      .setHorario(horario)
      .setTipo(tipo)
      .build();

    if (newUser.inserted) {
      const { subject, text } = {
        subject: "Bienvenido a la plataforma de emergencias",
        text:
          "Bienvenido a la plataforma de emergencias de la Universidad de San Carlos de Guatemala, su usuario es: " +
          newUser.usuario +
          " y su correo es: " +
          newUser.correo,
      };
      sende(newUser.correo, subject, text);
    }
    console.log(newUser);
    return res.status(200).json([
      [
        {
          resultado: newUser.resultado,
        },
      ],
      [
        {
          id_usuario: newUser.id,
        },
      ],
    ]);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error });
  }
};

exports.createClient = async (req, res) => {
  try {
    let {
      nombre,
      usuario,
      correo,
      edad,
      direccion,
      telefono,
      password,
      horario,
      especialidad,
      tipo,
    } = req.body;
    const newUser = await new personaBuilder(
      nombre,
      correo,
      edad,
      direccion,
      telefono
    )
      .setPassword(password)
      .setUsuario(usuario)
      .setEspecialidad(especialidad)
      .setHorario(horario)
      .setTipo(tipo)
      .build();
    console.log(req.body);
    res.json(newUser.resultado);
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
};

exports.logIn = async (req, res) => {
  let { usuarioIN, pass } = req.body;
  console.log(usuarioIN, pass);
  let sql = "";
  sql = `Call iniciarSesion('${usuarioIN}');`;
  const user = await db.query(sql);
  console.log(user);
  if (user[0].length == 0){
    console.log("no existe el usuario");
    return res.status(400).json({ message: "usuario no encontrado" });
  }
  try {
    if (await bcrypt.compare(pass, user[0][0].pass)) {
      const accessToken = jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, {
        expiresIn: "1500s",
      });
      console.log("Login correcto");
      res.json({
        accessToken: accessToken,
        userInfo: {
          ...user[0][0],
          pass: undefined,
        },
      });
    } else {
      console.log(pass, user[0][0].pass);
      res.status(400).json({ message: "contraseña incorrecta" });
    }
  } catch {
    res.status(500).json({ message: "error" });
  }
};

exports.middlewareTes = (req, res) => {
  try {
  } catch (error) {
    console.log(error);
    return res.send();
  }
};

async function sende(email, subject, text) {
  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "a.annelis@gmail.com",
      pass: "oplgphnvxmxctvhx",
    },
  });

  let info = {
    from: '"Gatifu Hospital" <a.annelis@gmail.com>', // sender address
    to: email, // list of receivers
    subject: subject, // Subject line
    html: `<body>${text}</body>`, // html body
  };

  transporter.sendMail(info, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}
