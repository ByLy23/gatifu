const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const login = require("../controller/login.controller");

//the routes here
router.get("/getUsers", login.getUsers);
router.post("/",authenticateToken, login.createUser);
router.post("/userEmergencia", login.createClient);
router.post("/login", login.logIn);

//router.post('/authTest',verifyToken,login.middlewareTes);
router.post("/authTest", login.middlewareTes);

function authenticateToken(req, res, next) {
  console.log(req.headers.authorization);
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  console.log("cheking token");
  console.log(token);
  if (token == null) return res.sendStatus(401); //there is no token to be valueted

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403); // the token is not longer value
    //here I can check from de data base
    req.user = user;
    console.log(user);
    next();
  });
}

module.exports = router;
