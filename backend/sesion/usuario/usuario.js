const bcrypt = require('bcrypt')
const db = require('../conexionDB/conexion')

class Persona {
    constructor (nombre,correo,edad,direccion,telefono){
        this.nombre = nombre;
        this.correo = correo;
        this.edad = edad;
        this.direccion =direccion;
        this.telefono = telefono;
    }
}



class personaBuilder{
    
    constructor(nombre,correo,edad,direccion,telefono){       
        this.persona = new Persona(nombre,correo,edad,direccion,telefono); 
    }

    setHorario(horario){
        this.persona.horario = horario;
        return this;
    }

    setEspecialidad(especialidad){
        this.persona.especialidad = especialidad;
        return this;
    }

    setTipo(tipo){
        this.persona.tipo = tipo;
        return this;
    }

    setPassword(password){
        this.persona.password = password;
        return this;
    }

    setUsuario(usuario){
        this.persona.usuario = usuario;
        return this;
    }

    async build(){
        
        try {
            //console.log("+++++++++++++++++++++FROM CLASS++++++++++++++++++++++")
            //console.log(this.persona);
            const salt = await bcrypt.genSalt();
            const hashedPassword = await bcrypt.hash(this.persona.password,salt);
            let sql = `CALL crearPersona(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
            const result = await db.query(sql, [
                this.persona.tipo,
                this.persona.nombre,
                this.persona.correo,
                this.persona.edad,
                this.persona.direccion,
                this.persona.telefono,
                hashedPassword,
                this.persona.usuario,
                this.persona.especialidad,
                this.persona.horario,
              ]);
            //console.log("+++++++++++++++++++++RESULT++++++++++++++++++++++")
            console.log(result[0][0].resultado);
            if(result[0][0].resultado==1){
                console.log("+++++++++++++++++++++TRUE++++++++++++++++++++++")
                this.persona.inserted = true;
                let user = await db.query(`SELECT id_usuario FROM USUARIO WHERE nombre_usuario = "${this.persona.usuario}"`);
                this.persona.id = user[0].id_usuario;
                this.persona.resultado = result[0][0].resultado;
            }else{
                console.log("+++++++++++++++++++++FALSE++++++++++++++++++++++")
                this.persona.resultado = result[0][0].resultado;
                this.persona.id = "";
                this.persona.inserted = false;
            }
            return this.persona
        } catch (error) {
            console.log("+++++++++++++++++++++ERROR++++++++++++++++++++++")
            this.persona.resultado ={ message: error.sqlMessage };
            this.persona.id = "";
            this.persona.inserted = false;
            return this.persona
        } 
    }

    async insertUser() {
        

    }

}
 
module.exports = personaBuilder;