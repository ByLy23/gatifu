const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("GET /medicamento", () => {
  // obtenter token de autenticacion para el usuario
  let token = "";

  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        // datos de usuario medico
        usuarioIN: "md",
        pass: SHA256("md").toString(),
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.accessToken; // save the token!
        done();
      });
  });

  // test to get all medicamentos
  it("should return 200 OK", () => {
    return req("http://localhost:3003")
      .get("/api/medicamento")
      .set("Authorization", `Bearer ${token}`)
      .expect(200);
  });

  // test to get all medicamentos
  // it("should return 401 Unauthorized", () => {
  //   return req("http://localhost:3003").get("/api/medicamento").expect(401);
  // });
});
