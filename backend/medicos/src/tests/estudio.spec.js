const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("POST /estudio", () => {
  // obtenter token de autenticacion para el usuario
  let token = "";

  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        // datos de usuario medico
        usuarioIN: "md",
        pass: SHA256("md").toString(),
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.accessToken; // save the token!
        done();
      });
  });

  // test to post estudio
  it("should return 200 OK", () => {
    return req("http://localhost:3003")
      .post("/api/estudio")
      .set("Authorization", `Bearer ${token}`)
      .send({
        nombre: "unit-test",
        direccion: "130-Complemento Caso Zara.pdf",
        idMotivo: 130,
      })
      .expect(200);
  });

  // test to post estudio with error
  it("should return 400 Bad Request", () => {
    return req("http://localhost:3003")
      .post("/api/estudio")
      .set("Authorization", `Bearer ${token}`)
      .send({
        direccion: "unit-test",
        nombre: "130-Complemento Caso Zara.pdf",
      })
      .expect(400);
  });

  // test to post estudio with error
  // it("should return 401 Unauthorized", () => {
  //   return req("http://localhost:3003")
  //     .post("/api/estudio")
  //     .send({
  //       direccion: "Calle 1",
  //       nombre: "Estudio 1",
  //       idMotivo: 130,
  //     })
  //     .expect(401);
  // });
});
