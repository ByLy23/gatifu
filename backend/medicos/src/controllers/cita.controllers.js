const db = require("../db/database");

//getCita --- Devuelve la cita actual es decir la cita que esta programada para la hora actual
exports.getCita = async (req, res,next) => {
  try {
    const { idMedico } = req.params;
    req.body = req.params;
    let arrayParams = [idMedico];
    if (!notUndefinedNull(arrayParams)){
      req.data = {
        message: "Error, datos incompletos. Se requiere idMedico"
      };
      req.error = "Si"
      next()
      return res.status(400).json({
        message: "Error, datos incompletos. Se requiere idMedico"
      });
    }
    const result = await db.query(`CALL obtenerCitaActualMedico (${idMedico})`);
    req.data = result[0][0];
    req.error  ="No";
    return res.status(200).json(result[0][0]);
  } catch (error) {
    req.data = {
      message: "Error al obtener cita. ",
      descirption: error,
    };
    req.error = "Si";
    next();
    return res.status(400).json({
      message: "Error al obtener cita. ",
      descirption: error,
    });
  }
};

// Verifica que no existan nulos, indefinidos, 0 ó cadenas vacías
function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index];
    if (
      element == undefined ||
      element == null ||
      element == "" ||
      element == " "
    ) {
      return false;
    }
  }
  return true;
}
