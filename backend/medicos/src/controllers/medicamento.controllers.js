const db = require("../db/database");

//getMedicamento --- Devuelve el id y nombre de todos los medicamentos
exports.getMedicamento = async (req, res,next) => {
  try {
    const result = await db.query(
      `SELECT id_medicamento AS id, nombre FROM MEDICAMENTO`
    );
    console.log(result);
    req.data = result;
    req.error = "No";
    next();
    return res.status(200).json(result);
  } catch (error) {
    req.data = {
      message: "Error al obtener Medicamentos. ",
      descirption: error,
    };
    req.error = "Si";
    next();
    return res.status(400).json({
      message: "Error al obtener Medicamentos. ",
      descirption: error,
    });
  }
};
