const db = require("../db/database");

//postEstudio --- Premite crear un estudio
exports.postEstudio = async (req, res,next) => {
  try {
    console.log("dasd", req.body);
    const data = req.body;
    let arrayParams = [data.direccion, data.nombre, data.idMotivo];
    if (!notUndefinedNull(arrayParams)){
      console.log("error");
      req.data = {
        message: "Error, datos incompletos",
      };
      req.error = "Si";
      next();
      return res.status(400).json({
        message: "Error, datos incompletos",
      });
    }

    await db.query("CALL crearEstudio (?,?,?);", arrayParams);
    req.data = { message: "Estudio Registrado!" };
    req.error = "No"
    next();
    return res.status(200).json({ message: "Estudio Registrado!" });
  } catch (error) {
    console.log(error);
    req.data = {
      message: "Error al crear Estudio. ",
      descirption: error,
    }
    req.error = "Si";
    next();
    return res.status(400).json({
      message: "Error al crear Estudio. ",
      descirption: error,
    });
  }
};

//deleteestudio --- Premite eliminar un estudio
exports.deleteEstudio = async (req, res,next) => {
  try {
    const { id_estudio } = req.body;
    let arrayParams = [id_estudio];
    if (!notUndefinedNull(arrayParams)){
      
      req.data = {
        message:
          "Error, datos incompletos. Se requiere id del estudio a eliminar",
      };
      req.error = "Si";
      next();
      return res.status(400).json({
        message:
          "Error, datos incompletos. Se requiere id del estudio a eliminar",
      });

    }
    const result = await db.query(
      `DELETE FROM ESTUDIO WHERE id_estudio=${id_estudio}`
    );
    if (result.affectedRows == 0){

      req.data = { message: "No se encontró ningún estudio con este id" };
      req.error = "No";
      next();
      return res
        .status(200)
        .json({ message: "No se encontró ningún estudio con este id" });
    }
    return res.status(200).json({ message: "Estudio Eliminado!" });
    
  } catch (error) {
    req.data = {
      message: "Error al eliminar Estudio. ",
      descirption: error,
    }
    req.error = "Si";
    next();
    return res.status(400).json({
      message: "Error al eliminar Estudio. ",
      descirption: error,
    });
  }
};

// Verifica que no existan nulos, indefinidos, 0 ó cadenas vacías
function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index];
    if (
      element == undefined ||
      element == null ||
      element == "" ||
      element == " "
    ) {
      return false;
    }
  }

  return true;
}
