require("dotenv/config");
var express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const db = require("./db/database");
const jwt = require("jsonwebtoken");
const app = express();

// require("dotenv/config");
// const PORT = process.env.PORT || 3004;

//import Router from 'rutaDelRouter'
const endpoints = require("./routes/raza.routes");
const medico = require("./routes/medico.routes");
const sala = require("./routes/sala.routes");
const mascota = require("./routes/mascota.routes");
const pagos = require("./routes/pagos.routes");

app.set("port", 3004);

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api/pagos", pagos);
// app.use(authenticateToken);
app.use("/api/raza", endpoints);
app.use("/api/medico", medico);
app.use("/api/salas", sala);
app.use("/api/mascotas", mascota);
app.use(logsIn_midlwr);
//Routes
// app.use("/api/user", secretaria);

app.get("/", function (req, res) {
  res.send("Hola mundo desde el serivdor de sercretaria");
});

//incio app
app.listen(app.get("port"), () => {
  console.log("Server running on port");
});

function authenticateToken(req, res, next) {
  console.log(req.headers.authorization);
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  console.log("cheking token");
  console.log(token);
  if (token == null) return res.sendStatus(401); //there is no token to be valueted

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      console.log(err);
      return res.sendStatus(403); // the token is not longer value
    }

    //here I can check from de data base

    req.user = user;
    console.log(user);
    next();
  });
}

async function logsIn_midlwr(req, res, next) {
  try {
    let route = req.originalUrl;
    //loop a json object
    // for(var attributename in data_in){
    //    cad += attributename+": "+data_in[attributename];
    // }
    let sql = `CALL insert_log("${route}",'${JSON.stringify(
      req.body
    )}','${JSON.stringify(req.data)}',"${req.error}");`;
    const id_log = await db.query(sql);
    console.log(id_log);
    console.log(id_log[0][0].resp);
  } catch (error) {
    console.log(error);
  }
}
