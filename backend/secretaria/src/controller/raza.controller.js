const { json } = require('body-parser')
const db = require('../db/database')

exports.postRaza = async (req, res) => {
  try {
    let { raza } = req.body
    //console.log(raza);
    let sql = `CALL crearRaza ("${raza}")`
    const result = await db.query(sql)
    if (result[0][0].resultado == 1) {
      req.data = { msg: 'raza creada' }
      req.error = 'No'
      next()
      return res.json({ msg: 'raza creada' })
    } else {
      req.data = { msg: 'Raza ya existe' }
      req.error = 'Si'
      next()
      return res.json({ msg: 'Raza ya existe' })
    }
  } catch (err) {
    console.log(err)
    req.data = { msg: 'error posting raza' }
    req.error = 'Si'
    return res.json({ msg: 'error posting raza' })
  }
}

exports.getRaza = async (req, res, next) => {
  try {
    let sql = `CALL obtenerRazas ();`
    const result = await db.query(sql)
    req.data = result[0]
    req.error = 'No'
    next()
    return res.json(result[0])
  } catch (error) {
    console.log(error)
    req.data = { msg: 'error getting Raza' }
    req.error = 'Si'
    next()
    return res.json({ msg: 'error getting Raza' })
  }
}

exports.postMascota = async (req, res, next) => {
  try {
    console.log(req.body)
    let { usuario_id, nombre, edad, genero, raza } = req.body
    let sql = `CALL crearMascota (${usuario_id},"${nombre}",${edad},"",${genero},${raza})`
    const result = await db.query(sql)
    req.data = { message: 'mascota creada' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'mascota creada', resultado: 1 })
  } catch (error) {
    req.data = { msg: 'error posting Mascota' }
    req.error = 'Si'
    next()
    console.log(error)
    return res.status(500).json({ msg: 'error posting Mascota', resultado: 0 })
  }
}
