const db = require('../db/database')
exports.getMascotas = async (req, res) => {
  try {
    let { id } = req.params
    let sql = `CALL obtenerMascotas (${id})`
    const result = await db.query(sql)
    return res.json(result[0])
  } catch (error) {
    console.log(error)
    return res.json({ message: 'error getting Mascota', description: error })
  }
}
exports.getTodasMascotas = async (req, res) => {
  try {
    console.log('entrando')
    let sql = 'SELECT * from MASCOTA'
    const result = await db.query(sql)
    return res.json(result)
  } catch (error) {
    console.log(error)
    return res.json({ message: 'error getting Mascotas', description: error })
  }
}
