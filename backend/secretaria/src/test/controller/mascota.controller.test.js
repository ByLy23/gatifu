const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA GET SERVICIOS", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        usuarioIN: "gatifu",
        pass: SHA256("gatifu").toString(),
      })
      .end((err, response) => {
        token = response.body.accessToken; 
        done();
      });
  });

  test("Este test verificara el obtener los servicios", async () => {
    const mascota = await req("http://localhost:3004")
      .get("/api/mascotas/mascota/all")
      .set("Authorization", `Bearer ${token}`);
      expect(mascota.statusCode).toBe(200);
  });
});

describe("PRUEBA UNITARIA GET SERVICIOS", () => {
    let token = "";
    beforeAll((done) => {
      req("http://localhost:3005")
        .post("/api/user/login")
        .send({
          usuarioIN: "gatifu",
          pass: SHA256("gatifu").toString(),
        })
        .end((err, response) => {
          token = response.body.accessToken; 
          done();
        });
    });
  
    test("Este test verificara el obtener los servicios", async () => {
      const mascota = await req("http://localhost:3004")
        .get("/api/mascotas/3")
        .set("Authorization", `Bearer ${token}`);
        expect(mascota.statusCode).toBe(200);
    });
  });
  
  
  


