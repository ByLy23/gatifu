const express = require("express");
const router = express.Router();

const pagos = require("../controller/pagos.controller");

router.get("/:idUsuario/:estadoPago", pagos.getListPagos);
router.post("/", pagos.postPago);
router.delete("/", pagos.deletePago);

module.exports = router;
