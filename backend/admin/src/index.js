const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const db = require("./db/database");
require("dotenv/config");
const PORT = process.env.PORT || 3000;
const jwt = require("jsonwebtoken");

//import Router from 'rutaDelRouter'
const user = require("./routes/user.routes");
const descuento = require("./routes/descuento.routes");
const producto = require("./routes/producto.routes");
const servicio = require("./routes/servicio.routes");
const paquete = require("./routes/paquete.routes");

const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Routes
// app.use(authenticateToken)

app.use("/api/user", user, logsIn_midlwr);
app.use("/api/descuento", descuento, logsIn_midlwr);
app.use("/api/productos", producto, logsIn_midlwr);
app.use("/api/servicios", servicio, logsIn_midlwr);
app.use("/api/paquetes", paquete, logsIn_midlwr);

app.get("/", function (req, res) {
  res.send("Hola mundo Paciente");
});

//incio app
app.listen(PORT, () => {
  console.log("Server running on port", PORT);
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401); //there is no token to be valueted

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403); // the token is not longer value
    //here I can check from de data base
    req.user = user;
    next();
  });
}

async function logsIn_midlwr(req, res, next) {
  try {
    let route = req.originalUrl;
    //loop a json object
    // for(var attributename in data_in){
    //    cad += attributename+": "+data_in[attributename];
    // }
    let sql = `CALL insert_log("${route}",'${JSON.stringify(
      req.body
    )}','${JSON.stringify(req.data)}',"${req.error}");`;
    const id_log = await db.query(sql);
  } catch (error) {
    console.log(error);
  }
}
