const db = require("../db/database");

//getServicio ---- Permite listar todos los servicios en el sistema
exports.getServicio = async (req, res, next) => {
  try {
    const result = await db.query("CALL obtenerServicios ()");
    req.data = result[0];
    req.error = "No";
    next();
    return res.status(200).json(result[0]);
  } catch (error) {
    req.data = {
      message: error,
    };
    req.error = "Si";
    next();
    return res.status(400).json({
      message: error,
    });
  }
};
