const db = require('../db/database')

//postPaquetes --- Premite crear paquetes.
exports.postPaquete = async (req, res, next) => {
  try {
    const data = req.body
    let arrayParams = [data.nombre, data.descripcion, data.precio]
    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Paquetes',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Paquetes',
      })
    }
    const idPaquete = await db.query('CALL crearPaquete (?,?,?)', arrayParams)
    for (let index = 0; index < data.servicios.length; index++) {
      const servicio = data.servicios[index]
      result = await db.query('CALL agregarServicioPaquete (?,?)', [
        idPaquete[0][0].resultado,
        servicio.id,
      ])
      if (result[0][0].resultado != 1) {
        req.data = {
          message: `Error, al agregar servicio al Paquete con id: ${idPaquete[0][0].resultado} `,
        }
        req.error = 'Si'
        next()
        return res.status(400).json({
          message: `Error, al agregar servicio al Paquete con id: ${idPaqueteidPaquete[0][0].resultado} `,
        })
      }
    }
    req.data = { message: 'Paquete Creado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Paquete Creado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: 'Error al crear Paquete. ',
      descirption: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: 'Error al crear Paquete. ',
      descirption: error,
    })
  }
}

//deletePaquete ---- Permite eliminar un paquete
exports.deletePaquete = async (req, res, next) => {
  try {
    const data = req.params
    let arrayParams = [data.id]

    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Paquete',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Paquete',
      })
    }

    //utilizar el proc
    const result = await db.query('CALL eliminarPaquete (?)', arrayParams)
    // console.log(result[0][0].resultado);
    if (result[0][0].resultado != 1) {
      req.data = {
        message: 'Error, al eliminar Paquete',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, al eliminar Paquete',
      })
    }
    req.data = { message: 'Paquete Eliminado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Paquete Eliminado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}

//getPaquetes --- Permite obtener todos los paquetes
exports.getPaquetes = async (req, res, next) => {
  try {
    const { id } = req.params
    const result = await db.query('CALL obtenerPaquetes (?)', [id])
    for (let item of result[0]) {
      const servicios = await db.query('CALL obtenerDetallePaquete(?)', [item.id_producto])
      item.servicios = servicios[0]
    }
    req.data = { message: 'Paquetes obtenidos' }
    req.error = 'No'
    next()
    return res.status(200).json(result[0])
  } catch (error) {
    console.log(error)
    req.data = { message: 'Error al obtener paquetes' }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}

//actualizar paquete --- Permite actualizar el precio de un paquete
exports.updatePrecioPaquete = async (req, res, next) => {
  try {
    const data = req.body
    const { id } = req.params
    console.log(data, id)
    let arrayParams = [id, data.descripcion, data.precio]
    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Paquete',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Paquete',
      })
    }
    const result = await db.query('CALL modificarPaquete (?,?,?)', arrayParams)
    if (result[0][0].resultado != 1) {
      req.data = {
        message: 'Error, al actualizar Paquete',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, al actualizar Paquete',
      })
    }
    req.data = { message: 'Paquete Actualizado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Paquete Actualizado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}


/**
 * 
 * agregar servicios a un paquete
 * /api/paquetes/servicios
 * 
 */
exports.updatePaquete = async(req,res,next)=>{
  try {
    const {id} = req.params;
    const servicios = req.body.servicios;

    let sql = `CALL obtenerDetallePaquete(${id});`;
    const result = await db.query(sql);
    const oldServices = []
    result[0].forEach(element => {
      oldServices.push(element.id);
    });
    //console.log("Old:",oldServices);
    //console.log("New:",servicios)
    
    await asynForEach(servicios,async(servicio)=>{
      const containsService = oldServices.includes(servicio.id) 
      if(containsService==false){
        const addService = await db.query(`CALL agregarServicioPaquete (${id},${servicio.id})`);
        if(addService.affectedRows == 0){
          req.data = {message:"Error al Agregar Servicio"};
          req.error = "Si"
          next();
          return res.status(400).json({message:"Error al Agregar Servicio"}); 
        }
      }
    });

    const serv = [];
    for (let index = 0; index < servicios.length; index++) {
      const servicio = servicios[index];
        serv.push(servicio.id);
    }

    await asynForEach(oldServices,async(servicio)=>{
      const containsService = serv.includes(servicio);
      if(containsService==false){
        const removeService = await db.query(`CALL eliminarServicioPaquete(${id},${servicio})`);
        if(removeService.affectedRows == 0){ 
          req.data = {message:"Error al eliminar Servicio"};
          req.error = "Si"
          next();
          return res.status(400).json({message:"Error al eliminar Servicio"}); 
        }
      }
    })

    req.data = {message: "Se actualizo el paquete con exito!!!"};
    req.error = "No";
    next();
    
    return res.status(200).json({message: "Se actualizo el paquete con exito!!!"});
  } catch (err) {
    console.log(err);
    req.data = {message:"error al actualizar paquete", error : err}
    req.error = "Si";
    next();
    return res.status(400).json({message:"error al actualizar paquete", error : err});
  }
}


async function asynForEach(array,callback){
  for (let index = 0; index < array.length; index++) {
    await callback(array[index],index,array);
  }
}


function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index]
    if (element == undefined || element == null || element == '' || element == ' ') {
      return false
    }
  }
  return true
}
