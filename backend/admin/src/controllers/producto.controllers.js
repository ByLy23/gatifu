const db = require('../db/database')
const ProductoState = require('../Producto/Producto')

//postProducto ---- Permite crear un nuevo producto
exports.postProducto = async (req, res, next) => {
  try {
    const data = req.body
    let arrayParams = [
      data.nombre,
      data.precio,
      data.descripcion,
      data.categoria,
      data.marca,
      data.stock,
    ]

    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Productos',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Productos',
      })
    }

    //utilizar el proc
    const result = await db.query('CALL crearProducto (?,?,?,?,?,?)', arrayParams)
    // console.log(result[0][0].resultado);
    if (result[0][0].resultado != 1) {
      req.data = {
        message: 'Error,al crear Productos',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error,al crear Productos',
      })
    }
    console.log(result)
    req.data = { message: 'Producto Creado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Producto Creado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}

//listarProductos ---- Permite listar todos los productos
exports.mostrarProductos = async (req, res, next) => {
  try {
    //TODO: crear aca metodo para listar todos los productos con estado
    const { tipo } = req.params //tipo 1: todos los productos, tipo 2: productos con stock
    req.body = req.params
    req.data = { message: 'Productos obtenidos con éxito' }
    req.error = 'No'
    const newProducto = await new ProductoState(tipo).setTipo(tipo).changeState()
    if (newProducto.resultado === null)
      return res.status(500).json({ message: 'Error al obtener productos' })
    return res.status(200).json(newProducto.resultado)
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(500).json({
      message: error,
    })
  }
}

//deleteProducto ---- Permite eliminar un producto
exports.deleteProducto = async (req, res, next) => {
  try {
    const data = req.params
    let arrayParams = [data.id]

    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Productos',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Productos',
      })
    }

    //utilizar el proc
    const result = await db.query('CALL eliminarProducto (?)', arrayParams)
    // console.log(result[0][0].resultado);
    if (result[0][0].resultado != 1) {
      req.data = {
        message: 'Error, al elimianr Producto',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, al elimianr Producto',
      })
    }
    req.data = { message: 'Producto Eliminado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Producto Eliminado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}

//updateProducto ---- Permite actualizar el producto, no se va editar el nombre, ni el stock
exports.updateProducto = async (req, res, next) => {
  try {
    const { id } = req.params
    const data = req.body
    let arrayParams = [id, data.precio, data.descripcion, data.categoria, data.marca]

    if (!notUndefinedNull(arrayParams)) {
      req.data = {
        message: 'Error, datos incompletos en manejo de Productos',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error, datos incompletos en manejo de Productos',
      })
    }

    //utilizar el proc
    const result = await db.query('CALL modificarProducto (?,?,?,?,?)', arrayParams)

    if (result[0][0].resultado != 1) {
      req.data = {
        message: 'Error,al modificar Producto',
      }
      req.error = 'Si'
      next()
      return res.status(400).json({
        message: 'Error,al modificar Producto',
      })
    }
    req.data = { message: 'Producto Modificado!' }
    req.error = 'No'
    next()
    return res.status(200).json({ message: 'Producto Modificado!' })
  } catch (error) {
    console.log(error)
    req.data = {
      message: error,
    }
    req.error = 'Si'
    next()
    return res.status(400).json({
      message: error,
    })
  }
}

//Incrementara el stock de un producto
exports.restockFarmacia = async (req, res, next) => {
  try {
    let { id } = req.params
    let { stock } = req.body
    const data = req.body
    sql = `call restock(${id},${stock});`
    const result = await db.query(sql)
    req.data = { message: 'El stock del producto seleccionado a sido actualizado con éxito.' }
    req.error = 'No'
    next()
    return res
      .status(200)
      .json({ message: 'El stock del producto seleccionado a sido actualizado con éxito.' })
  } catch (err) {
    console.log(err)
    req.data = { message: 'Error en restock farmacia.' }
    req.error = 'Si'
    next()
    return res.status(400).json({ message: 'Error en restock farmacia.' })
  }
}

//Obtiene todas las categorias
exports.getCategorias = async (req, res, next) => {
  try {
    sql = `call obtenerCategorias();`
    const result = await db.query(sql)
    req.data = result[0]
    req.error = 'No'
    next()
    return res.status(200).json(result[0])
  } catch (err) {
    console.log(err)
    req.data = { message: 'error getting categorias' }
    req.error = 'No'
    next()
    return res.status(400).json({ message: 'error getting categorias' })
  }
}

//Obtendra todas las marcas
exports.getMarcas = async (req, res, next) => {
  try {
    sql = `call obtenerMarcas();`
    const result = await db.query(sql)
    req.data = result[0][0]
    req.error = 'No'
    next()
    return res.status(200).json(result[0])
  } catch (err) {
    console.log(err)
    req.data = { message: 'error getting marcas' }
    req.error = 'Si'
    next()
    return res.status(400).json({ message: 'error getting marcas' })
  }
}

function notUndefinedNull(arrayParams) {
  for (let index = 0; index < arrayParams.length; index++) {
    let element = arrayParams[index]
    if (element == undefined || element == null || element == '' || element == ' ') {
      return false
    }
  }
  return true
}
