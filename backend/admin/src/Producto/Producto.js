const db = require('../db/database')
class Producto {
  constructor(tipo) {
    this.tipo = tipo
  }
}

class ProductoState {
  constructor(tipo) {
    this.producto = new Producto(tipo)
  }
  setTipo(tipo) {
    this.tipo = tipo
    return this
  }
  async changeState() {
    try {
      let result = null
      switch (this.tipo) {
        case '1':
          result = await db.query('CALL obtenerProductos(?)', [this.producto.tipo])
          break
        case '2':
          result = await db.query('CALL obtenerProductos(?)', [this.producto.tipo])
          break
        default:
          result = null
      }
      this.producto.resultado = result[0]
      return this.producto
    } catch (e) {
      this.producto.resultado = null
      return this.producto
    }
  }
}

module.exports = ProductoState
