const express = require('express')
const router = express.Router()

const paquete = require('../controllers/paquete.controllers')

//Peticiones
router.post('/', paquete.postPaquete)
router.get('/:id', paquete.getPaquetes)
router.delete('/:id', paquete.deletePaquete)

router.put("/:id", paquete.updatePrecioPaquete);
router.put("/servicios/:id", paquete.updatePaquete);
module.exports = router;
