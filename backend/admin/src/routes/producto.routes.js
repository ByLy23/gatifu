const express = require('express')
const router = express.Router()

const producto = require('../controllers/producto.controllers')

//Peticiones
router.get('/tipo/:tipo', producto.mostrarProductos)
router.post("/", producto.postProducto);
router.delete("/:id", producto.deleteProducto);
router.put("/:id", producto.updateProducto);

//Gestion de Producto
router.put('/stock/:id', producto.restockFarmacia)
router.get('/categoria', producto.getCategorias)
router.get('/marca', producto.getMarcas)

module.exports = router
