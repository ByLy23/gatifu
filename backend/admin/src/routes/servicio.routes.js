const express = require("express");
const router = express.Router();

const servicio = require("../controllers/servicio.controllers");

//Peticiones
router.get("/", servicio.getServicio);

module.exports = router;
