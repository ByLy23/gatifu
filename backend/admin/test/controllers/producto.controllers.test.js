const req = require("supertest");
const { SHA256 } = require("crypto-js");


describe("PRUEBA UNITARIA GET SERVICIOS",()=> {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        // datos de usuario medico
        usuarioIN: "gatifu",
        pass: SHA256("gatifu").toString(),
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.accessToken; // save the token!
        done();
      });
  });

  test("Este Test Verificara el Incremeto de stock en un producto",async () => {
    const result = await req("http://localhost:3001")
      .put("/api/productos/stock/1")
      .set("Authorization", `Bearer ${token}`)
      .send({
        stock:1
       })
       expect(result.statusCode).toBe(200);
  });

})
