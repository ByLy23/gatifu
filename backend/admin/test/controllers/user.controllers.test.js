const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA GET PERSONAS", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        usuarioIN: "gatifu",
        pass: SHA256("gatifu").toString(),
      })
      .end((err, response) => {
        token = response.body.accessToken; 
        done();
      });
  });

  test("Esta prueba obtendra un estado valido 200", async () => {
      const personas = await req("http://localhost:3001")
      .get("/api/user/2")
      .set("Authorization", `Bearer ${token}`);
      expect(personas.statusCode).toBe(200);
  });
});
