const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA UNITARIA GET SERVICIOS", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/user/login")
      .send({
        usuarioIN: "gatifu",
        pass: SHA256("gatifu").toString(),
      })
      .end((err, response) => {
        token = response.body.accessToken; 
        done();
      });
  });

  test("Este test verificara el obtener los servicios", async () => {
    const personas = await req("http://localhost:3001")
      .get("/api/servicios/")
      .set("Authorization", `Bearer ${token}`);
      expect(personas.statusCode).toBe(200);
  });
});
