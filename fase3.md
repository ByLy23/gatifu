# Proyecto 1 - G5 - Análisis y diseño 2 - Septiembre 2022 - Fase # 3


## Tabla de contenidos
- [Diagramas de casos de uso](./documentacion/diagrama_cdu3.md)
- [Casos de uso extendidos](./documentacion/cdu_extendidos3.md)
- [Historias de usuario](./documentacion/historias_usuario3.md)
- [Diagrama de historias de usuario](./documentacion/dHistorias.pdf)
- [Mapeo de historias de usuario](./documentacion/files/dHistorias.pdf)
- [Requerimientos funcionales](./documentacion/req_fun3.md)
- [Requerimientos no funcionales](./documentacion/req_no_fun3.md)
- [Diagrama de componentes](./documentacion/files/componentes3.pdf)
- [Diagrama de despliegue](./documentacion/files/deploy2.pdf)
- [Modelo de datos entidad relacion](./documentacion/er3.pdf)
- [Mockups](./documentacion/files/mockups3.pdf)
- [Diseño arquitectónico](./documentacion/files/arquitectura.pdf)
- [Sprints](./documentacion/sprints3.md)
- [Patrones de diseño](./documentacion/patrones.md)
- [Diagrama de pruebas](./documentacion/dPruebas.pdf)
- [Diagrama devops](./documentacion/dDevops.pdf)
- [Diagrama de producción ](./documentacion/dProduccion.pdf)



