# Proyecto 1 - G5 - Análisis y diseño 2 - Septiembre 2022

## Integrantes

|Nombre|Carné|
|:- |:- |
|Christtopher Jose Chitay Coutino|201113851|
|Joel Estuardo Rodríguez Santos|201115018|
|Byron Antonio Orellana Alburez|201700733|
|Marvin Daniel Rodriguez Felix |201709450|
|Audrie Annelisse del Cid Ochoa|201801263|


## Tabla de contenidos
- [Fase 1](./fase1.md)
- [Fase 2](./fase2.md)
- [Fase 3](./fase3.md)



