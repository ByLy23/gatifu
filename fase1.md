# Proyecto 1 - G5 - Análisis y diseño 2 - Septiembre 2022 - Fase # 1


## Tabla de contenidos
- [Diagramas de casos de uso](./documentacion/diagrama_cdu.md)
- [Casos de uso extendidos](./documentacion/cdu_extendidos.md)
- [Historias de usuario](./documentacion/historias_usuario.md)
- [Mapeo de historias de usuario](./documentacion/files/historias_usuario.pdf)
- [Requerimientos funcionales](./documentacion/req_fun.md)
- [Requerimientos no funcionales](./documentacion/req_no_fun.md)
- [Diagrama de componentes](./documentacion/files/componentes.pdf)
- [Diagrama de despliegue](./documentacion/files/deploy.pdf)
- [Modelo de datos entidad relacion](./documentacion/files/er.pdf)
- [Mockups](./documentacion/files/mockups.pdf)
- [Diseño arquitectónico](./documentacion/files/arquitectura.pdf)
- [Sprints](./documentacion/sprints.md)




