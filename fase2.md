# Proyecto 1 - G5 - Análisis y diseño 2 - Septiembre 2022 - Fase # 2


## Tabla de contenidos
- [Diagramas de casos de uso](./documentacion/diagrama_cdu2.md)
- [Casos de uso extendidos](./documentacion/cdu_extendidos2.md)
- [Historias de usuario](./documentacion/historias_usuario2.md)
- [Mapeo de historias de usuario](./documentacion/files/historias_usuario2.pdf)
- [Requerimientos funcionales](./documentacion/req_fun2.md)
- [Requerimientos no funcionales](./documentacion/req_no_fun2.md)
- [Diagrama de componentes](./documentacion/files/componentes2.pdf)
- [Diagrama de despliegue](./documentacion/files/deploy2.pdf)
- [Modelo de datos entidad relacion](./documentacion/files/er2.pdf)
- [Mockups](./documentacion/files/mockups2.pdf)
- [Diseño arquitectónico](./documentacion/files/arquitectura.pdf)
- [Sprints](./documentacion/sprints2.md)
- [Patrones de diseño](./documentacion/patrones.md)




