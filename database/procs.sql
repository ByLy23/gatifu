#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearPersona
DELIMITER //
CREATE PROCEDURE crearPersona(
	IN tipoUsuarioIN INT,
    IN nombreIN VARCHAR(100),
    IN correoIN VARCHAR(100),
    IN edadIN INT,
    IN direccionIN VARCHAR(100),
    IN telefonoIN VARCHAR(100),
    IN passIN VARCHAR(256),
    IN nombreUsuarioIN VARCHAR(100),
    IN especialidadIN INT,
    IN horarioIN INT
)
this_proc:BEGIN
	DECLARE ultimaPersona INT;
    DECLARE repetido INT;
    DECLARE userRepetido INT;
    DECLARE altaUsuario INT;
    DECLARE idPersona INT;
    SET altaUsuario = 1;
    #Conseguir repetido
    SET repetido = (SELECT id_persona FROM PERSONA WHERE LOWER(nombre) = LOWER(nombreIN) 
    AND LOWER(correo) = LOWER(correoIN) limit 1);
    SET userRepetido = (SELECT id_usuario FROM USUARIO WHERE nombre_usuario = nombreUsuarioIN limit 1);
    #Error si es repetido
    IF repetido IS NOT NULL THEN
		SELECT 0 AS resultado;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Nombre de usuario ya existe.';
		LEAVE this_proc;
    END IF;
    
    IF userRepetido IS NOT NULL THEN
		SELECT 2 AS resultado;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Nombre de usuario ya existe.';
		LEAVE this_proc;    
    END IF;
    #Nueva persona
	INSERT INTO PERSONA (nombre,correo,edad,direccion,telefono)
	VALUES (nombreIN,correoIN,edadIN,direccionIN,telefonoIN);
    #Get la ultima persona registrada
    
    SET ultimaPersona = (SELECT id_persona FROM PERSONA WHERE
    nombre = nombreIN AND correo = correoIN AND edad = edadIN AND direccion = direccionIN
    AND telefono = telefonoIN);
    
    #Get el ultimo registro
	#SET ultimaPersona = (SELECT id_persona FROM PERSONA ORDER BY id_persona DESC limit 1);
    #Crear perfil si persona es doctor o cliente
	IF tipoUsuarioIN = 4 THEN
		#El usuario es un cliente
        INSERT INTO CLIENTE (id_persona,id_t_cl)
        VALUES (ultimaPersona,1);
    ELSEIF tipoUsuarioIN = 2 THEN
		#El usuario es un doctor
        INSERT INTO MEDICO (id_persona,id_especialidad)
        VALUES (ultimaPersona,especialidadIN);	
        #Crear el horario
		INSERT INTO H_L (id_dia,id_horario,id_persona)
        VALUES (1,horarioIN,ultimaPersona);
        SET altaUsuario = 2;
	ELSEIF tipoUsuarioIN = 3 THEN
        #Crear el horario
		INSERT INTO H_L (id_dia,id_horario,id_persona)
        VALUES (1,horarioIN,ultimaPersona);  
        SET altaUsuario = 2;
    END IF;
    
    #Crear el usuario
    INSERT INTO USUARIO (id_usuario, nombre_usuario,pass,id_persona,id_e_us,id_t_us,puntos)
    VALUES (ultimaPersona, nombreUsuarioIN,passIN,ultimaPersona,altaUsuario,tipoUsuarioIN,0);
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearPersonaEmergencia
DELIMITER //
CREATE PROCEDURE crearPersonaEmergencia(
	IN tipoUsuarioIN INT,
    IN nombreIN VARCHAR(100),
    IN correoIN VARCHAR(100),
    IN edadIN INT,
    IN direccionIN VARCHAR(100),
    IN telefonoIN VARCHAR(100),
    IN passIN VARCHAR(256),
    IN nombreUsuarioIN VARCHAR(100),
    IN especialidadIN INT,
    IN horarioIN INT
)
this_proc:BEGIN
	DECLARE ultimaPersona INT;
    DECLARE repetido INT;
    DECLARE userRepetido INT;
    DECLARE altaUsuario INT;
    SET altaUsuario = 2;
    #Conseguir repetido
    SET repetido = (SELECT id_persona FROM PERSONA WHERE LOWER(nombre) = LOWER(nombreIN) 
    AND LOWER(correo) = LOWER(correoIN) limit 1);
    SET userRepetido = (SELECT id_usuario FROM USUARIO WHERE nombre_usuario = nombreUsuarioIN limit 1);
    #Error si es repetido
    IF repetido IS NOT NULL THEN
		SELECT 0 AS resultado;
		LEAVE this_proc;
    END IF;
    
    IF userRepetido IS NOT NULL THEN
		SELECT 2 AS resultado;
		LEAVE this_proc;    
    END IF;
    #Nueva persona
	INSERT INTO PERSONA (nombre,correo,edad,direccion,telefono)
	VALUES (nombreIN,correoIN,edadIN,direccionIN,telefonoIN);
    #Get el ultimo registro
	#SET ultimaPersona = (SELECT id_persona FROM PERSONA ORDER BY id_persona DESC limit 1);
    SET ultimaPersona = (SELECT id_persona FROM PERSONA WHERE
    nombre = nombreIN AND correo = correoIN AND edad = edadIN AND direccion = direccionIN
    AND telefono = telefonoIN);
    #Crear perfil si persona es doctor o cliente
	IF tipoUsuarioIN = 4 THEN
		#El usuario es un cliente
        INSERT INTO CLIENTE (id_persona,id_t_cl)
        VALUES (ultimaPersona,2);
    ELSEIF tipoUsuarioIN = 2 THEN
		#El usuario es un doctor
        INSERT INTO MEDICO (id_persona,id_especialidad)
        VALUES (ultimaPersona,especialidadIN);	
        #Crear el horario
		INSERT INTO H_L (id_dia,id_horario,id_persona)
        VALUES (1,horarioIN,ultimaPersona);
        SET altaUsuario = 2;
	ELSEIF tipoUsuarioIN = 3 THEN
        #Crear el horario
		INSERT INTO H_L (id_dia,id_horario,id_persona)
        VALUES (1,horarioIN,ultimaPersona);  
        SET altaUsuario = 2;
    END IF;
    
    #Crear el usuario
    INSERT INTO USUARIO (nombre_usuario,pass,id_persona,id_e_us,id_t_us,puntos)
    VALUES (nombreUsuarioIN,passIN,ultimaPersona,altaUsuario,tipoUsuarioIN,0);
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizarEstadoUsuario
DELIMITER //
CREATE PROCEDURE actualizarEstadoUsuario(
	IN idUsuarioIN INT,
    IN estadoIN INT
)
this_proc:BEGIN
	UPDATE USUARIO
    SET id_e_us = estadoIN
    WHERE id_usuario = idUsuarioIN;
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

call obtenerServicios();

#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS iniciarSesion
DELIMITER //
CREATE PROCEDURE iniciarSesion(
	IN usuarioIN VARCHAR(100)
)
this_proc:BEGIN
	DECLARE idUsuario INT;
    DECLARE passGuardada VARCHAR(256);
    DECLARE estadoUsuario INT;
    DECLARE tipoUsuario INT;
    #Verificar que el usuario exista
    SELECT id_usuario,id_e_us,id_t_us INTO idUsuario,estadoUsuario,tipoUsuario
    FROM USUARIO WHERE LOWER(nombre_usuario) = LOWER(usuarioIN);
    IF idUsuario IS NULL THEN
		SELECT 0 AS resultado;
        LEAVE this_proc;
    END IF;
    
    #Verificar que el usuario no este de baja
    IF estadoUsuario = 3 THEN
		SELECT 3 AS resultado;
        LEAVE this_proc;
    END IF;
    
    #Verificar que el usuario este de alta
    IF estadoUsuario = 1 THEN
		SELECT 4 AS resultado;
        LEAVE this_proc;
    END IF;    
    
    #Recuperar la contraseña
    SET passGuardada = (SELECT USUARIO.pass FROM USUARIO WHERE id_usuario = idUsuario);

    #Retornar datos para comparación en backend
	IF tipoUsuario = 2 THEN
		SELECT 1 AS resultado, id_usuario,nombre_usuario,a.pass, b.nombre AS estado,c.nombre AS tipo,
		d.nombre,d.correo,d.edad,d.direccion,d.telefono,f.nombre AS especialidad, 
        a.puntos,a.id_t_us FROM USUARIO a
		INNER JOIN E_US b
		ON a.id_e_us = b.id_e_us
		INNER JOIN T_US c
		ON c.id_t_us = a.id_t_us
		INNER JOIN PERSONA d
		ON a.id_persona = d.id_persona
		INNER JOIN MEDICO e
		ON e.id_persona = a.id_persona
		INNER JOIN ESPECIALIDAD f
		ON e.id_especialidad = f.id_especialidad
		WHERE a.id_usuario = idUsuario;
		LEAVE this_proc;
	ELSE
		SELECT 1 AS resultado, id_usuario,nombre_usuario, b.nombre AS estado,c.nombre AS tipo,
		d.nombre,d.correo,d.edad,d.direccion,d.telefono, a.puntos, 
        a.pass, a.id_t_us FROM USUARIO a
		INNER JOIN E_US b
		ON a.id_e_us = b.id_e_us
		INNER JOIN T_US c
		ON c.id_t_us = a.id_t_us
		INNER JOIN PERSONA d
		ON a.id_persona = d.id_persona
		WHERE a.id_usuario = idUsuario;
		LEAVE this_proc;       
	END IF;
    LEAVE this_proc;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMedicos
DELIMITER //
CREATE PROCEDURE obtenerMedicos(
)
this_proc:BEGIN
	#Retornar todos los médicos con los siguientes datos
    #Nombre-Especialidad-id_especialidad-Horario-id_horario
    SELECT a.nombre,c.id_especialidad,d.nombre AS especialidad,
    e.id_horario AS id_horario, f.nombre as horario FROM PERSONA a 
    INNER JOIN USUARIO b
    ON a.id_persona = b.id_persona
    INNER JOIN MEDICO c
    ON c.id_persona = b.id_persona
    INNER JOIN ESPECIALIDAD d
    ON c.id_especialidad = d.id_especialidad
    INNER JOIN H_L e
    ON e.id_persona = c.id_persona
    INNER JOIN HORARIO f
    ON f.id_horario = e.id_horario
    WHERE b.id_t_us = 2;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearMascota
DELIMITER //
CREATE PROCEDURE crearMascota(
	IN usuarioIN INT,
	IN nombreIN VARCHAR(100),
    IN edadIN INT,
    IN fotoIN VARCHAR(300),
    IN generoIN INT,
    IN razaIN INT
)
this_proc:BEGIN
	DECLARE yaExiste INT;
    DECLARE yaAsignada INT;
    DECLARE idMascota INT;
    SET yaExiste = (SELECT id_mascota FROM MASCOTA WHERE LOWER(nombre) = LOWER(nombreIN));
    #Verificar si ya existe ese nombre de mascota
    IF yaExiste IS NOT NULL THEN
		#Verificar que esa mascota este al nombre del usuario
        SET yaAsignada = (SELECT id_mascota FROM MA_D WHERE id_usuario = usuarioIN
        AND id_mascota = yaExiste);
        IF yaAsignada IS NOT NULL THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el nombre de mascota ya existe.';
			#SELECT 0 AS resultado, 'Error, el nombre de mascota ya existe.' AS mensaje;
			LEAVE this_proc;
        END IF;
    END IF;
    #Crear la mascota
    INSERT INTO MASCOTA (nombre,edad,foto,genero,id_raza)
    VALUES (nombreIN,edadIN,fotoIN,generoIN,razaIN);
    #Recuperar id de la mascota
    SET idMascota = (SELECT id_mascota FROM MASCOTA ORDER BY id_mascota DESC limit 1);
    SET idMascota = (
		SELECT id_mascota FROM MASCOTA 
        WHERE nombre = nombreIN AND edad = edadIN
        AND genero = generoIN AND id_raza = razaIN
    );
    #Asignar mascota
    INSERT INTO MA_D (id_usuario,id_mascota)
    VALUES (usuarioIN,idMascota);
    SELECT 1 AS resultado, 'Mascota creada con éxito.' AS mensaje;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerUsuarios
DELIMITER //
CREATE PROCEDURE obtenerUsuarios(
	IN tipoUsuarioIN INT
)
this_proc:BEGIN
	#Retornar todos los usuarios
    SELECT a.id_usuario as id,a.nombre_usuario,b.nombre,b.correo,b.edad,b.direccion,
    b.telefono, c.id_e_us as estado, d.nombre as tipo FROM USUARIO a
    INNER JOIN PERSONA b
    ON a.id_persona = b.id_persona
    INNER JOIN E_US c
    ON a.id_e_us = c.id_e_us
    INNER JOIN T_US d
    ON a.id_t_us = d.id_t_us
    WHERE a.id_t_us = tipoUsuarioIN
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMascotas
DELIMITER //
CREATE PROCEDURE obtenerMascotas(
	IN usuarioIN INT
)
this_proc:BEGIN
	#Retornar todas las mascotas del usuario
    SELECT a.id_mascota,a.nombre,a.edad,a.foto,
    CASE WHEN a.genero = 1 THEN 'Masculino'
    WHEN a.genero = 2 THEN 'Femenino'
    END AS genero, b.nombre AS raza
    FROM MASCOTA a
    INNER JOIN RAZA b
    ON a.id_raza = b.id_raza
    INNER JOIN MA_D c
    ON a.id_mascota = c.id_mascota
    WHERE c.id_usuario = usuarioIN
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearRaza
DELIMITER //
CREATE PROCEDURE crearRaza(
	IN razaIN VARCHAR(100)
)
this_proc:BEGIN
	DECLARE razaRepetida INT;
    SELECT id_raza INTO razaRepetida FROM RAZA WHERE LOWER(nombre) = LOWER(razaIN); 
    #Verificar que la raza ya exista.
    IF razaRepetida IS NOT NULL THEN
		SELECT 0 AS resultado;
		LEAVE this_proc;
    END IF;
    #Crear la raza
    INSERT INTO RAZA (nombre) 
    VALUES (razaIN);
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerRazas
DELIMITER //
CREATE PROCEDURE obtenerRazas(
)
this_proc:BEGIN
	#Retornar todas las mascotas del usuario
    SELECT * FROM RAZA ORDER BY nombre ASC;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS manejarDescuento
DELIMITER //
CREATE PROCEDURE manejarDescuento(
	IN tipoAccionIN INT,
    IN nombreNuevoIN VARCHAR(100),
    IN cantidadNuevoIN NUMERIC(6,2),
    IN idDescuentoIN INT
)
this_proc:BEGIN
	DECLARE nombreRepetido INT;
    DECLARE descuentoActivo INT;
    DECLARE descuentoEliminado INT;
    #Verificar si existe el nombre
    SET nombreRepetido = (SELECT id_descuento FROM DESCUENTO 
	WHERE LOWER(nombre) = LOWER(nombreNuevoIN));

	IF tipoAccionIN = 1 THEN
    	IF nombreRepetido IS NOT NULL THEN
		#ERROR, nombre ya existe
			SELECT 0 AS resultado;
			LEAVE this_proc;
		END IF;
    #Crear nuevo descuento
		INSERT INTO DESCUENTO (nombre,cantidad,id_e_de)
        VALUES (nombreNuevoIN,cantidadNuevoIN,2);

        SELECT 1 AS resultado;
        LEAVE this_proc;
    END IF;
    #Recuperar estado eliminado
    SET descuentoEliminado = (SELECT id_e_de FROM DESCUENTO 
    WHERE id_descuento = idDescuentoIN);
    
    IF tipoAccionIN = 3 THEN
		#Desactivar descuento
        #Verificar que el descuento no este eliminado
        IF descuentoEliminado = 3 THEN
			SELECT 3 AS resultado;
            LEAVE this_proc;
        END IF;
        #Activar descuento 
        UPDATE DESCUENTO SET id_e_de = 2 WHERE id_descuento = idDescuentoIN;
	END IF;
    
    IF tipoAccionIN = 2 THEN
    #Activar descuento
    #Verificar si no hay otro descuento activo.
		SELECT COUNT(id_descuento) INTO descuentoActivo FROM DESCUENTO 
        WHERE id_e_de = 1;
        
		IF descuentoActivo > 0 THEN
		#ERROR, ya hay un descuento activado
			SELECT 2 AS resultado;
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, ya hay un descuento activo.';
			LEAVE this_proc;
		END IF;
        
        #Verificar que el descuento no este eliminado
        IF descuentoEliminado = 3 THEN
			SELECT 3 AS resultado;
            LEAVE this_proc;
        END IF;
        #Activar descuento 
        UPDATE DESCUENTO SET id_e_de = 1 WHERE id_descuento = idDescuentoIN;
	END IF;

    IF tipoAccionIN = 4 THEN
		#Eliminar descuento
        #Verificar que el descuento no este eliminado
        IF descuentoEliminado = 3 THEN
			SELECT 3 AS resultado;
            LEAVE this_proc;
        END IF;
        #Activar descuento 
        UPDATE DESCUENTO SET id_e_de = 3 WHERE id_descuento = idDescuentoIN;
	END IF;
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerDescuentos
DELIMITER //
CREATE PROCEDURE obtenerDescuentos(
)
this_proc:BEGIN
	#Retornar todas las mascotas del usuario
    SELECT id_descuento AS id, nombre, cantidad, id_e_de AS estado 
    FROM DESCUENTO;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearEstudio
DELIMITER //
CREATE PROCEDURE crearEstudio(
	IN nombreIN VARCHAR(100),
    IN direccionIN VARCHAR(300),
    IN idMotivoIN INT
)
this_proc:BEGIN
	#Crear nuevo estudio
    INSERT INTO ESTUDIO (direccion,nombre,id_d_mo)
	VALUES (direccionIN,nombreIN,idMotivoIN)
	;
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerEstudios
DELIMITER //
CREATE PROCEDURE obtenerEstudios(
    IN id_mascotaIN INT
)
this_proc:BEGIN
	#Obtener estudios de mascota
    SELECT a.id_estudio as id,a.direccion,a.nombre, b.fecha as cita FROM ESTUDIO a
    INNER JOIN CITA b
    ON b.id_cita = a.id_cita
    WHERE b.id_mascota = id_mascotaIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerCalendarioOcupadoDoctor
DELIMITER //
CREATE PROCEDURE obtenerCalendarioOcupadoDoctor(
    IN idMedicoIN INT
)
this_proc:BEGIN
	SELECT c.fecha,a.hora_inicio as inicio, a.hora_fin as fin, a.id_sala, b.nombre as sala,
    a.id_cita,d.nombre as tipo_cita, e.nombre as medico FROM D_MO a
    INNER JOIN SALA b
    ON a.id_sala = b.id_sala
    INNER JOIN CITA c
    ON c.id_cita = a.id_cita
    INNER JOIN T_CI d
    ON d.id_t_ci = c.id_t_ci
    INNER JOIN PERSONA e
    ON e.id_persona = a.id_persona
    WHERE c.id_e_ci != 3 
    AND e.id_persona = idMedicoIN
    ORDER BY c.fecha,a.hora_inicio,a.hora_fin,a.id_sala DESC
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerCalendarioOcupadoGeneral
DELIMITER //
CREATE PROCEDURE obtenerCalendarioOcupadoGeneral(
)
this_proc:BEGIN
	SELECT c.fecha,a.hora_inicio as inicio, a.hora_fin as fin, a.id_sala, b.nombre as sala,
    a.id_d_mo as motivo,d.nombre as tipo_cita, e.id_persona as id_medico, e.nombre as medico, 
    h.id_especialidad, h.nombre as especialidad FROM D_MO a
    INNER JOIN SALA b
    ON a.id_sala = b.id_sala
    INNER JOIN CITA c
    ON c.id_cita = a.id_cita
    INNER JOIN T_CI d
    ON d.id_t_ci = c.id_t_ci
    INNER JOIN PERSONA e
    ON e.id_persona = a.id_persona
    INNER JOIN MEDICO g
    ON g.id_persona = a.id_persona
    INNER JOIN ESPECIALIDAD h
    ON h.id_especialidad = g.id_especialidad
    WHERE c.id_e_ci < 3 
    GROUP BY
    c.fecha,a.hora_inicio, a.hora_fin, a.id_sala, b.nombre,
    a.id_d_mo,d.nombre, e.id_persona, e.nombre, 
    h.id_especialidad, h.nombre
    ORDER BY c.fecha,a.hora_inicio,a.hora_fin,a.id_sala DESC
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerHistorialCitasMedico
DELIMITER //
CREATE PROCEDURE obtenerHistorialCitasMedico(
	IN idMedicoIN INT,
    IN tipoCitasIN INT
)
this_proc:BEGIN
	IF tipoCitasIN = 1 THEN
		SELECT * FROM vistaHistorialCitasMedico
		WHERE estado_motivo = 3 
		AND idMedico = idMedicoIN 
        AND (estado_pago = 2
        OR (estado_pago = 1 AND id_tipo_cita = 2 AND estado_motivo = 3))
		ORDER BY fecha,inicio,fin DESC;
    ELSE
		SELECT * FROM vistaHistorialCitasMedico
		WHERE estado_motivo = 1 
		AND idMedico = idMedicoIN
        AND (estado_pago = 2
        OR (estado_pago = 1 AND id_tipo_cita = 2 AND estado_motivo = 1))
		ORDER BY fecha,inicio,fin DESC;
    END IF;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerHistorialCitasCliente
DELIMITER //
CREATE PROCEDURE obtenerHistorialCitasCliente(
	IN idClienteIN INT,
    IN tipoCitasIN INT
)
this_proc:BEGIN
	IF tipoCitasIN = 1 THEN
		SELECT * FROM vistaHistorialCitasCliente
		WHERE estado_motivo = 3 
		AND idCliente = idClienteIN
        AND (estado_pago = 2
        OR (estado_pago = 1 AND id_tipo_cita = 2 AND estado_motivo = 3))
		ORDER BY fecha,inicio,fin DESC;
        
    ELSE
		SELECT * FROM vistaHistorialCitasCliente
		WHERE estado_motivo = 1 
		AND idCliente = idClienteIN
        AND (estado_pago = 2
        OR (estado_pago = 1 AND id_tipo_cita = 2 AND estado_motivo != 3))
		ORDER BY fecha,inicio,fin DESC;
    END IF;
    
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerCitaActualMedico
DELIMITER //
CREATE PROCEDURE obtenerCitaActualMedico(
	IN idMedicoIN INT
)
this_proc:BEGIN
	SELECT a.hora_inicio as inicio,a.id_d_mo as id_motivo, b.nombre as motivo, 
    c.nombre as sala, e.id_mascota, e.nombre as mascota, h.nombre as dueño
    FROM D_MO a
    INNER JOIN MOTIVO b
    ON a.id_motivo = b.id_motivo
    INNER JOIN SALA c
    ON c.id_sala = a.id_sala
    INNER JOIN CITA d
	ON d.id_cita = a.id_cita
    INNER JOIN MASCOTA e
    ON e.id_mascota = d.id_mascota
    INNER JOIN MA_D f
    ON e.id_mascota = f.id_mascota
    INNER JOIN USUARIO g
    ON g.id_usuario = f.id_usuario
    INNER JOIN PERSONA h
    ON h.id_persona = g.id_persona
    WHERE a.id_e_mo != 3
    ORDER BY a.id_d_mo, a.hora_inicio
    LIMIT 1
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearCita
DELIMITER //
CREATE PROCEDURE crearCita(
    IN fechaIN VARCHAR(10),
    IN idMascotaIN INT,
    #IN idDescuentoIN INT,
    IN idTipoCitaIN INT,
    IN puntosIN INT,
    IN addPuntosIN INT
)
this_proc:BEGIN
	DECLARE idCita INT;
    DECLARE idUsuario INT;
    DECLARE hayPagoPendiente INT;
    #Conseguir id de usuario desde el id de la mascota
	SET idUsuario = (SELECT id_usuario FROM MA_D WHERE id_mascota = idMascotaIN);
    #Verificar que no tenga pagos pendientes
    SET hayPagoPendiente = (
    SELECT COUNT(c.id_e_pa) FROM CITA a
    INNER JOIN MA_D b
    ON a.id_mascota = b.id_mascota
    INNER JOIN PAGO c
    ON a.id_cita = c.id_cita
    WHERE b.id_usuario = idUsuario
    AND c.id_e_pa = 1
    );

    IF hayPagoPendiente THEN
		#SELECT 0 AS resultado, 'No puede crear más citas, tiene pagos pendientes.' AS message;
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No puede crear más citas, tiene pagos pendientes.';
        LEAVE this_proc;
    END IF;
    
	#fecha, costo, id_mascota, id_descuento, id_e_ci, id_t_ci
    INSERT INTO CITA (fecha,costo,id_mascota,/*id_descuento,*/id_e_ci,id_t_ci,puntos)
    VALUES (STR_TO_DATE(fechaIN,'%d-%m-%Y'),0,idMascotaIN,/*idDescuentoIN,*/1,idTipoCitaIN,puntosIN);
    SET idCita = (SELECT id_cita FROM CITA ORDER BY id_cita DESC limit 1);
    #Crear el pago
    #total, id cita, estado pago 1 pendiente, tipo pago 1 efectivo, id moneda 1 quetzal
    INSERT INTO PAGO (total,id_cita,id_e_pa,id_t_pa,id_moneda)
    VALUES (0,idCita,1,1,1);
    SELECT idCita;
    
    UPDATE USUARIO
    SET puntos = addPuntosIN + puntos;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS agregarMotivo
DELIMITER //
CREATE PROCEDURE agregarMotivo(
	IN horaInicioIN VARCHAR(10),
    IN idMotivoIN INT,
    IN idSalaIN INT,
    IN idCitaIN INT,
    IN idMedicoIN INT,
    IN totalIN NUMERIC(8,2),
    IN puntosIN INT
)
this_proc:BEGIN
	DECLARE tipoCita INT;
    DECLARE costoLocal NUMERIC(8,2);
    DECLARE horaInicio TIME;
    DECLARE horaFin TIME;
    DECLARE totalLocal NUMERIC(8,2);
    DECLARE tarifaExamen NUMERIC(8,2);
    DECLARE totalAnterior NUMERIC(8,2);
    DECLARE cantidadMotivos INT;
    DECLARE descuentoActivo INT;
    #Convertir horainicio
    SET horaInicio = STR_TO_DATE(horaInicioIN,'%H:%i:%s');
    #Calcular hora final
	#inicio,motivo,sala,cita,persona/medico
	#Conseguir el tipo de cita para el calculo del  costo
    SET tipoCita = (SELECT id_t_ci FROM CITA WHERE id_cita = idCitaIN);
    SET totalAnterior = (SELECT a.total FROM PAGO a WHERE id_cita = idCitaIN);
	IF tipoCita = 1 THEN
		SET costoLocal = (SELECT precio FROM MOTIVO WHERE id_motivo = idMotivoIN);
        SET horaFin = DATE_ADD(horaInicio, INTERVAL 1 HOUR);
        #Actualizar costo de la cita
        SET totalLocal = (SELECT a.costo FROM CITA a WHERE id_cita = idCitaIN);
		 UPDATE CITA
		 #SET costo = (totalLocal + costoLocal)
         SET costo = (totalLocal + totalIN)
		 WHERE id_cita = idCitaIN;
    ELSE
		SET costoLocal = 0;
        SET horaFin = horaInicio;
    END IF;
    #Insertar nuevo motivo
	INSERT INTO D_MO (costo,hora_inicio,hora_fin,id_motivo,id_sala,id_cita,id_persona,id_e_mo,puntos)
    VALUES (totalIN,horaInicio,horaFin,idMotivoIN,idSalaIN,idCitaIN,idMedicoIN,1,puntosIN);
   
    #Actualizar costo si es cita normal
    IF tipoCita = 1 THEN
		#Conseguir el precio del servicio
		SET tarifaExamen = (SELECT precio FROM MOTIVO WHERE id_motivo = idMotivoIN);
        
		#Actualizar el total de la factura
        UPDATE PAGO 
        #SET total = (totalAnterior+tarifaExamen)
        SET total = (totalAnterior+totalIN)
        WHERE id_cita = idCitaIN;
    END IF;
    #Verificar si hay que aplicar descuento
    SET cantidadMotivos = (SELECT COUNT(id_d_mo) FROM D_MO WHERE id_cita = idCitaIN);
    IF cantidadMotivos > 1 AND tipoCita = 1 THEN
		#Recuperar el descuento activo
        SET descuentoActivo = (SELECT id_descuento FROM DESCUENTO WHERE id_e_de = 1 limit 1);
        IF descuentoActivo IS NOT NULL THEN 
			UPDATE CITA
            SET id_descuento = descuentoActivo
            WHERE id_cita = idCitaIN;
        END IF;
    END IF;
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizarMotivo
DELIMITER //
CREATE PROCEDURE actualizarMotivo(
	IN idMotivoIN INT,
    IN tipoAccionIN INT
)
this_proc:BEGIN
/*
	Acciones
    1 -> Activar motivo
    2 -> Finalizar motivo
*/
	DECLARE idCita INT;
    DECLARE tipoCita INT;
    DECLARE tipoCliente INT;
    DECLARE horaInicio TIME;
    DECLARE horaFin TIME;
    DECLARE tiempoEmergencia INT;
    DECLARE costoEmergencia NUMERIC(8,2) DEFAULT 0.00;
    DECLARE costoExtra INT DEFAULT 1;
    DECLARE motivosRestantes INT;
    DECLARE totalConDescuento NUMERIC(8,2);
    DECLARE descuentoAaplicar NUMERIC(8,2);
    DECLARE conteoCitas INT;
    DECLARE idCliente INT;
    DECLARE numRandom INT;
    DECLARE costoCita NUMERIC(8,2);
    DECLARE totalPago NUMERIC(8,2);
    #Recuperar la cita de ese motivo y el tipo de cita
    SELECT a.id_cita, b.id_t_ci INTO idCita,tipoCita FROM D_MO a
    INNER JOIN CITA b
    ON a.id_cita = b.id_cita
    WHERE a.id_d_mo = idMotivoIN;
    
    IF tipoAccionIN = 1 THEN
		UPDATE D_MO
		SET id_e_mo = 2
		WHERE id_d_mo = idMotivoIN;
        SELECT 1 as resultado;
		LEAVE this_proc;
    END IF;
	#Actualizar estado del motivo
	UPDATE D_MO
	SET id_e_mo = 3
	WHERE id_d_mo = idMotivoIN;
    #Verificar si es de emergencia y actualizar la fecha fin y el precio
    /*
		Precio:
        Tiempo * 35
        Si es mayor a 5 horas se multiplica por 30
    */
    #Si la cita es por emergencia
    IF tipoCita = 2 THEN
	/* select * from d_mo;
		UPDATE D_MO
        SET hora_fin = NOW()
        WHERE id_d_mo = idMotivoIN;
	*/
        #Recuperar las horas
        SELECT hora_inicio INTO horaInicio FROM D_MO
        WHERE id_d_mo = idMotivoIN;
        #Calcular el random para la hora
        SET numRandom = FLOOR(RAND()*(8-0+0)+1);
		#Calcular la hora fin
        SET horaFin = (DATE_ADD(horaInicio, INTERVAL numRandom HOUR));
        #Calcular tiempo del motivo
        SET tiempoEmergencia = (SELECT time_to_sec(timediff(horaFin, horaInicio))/60);
        #Verificar el tema de las 5 horas
        /*
        IF tiempoEmergencia > 300 THEN
			SET costoExtra = 30;
        END IF;
        WHILE tiempoEmergencia > 60 DO
			SET tiempoEmergencia = tiempoEmergencia - 60;
			SET costoEmergencia = costoEmergencia + 35;
		END WHILE;
        */
        
        #Agregar los minutos que queden sobrando
        
        /*
        IF tiempoEmergencia > 0 THEN
			SET costoEmergencia = costoEmergencia + (tiempoEmergencia * 30);
        END IF;
        #Agregar el precio extra por las 5 horas
        SET costoEmergencia = costoEmergencia * costoExtra;
        */
        #Calcular el costo de la cita
		IF tiempoEmergencia > 300 THEN
			SET costoExtra = 30;
		ELSE 
			SET costoExtra = 35;
        END IF;
        SET costoEmergencia = numRandom * costoExtra;
        
        #Actualizar el costo de la cita y del pago
        SET costoCita = (SELECT costo FROM CITA WHERE id_cita = idCita);
        SET totalPago = (SELECT total FROM PAGO WHERE id_cita = idCita);
        #Agregarles valor default 0 si son null
        IF costoCita IS NULL THEN
			SET costoCita = 0.00;
		END IF;
			
        IF totalPago IS NULL THEN
			SET totalPago = 0.00;
        END IF;
        SELECT tiempoEmergencia,horaFin,numRandom,costoEmergencia,costoCita,totalPago;
        UPDATE CITA
        SET costo = costoCita + costoEmergencia
        WHERE id_cita = idCita;
        
        UPDATE PAGO
        SET total = totalPago + costoEmergencia
        WHERE id_cita = idCita;
        #Actualizar datos del motivo a cerrar
        UPDATE D_MO
        SET hora_fin = horaFin
        WHERE id_d_mo = idMotivoIN;
        
        UPDATE D_MO
        SET costo = costoEmergencia
        WHERE id_d_mo = idMotivoIN;
    END IF;
	#idCita
    CALL calcularTotal(idCita);
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

SELECT * FROM FACTURA;
SELECT * FROM D_FA;
SELECT * FROM PAGO;
call obtenerPuntos(17);
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS cambiarTipoCliente
DELIMITER //
CREATE PROCEDURE cambiarTipoCliente(
    IN idClienteIN VARCHAR(10)
)
this_proc:BEGIN
	DECLARE conteoCitas INT;
	#Cambiar estado de cliente por si se volvio recurrente
	SET conteoCitas = (SELECT COUNT(id_cita) FROM CITA a
	INNER JOIN MASCOTA b
	ON a.id_mascota = b.id_mascota
	INNER JOIN MA_D c
	ON b.id_mascota = c.id_mascota
	INNER JOIN USUARIO d
	ON d.id_usuario = c.id_usuario
	INNER JOIN CLIENTE e
	ON d.id_persona = e.id_persona
	WHERE e.id_persona = idClienteIN);
    
	IF conteoCitas > 2 THEN
		UPDATE CLIENTE
		SET id_t_cl = 2
		WHERE id_persona = idClienteIN;
	END IF;
    
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS calcularTotal
DELIMITER //
CREATE PROCEDURE calcularTotal(
    IN idCitaIN INT
)
this_proc:BEGIN
	DECLARE motivosRestantes INT;
    DECLARE totalConDescuento NUMERIC(8,2);
    DECLARE tipoCliente INT;
    DECLARE idCliente INT;
    DECLARE descuentoAaplicar NUMERIC(8,2);
    DECLARE tipoCita INT;
    DECLARE totalCitaEmergencia NUMERIC(8,2);

    #Recuperar el tipo de cita
    #SET tipoCita = (SELECT id_t_ci FROM CITA WHERE id_cita = idCitaIN);
	#Verifiar que todos los motivos esten cerrados para calcular los descuentos
    SET motivosRestantes = (SELECT COUNT(id_motivo) FROM D_MO 
    WHERE id_cita = idCitaIN
    AND id_e_mo = 1 /*OR id_e_mo = 2*/);
    #Recuperar el tipo de cita
	SELECT costo,id_t_ci INTO totalCitaEmergencia , tipoCita 
    FROM CITA WHERE id_cita = idCitaIN;
    
    IF tipoCita = 1 AND motivosRestantes = 0 THEN
		UPDATE CITA
        SET id_e_ci = 3
        WHERE id_cita = idCitaIN;
        CALL cambiarTipoCliente(idCliente);
		SELECT 1 AS resultado;
    END IF;
    
    IF motivosRestantes = 0 AND tipoCita = 2 THEN
		#Finalizar cita
		UPDATE CITA
        SET id_e_ci = 3
        WHERE id_cita = idCitaIN;
        #Recuperar total sin descuento
		SET totalConDescuento = (SELECT costo FROM CITA WHERE id_cita = idCitaIN);
        #Recuperar el tipo del cliente
        SELECT e.id_t_cl, e.id_persona INTO tipoCliente, idCliente FROM CITA a
        INNER JOIN MASCOTA b
        ON a.id_mascota = b.id_mascota
        INNER JOIN MA_D c
        ON b.id_mascota = c.id_mascota
        INNER JOIN USUARIO d
        ON d.id_usuario = c.id_usuario
        INNER JOIN CLIENTE e
        ON d.id_persona = e.id_persona
        WHERE a.id_cita = idCitaIN;
        #Agregar descuento del 15% si es cliente recurrente
        /*
        IF tipoCliente = 2 THEN
			SET totalConDescuento = totalConDescuento - (totalConDescuento * 0.15);
        END IF;
        */
        #Agregar demas descuentos si existen descuentos agregados
        /*
        SET descuentoAaplicar = (SELECT cantidad FROM CITA a 
        INNER JOIN DESCUENTO b
        ON a.id_descuento = b.id_descuento
        WHERE id_cita = idCitaIN
        );
        */
        /*
		IF descuentoAaplicar IS NOT NULL THEN
			SET totalConDescuento = totalConDescuento - (totalConDescuento * descuentoAaplicar);
        END IF;
        */
        
        #ESTO LO ACABO DE MODIFICAR Y ACTUALIZABA LOS DESCUENTOS PERO YA NO HAY
        /*
        IF tipoCita = 1 THEN
        #Actualizar el pago cita con descuentos
			UPDATE PAGO
			SET total = totalConDescuento
			WHERE id_cita = idCitaIN;
		ELSE
        #Actualizar el pago de las citas por emergencia
 			UPDATE PAGO
			SET total = totalCitaEmergencia
			WHERE id_cita = idCitaIN;       
        select * from T_CI;
        END IF;
        */
		#idCliente
		CALL cambiarTipoCliente(idCliente);
    END IF;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


DROP PROCEDURE IF EXISTS calcularTotalCitaNormal
DELIMITER //
CREATE PROCEDURE calcularTotalCitaNormal(
	IN idCitaIN INT
)
this_proc:BEGIN
	DECLARE motivosRestantes INT;
    DECLARE totalConDescuento NUMERIC(8,2) DEFAULT 1;
    DECLARE tipoCliente INT;
    DECLARE idCliente INT;
    DECLARE descuentoAaplicar NUMERIC(8,2);
    DECLARE tipoCita INT;
    DECLARE totalCitaEmergencia NUMERIC(8,2);
    DECLARE costoCita NUMERIC(8,2);
    
		SELECT total INTO costoCITA FROM PAGO
        WHERE id_cita = idCitaIN
        ;
        #Recuperar total sin descuento
		SET totalConDescuento = (SELECT costo FROM CITA WHERE id_cita = idCitaIN);
        #Recuperar el tipo del cliente
        SELECT e.id_t_cl, e.id_persona INTO tipoCliente, idCliente FROM CITA a
        INNER JOIN MASCOTA b
        ON a.id_mascota = b.id_mascota
        INNER JOIN MA_D c
        ON b.id_mascota = c.id_mascota
        INNER JOIN USUARIO d
        ON d.id_usuario = c.id_usuario
        INNER JOIN CLIENTE e
        ON d.id_persona = e.id_persona
        WHERE a.id_cita = idCitaIN;
        #Agregar descuento del 15% si es cliente recurrente
        IF tipoCliente = 2 THEN
			SET totalConDescuento = totalConDescuento - (totalConDescuento * 0.15);
        END IF;
        #Agregar demas descuentos si existen descuentos agregados
        SET descuentoAaplicar = (SELECT b.cantidad FROM CITA a 
        INNER JOIN DESCUENTO b
        ON a.id_descuento = b.id_descuento
        WHERE id_cita = idCitaIN
        );
		IF descuentoAaplicar IS NOT NULL THEN
			SET totalConDescuento = totalConDescuento - (totalConDescuento * descuentoAaplicar);
        END IF;
        #Actualizar el pago
		UPDATE PAGO
		SET total = totalConDescuento
		WHERE id_cita = idCitaIN;
		#idCliente
		CALL cambiarTipoCliente(idCliente);
END //;
DELIMITER ;


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearReceta
DELIMITER //
CREATE PROCEDURE crearReceta(
    IN idMotivoIN INT,
    IN descripcionIN VARCHAR(300)
)
this_proc:BEGIN
	DECLARE idReceta INT;
	INSERT INTO RECETA (id_d_mo,descripcion) VALUES (idMotivoIN,descripcionIN);
    SET idReceta = (SELECT id_receta FROM RECETA ORDER BY id_receta DESC limit 1);
    SELECT idReceta as receta;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS agregarElementoReceta
DELIMITER //
CREATE PROCEDURE agregarElementoReceta(
    IN idRecetaIN INT,
    IN idMedicamentoIN INT
)
this_proc:BEGIN
	INSERT INTO D_RE (id_medicamento,id_receta) VALUES (idMedicamentoIN,idRecetaIN);
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerHistorialMedico
DELIMITER //
CREATE PROCEDURE obtenerHistorialMedico(
    IN idMascotaIN INT
)
this_proc:BEGIN
	SELECT a.fecha,b.id_d_mo as idMotivo, c.nombre as motivo,d.nombre as medico
    FROM CITA a
    INNER JOIN D_MO b
    ON a.id_cita = b.id_cita
    INNER JOIN MOTIVO c
    ON b.id_motivo = c.id_motivo
    INNER JOIN PERSONA d
    ON b.id_persona = d.id_persona
    GROUP BY a.fecha,c.nombre ,d.nombre,b.id_d_mo;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerRecetas
DELIMITER //
CREATE PROCEDURE obtenerRecetas(
    IN idMascotaIN INT
)
this_proc:BEGIN
SELECT e.id_receta as receta, a.fecha,c.nombre as motivo,d.nombre as medico, g.nombre as medicina
    FROM CITA a
    INNER JOIN D_MO b
    ON a.id_cita = b.id_cita
    INNER JOIN MOTIVO c
    ON b.id_motivo = c.id_motivo
    INNER JOIN PERSONA d
    ON b.id_persona = d.id_persona
    INNER JOIN RECETA e
    ON e.id_cita = b.id_cita
    INNER JOIN D_RE f
    ON e.id_receta = f.id_receta
    INNER JOIN MEDICAMENTO g
    ON g.id_medicamento = f.id_medicamento
    GROUP BY a.fecha,c.nombre ,d.nombre,g.nombre;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerSalas
DELIMITER //
CREATE PROCEDURE obtenerSalas(

)
this_proc:BEGIN
	SELECT id_sala as id, nombre from SALA;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

use f1ayd2;



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerPagoGenerado
DELIMITER //
CREATE PROCEDURE obtenerPagoGenerado(
	IN idUsuarioIN INT,
    IN estadoPagoIN INT
)
this_proc:BEGIN
    DECLARE tipoUsuario INT;
    DECLARE tipoCita INT;
    DECLARE idDescuento INT;
    DECLARE descuento NUMERIC(8,2);
    DECLARE totalConDescuento NUMERIC(8,2);
    DECLARE totalCita NUMERIC(8,2);
    #Retornos
	DECLARE idPago INT;
	DECLARE totalPago NUMERIC(8,2);
	DECLARE idCita INT;
	DECLARE estadoPago VARCHAR(100);
	DECLARE fechaCita DATE;
	DECLARE nombreMascota VARCHAR(100);
    DECLARE idCliente INT;
    DECLARE puntos INT;
    
	IF estadoPagoIN = 1 THEN
		#Actualizar tipo del cliente
        SET idCliente = (SELECT a.id_persona FROM CLIENTE a
        INNER JOIN USUARIO b
        ON a.id_persona = b.id_persona
        WHERE b.id_usuario = idUsuarioIN);
        CALL cambiarTipoCliente(idCliente);
		#Pago pendiente
        #Obtener tipoUsuario
        SET tipoUsuario = (SELECT id_t_cl FROM CLIENTE a
        INNER JOIN USUARIO b
        ON a.id_persona = b.id_persona
        WHERE b.id_usuario = idUsuarioIN);

        #VConseguir primer total
		SELECT a.id_pago ,a.total,a.id_cita, b.nombre,
        c.fecha, f.nombre, c.costo, c.id_t_ci, c.puntos
        INTO idPago,totalPago,idCita,estadoPago,
        fechaCita,nombreMascota,totalCita,tipoCita,puntos FROM PAGO a
		INNER JOIN E_PA b
		ON a.id_e_pa = b.id_e_pa
		INNER JOIN CITA c
		ON a.id_cita = c.id_cita
		INNER JOIN MA_D d
		ON c.id_mascota = d.id_mascota
		INNER JOIN USUARIO e
		ON d.id_usuario = e.id_usuario
        INNER JOIN MASCOTA f
        ON f.id_mascota = c.id_mascota
		WHERE e.id_usuario = idUsuarioIN
		AND a.id_e_pa=1
		ORDER BY a.id_pago DESC LIMIT 1;
        #Set el primer resultado para el costo con descuento
        SET totalConDescuento = totalPago;

        #Verificar que el usuario vale para el primer descuento de 0.15%
        IF tipoUsuario = 2 THEN
			SET totalConDescuento = totalConDescuento - (totalPago * 0.15);
        END IF;
        #Verificar si hay descuentos
        SET idDescuento = (SELECT a.id_descuento FROM CITA a
        INNER JOIN PAGO b
        ON a.id_cita = b.id_cita
        WHERE b.id_pago = idPago
        );

        IF idDescuento IS NOT NULL AND tipoUsuario = 2 THEN
			#Conseguir descuento
            SET descuento = (SELECT cantidad FROM DESCUENTO WHERE id_descuento = idDescuento);
            SET totalConDescuento = totalConDescuento - (totalConDescuento * descuento);
            #SELECT totalConDescuento;
        END IF;
        #SELECT totalConDescuento, totalPago, totalCita;
        IF totalConDescuento != totalPago AND totalPago = totalCita 
        AND tipoUsuario = 2 
        AND tipoCita != 2 THEN
			#Actualizar pago y poner los descuentos
            UPDATE PAGO
            SET total = totalConDescuento
            WHERE id_pago = idPago;
            
            SELECT idPago as id ,totalConDescuento as total ,idCita as cita ,
			estadoPago as estado,
			fechaCita as fecha,nombreMascota as mascota;
            LEAVE this_proc;
        END IF;
        
        #Retornar los datos 
        SELECT idPago as id ,totalPago as total ,idCita as cita ,
        estadoPago as estado,
        fechaCita as fecha,nombreMascota as mascota, puntos as 'puntos gastados';
        
 /*
 #SI no funciona este era el resultado sin nada mas
		SELECT a.id_pago as id,a.total,a.id_cita as cita, b.nombre as estado,
        c.fecha, f.nombre as mascota FROM PAGO a
		INNER JOIN E_PA b
		ON a.id_e_pa = b.id_e_pa
		INNER JOIN CITA c
		ON a.id_cita = c.id_cita
		INNER JOIN MA_D d
		ON c.id_mascota = d.id_mascota
		INNER JOIN USUARIO e
		ON d.id_usuario = e.id_usuario
        INNER JOIN MASCOTA f
        ON f.id_mascota = c.id_mascota
		WHERE e.id_usuario = idUsuarioIN
		AND a.id_e_pa=1
		ORDER BY a.id_pago DESC LIMIT 1;
        */
    ELSE
		SELECT /*a.id_pago as id,*/a.total,/*a.id_cita as cita,*/ b.nombre as estado, c.fecha,
        f.nombre as mascota, c.puntos as 'puntos gastados'
        FROM PAGO a
		INNER JOIN E_PA b
		ON a.id_e_pa = b.id_e_pa
		INNER JOIN CITA c
		ON a.id_cita = c.id_cita
		INNER JOIN MA_D d
		ON c.id_mascota = d.id_mascota
		INNER JOIN USUARIO e
		ON d.id_usuario = e.id_usuario
        #Nombre mascota
        INNER JOIN MASCOTA f
        ON f.id_mascota = d.id_mascota
		WHERE e.id_usuario = idUsuarioIN
		AND a.id_e_pa=2
		ORDER BY a.id_pago; #DESC LIMIT 1;
    END IF;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerDataPago
DELIMITER //
CREATE PROCEDURE obtenerDataPago(
	IN idPagoIN INT
)
this_proc:BEGIN
		SELECT a.id_pago as id,a.total,a.id_cita as cita, b.nombre as estado,
        c.fecha, f.nombre as mascota, g.correo, c.puntos FROM PAGO a
		INNER JOIN E_PA b
		ON a.id_e_pa = b.id_e_pa
		INNER JOIN CITA c
		ON a.id_cita = c.id_cita
		INNER JOIN MA_D d
		ON c.id_mascota = d.id_mascota
		INNER JOIN USUARIO e
		ON d.id_usuario = e.id_usuario
        INNER JOIN MASCOTA f
        ON f.id_mascota = c.id_mascota
		#Correo de la persona
        INNER JOIN PERSONA g
        ON g.id_persona = e.id_persona
        WHERE a.id_pago = idPagoIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS pagar
DELIMITER //
CREATE PROCEDURE pagar(
	IN idPagoIN INT,
    IN idTipoPagoIN INT,
    IN estadoPagoIN INT
)
this_proc:BEGIN
	DECLARE idCita INT;
	IF estadoPagoIN = 2 THEN
		UPDATE PAGO
		SET id_e_pa = 3
		WHERE id_pago = idPagoIN;
        
		UPDATE PAGO
		SET id_t_pa = idTipoPagoIN
		WHERE id_pago = idPagoIN;
		#Recuperar cita a eliminar
		SET idCita = (
			SELECT id_cita FROM PAGO
			WHERE id_pago = idPagoIN
		);

		UPDATE CITA
		SET id_e_ci = 4
		WHERE id_cita = idCita;
        SELECT 1 as resultado;
		LEAVE this_proc;
    END IF;


	UPDATE PAGO
	SET id_e_pa = 2
	WHERE id_pago = idPagoIN;
    
	UPDATE PAGO
	SET id_t_pa = idTipoPagoIN
	WHERE id_pago = idPagoIN;

    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerRecetasPorMotivo
DELIMITER //
CREATE PROCEDURE obtenerRecetasPorMotivo(
	IN idMotivoIN INT
)
this_proc:BEGIN
	SELECT id_receta,descripcion FROM RECETA WHERE id_d_mo = idMotivoIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerEstudiosPorMotivo
DELIMITER //
CREATE PROCEDURE obtenerEstudiosPorMotivo(
	IN idMotivoIN INT
)
this_proc:BEGIN
	SELECT id_estudio,direccion,nombre,id_d_mo as motivo 
    FROM ESTUDIO WHERE id_d_mo = idMotivoIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMedicinaPorReceta
DELIMITER //
CREATE PROCEDURE obtenerMedicinaPorReceta(
	IN idRecetaIN INT
)
this_proc:BEGIN
	SELECT b.id_medicamento, b.nombre as medicamento FROM D_RE a
    INNER JOIN MEDICAMENTO b
    ON a.id_medicamento = b.id_medicamento
    WHERE a.id_receta = idRecetaIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMotivosPorCita
DELIMITER //
CREATE PROCEDURE obtenerMotivosPorCita(
	IN idCitaIN INT,
    IN idMedicoIN INT
)
this_proc:BEGIN
	IF idMedicoIN = -1 THEN
		SELECT a.id_d_mo as idMotivo, e.nombre as motivo, 
		a.hora_inicio as inicio, a.hora_fin as fin,
		a.id_sala as idSala, b.nombre as sala,a.id_cita,a.id_persona as idMedico, 
		c.nombre as medico,a.id_e_mo as id_estado, d.nombre as estado
		FROM D_MO a
		INNER JOIN SALA b
		ON a.id_sala = b.id_sala
		INNER JOIN PERSONA c
		ON a.id_persona = c.id_persona
		INNER JOIN E_MO d
		ON a.id_e_mo = d.id_e_mo
		INNER JOIN MOTIVO e
		ON a.id_motivo = e.id_motivo
		WHERE id_cita = idCitaIN;    
    ELSE
		SELECT a.id_d_mo as idMotivo, e.nombre as motivo, 
		a.hora_inicio as inicio, a.hora_fin as fin,
		a.id_sala as idSala, b.nombre as sala,a.id_cita,a.id_persona as idMedico, 
		c.nombre as medico,a.id_e_mo as id_estado, d.nombre as estado
		FROM D_MO a
		INNER JOIN SALA b
		ON a.id_sala = b.id_sala
		INNER JOIN PERSONA c
		ON a.id_persona = c.id_persona
		INNER JOIN E_MO d
		ON a.id_e_mo = d.id_e_mo
		INNER JOIN MOTIVO e
		ON a.id_motivo = e.id_motivo
		WHERE id_cita = idCitaIN
        AND a.id_persona = idMedicoIN;
    END IF;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMotivosPorMascota
DELIMITER //
CREATE PROCEDURE obtenerMotivosPorMascota(
	IN idMascotaIN INT
)
this_proc:BEGIN
	SELECT a.id_d_mo as idMotivo, e.nombre as motivo, 
    a.hora_inicio as inicio, a.hora_fin as fin,
    a.id_sala as idSala, b.nombre as sala,a.id_cita,a.id_persona as idMedico, 
    c.nombre as medico,a.id_e_mo as id_estado, d.nombre as estado
    FROM D_MO a
    INNER JOIN SALA b
    ON a.id_sala = b.id_sala
    INNER JOIN PERSONA c
    ON a.id_persona = c.id_persona
    INNER JOIN E_MO d
    ON a.id_e_mo = d.id_e_mo
    INNER JOIN MOTIVO e
    ON a.id_motivo = e.id_motivo
    INNER JOIN CITA f
    ON f.id_cita = a.id_cita
    INNER JOIN MASCOTA g
    ON g.id_mascota = f.id_mascota
    WHERE f.id_mascota = idMascotaIN
    AND a.id_e_mo = 3;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizarFotoMascota
DELIMITER //
CREATE PROCEDURE actualizarFotoMascota(
	IN idMascotaIN INT,
    IN direccionFotoIN VARCHAR(300)
)
this_proc:BEGIN
	UPDATE MASCOTA
    SET foto = direccionFotoIN
    WHERE id_mascota = idMascotaIN;
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerDescuentos
DELIMITER //
CREATE PROCEDURE obtenerDescuentos(
)
this_proc:BEGIN
	#Retornar todas las mascotas del usuario
    SELECT id_descuento AS id, nombre, cantidad, id_e_de AS estado 
    FROM DESCUENTO;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////




#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearProducto
DELIMITER //
CREATE PROCEDURE crearProducto(
	IN nombreIN VARCHAR(100),
	IN precioIN NUMERIC(8,2),
	IN descripcionIN VARCHAR(200),
	IN categoriaIN INT,
	IN marcaIN INT,
	IN stockIN INT
)
this_proc:BEGIN
	DECLARE repetido INT;
    DECLARE idProducto INT;
	#Verificar que el producto no este repetido
    SET repetido = (SELECT id_producto FROM PRODUCTO WHERE
    LOWER(nombre)  = LOWER(nombreIN)
    );
    #Error, ese nombre ya existe
    IF repetido IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el producto ya existe.';
        LEAVE this_proc;
    END IF;
    #Insertar nuevo producto
    INSERT INTO PRODUCTO (nombre,precio,descripcion,id_t_pr,id_c_pr, e_pr,
    pt_gan,pt_cam,pt_precio) VALUES
	(nombreIN,precioIN,descripcionIN,1,categoriaIN,1,0,0,0);
    #Recuperar id del producto nuevo
    SET idProducto = (SELECT id_producto FROM PRODUCTO WHERE
    nombre = nombreIN AND
    precio = precioIN AND
    descripcion = descripcionIN AND
    id_c_pr = categoriaIN);
    #Insertar producto en farmacia
	INSERT INTO FARMACIA (id_producto,stock,id_marca) VALUES
	(idProducto,stockIN,marcaIN);
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS modificarProducto
DELIMITER //
CREATE PROCEDURE modificarProducto(
	IN idProductoIN INT,
	IN precioIN NUMERIC(8,2),
	IN descripcionIN VARCHAR(200),
	IN categoriaIN INT,
	IN marcaIN INT
	#IN stockIN INT
)
this_proc:BEGIN
	DECLARE existe INT;

	#Verificar que el producto no este repetido
    SET existe = (SELECT id_producto FROM PRODUCTO WHERE
    id_producto  = idProductoIN
    );
    #Error, ese nombre ya existe
    IF existe IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el producto no existe.';
        LEAVE this_proc;
    END IF;
    #Modificar datos del producto
    UPDATE PRODUCTO
    SET precio = precioIN,
    descripcion = descripcionIN,
    id_c_pr = categoriaIN
    WHERE id_producto = idProductoIN;
    #Modificar farmacia
    UPDATE FARMACIA
    SET #stock = stockIN,
    id_marca = marcaIN
    WHERE id_producto = idProductoIN;
	SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminarProducto
DELIMITER //
CREATE PROCEDURE eliminarProducto(
	IN idProductoIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE estado INT;
	#Verificar que el producto exista y recuperar su estado
    SELECT id_producto, e_pr INTO existe, estado FROM PRODUCTO WHERE
    id_producto = idProductoIN;
    #Error, ese nombre ya existe
    IF existe IS NULL OR estado = 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el producto no existe.';
        LEAVE this_proc;
    END IF;
    #Modificar datos del producto
    UPDATE PRODUCTO
    SET e_pr = 0
    WHERE id_producto = idProductoIN;
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerProductos
DELIMITER //
CREATE PROCEDURE obtenerProductos(
	IN tipoUsuarioIN INT
)
this_proc:BEGIN
	DECLARE estado INT DEFAULT 1;
    IF tipoUsuarioIN = 1 THEN
		SET estado = 0;
    END IF;

	SELECT a.id_producto as id, a.nombre, a.precio, a.descripcion,
    a.id_c_pr as id_categoria, c.nombre as categoria,
    d.id_marca,d.nombre as marca,b.stock, a.e_pr as estado
    FROM PRODUCTO a
    #Stock y id_categoria
    INNER JOIN FARMACIA b
    ON a.id_producto = b.id_producto
    #nombre categoria
    INNER JOIN C_PR c
    ON a.id_c_pr = c.id_c_pr
    #nombre marca
    INNER JOIN MARCA d
    ON b.id_marca = d.id_marca
    WHERE a.e_pr >= estado AND
    (a.id_c_pr  != 1 AND a.id_c_pr  != 11)
    AND b.stock >= estado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerServicios
DELIMITER //
CREATE PROCEDURE obtenerServicios(
)
this_proc:BEGIN
	SELECT a.id_producto as id,a.nombre,a.precio,a.descripcion,
    a.id_c_pr as id_categoria, c.nombre as categoria,
    b.duracion, pt_gan as ptsGanar, pt_cam as ptsPrecio, 0 as cantidad FROM PRODUCTO a
    INNER JOIN SERVICIO b
    ON a.id_producto = b.id_producto
    #nombre categoria
    INNER JOIN C_PR c
    ON c.id_c_pr = a.id_c_pr;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerMarcas
DELIMITER //
CREATE PROCEDURE obtenerMarcas(
)
this_proc:BEGIN
	SELECT id_marca as id, nombre FROM MARCA;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerCategorias
DELIMITER //
CREATE PROCEDURE obtenerCategorias(
)
this_proc:BEGIN
	SELECT id_c_pr as id, nombre FROM C_PR;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS restock
DELIMITER //
CREATE PROCEDURE restock(
	IN idProductoIN INT,
    IN stockIN INT
)
this_proc:BEGIN
	UPDATE FARMACIA
    SET stock = stockIN
    WHERE id_producto = idProductoIN;
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearPaquete
DELIMITER //
CREATE PROCEDURE crearPaquete(
	IN nombreIN VARCHAR(100),
	IN descripcionIN VARCHAR(200),
	IN precioIN NUMERIC(8,2)
)
this_proc:BEGIN
	DECLARE idProducto INT;
    DECLARE existe INT;
    #Verificar que no este repetido
    SET existe = (SELECT id_producto FROM PRODUCTO WHERE
		LOWER(nombre) = LOWER(nombreIN)
    );
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el paquete ya existe.';
		LEAVE this_proc;
    END IF;
    #Insertar nuevo producto
	INSERT INTO PRODUCTO (nombre,precio,descripcion,id_t_pr,id_c_pr,e_pr) 
    VALUES (nombreIN,precioIN,descripcionIN,3,10,1);
    #Recuperar id del producto
    SET idProducto = (SELECT id_producto FROM PRODUCTO WHERE
		LOWER(nombre) = LOWER(nombreIN) AND
		descripcion = descripcionIN AND
		precio = precioIN AND
		id_t_pr = 3 AND
		id_c_pr = 10
    );    
    #Insertar nuevo paquete
    INSERT INTO PAQUETE (id_producto) VALUES (idProducto);
    SELECT idProducto AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS modificarPaquete
DELIMITER //
CREATE PROCEDURE modificarPaquete(
	IN idPaqueteIN VARCHAR(100),
	IN descripcionIN VARCHAR(200),
	IN precioIN NUMERIC(8,2)
)
this_proc:BEGIN
    DECLARE existe INT;
    #Verificar que exista
    SET existe = (SELECT id_producto FROM PRODUCTO WHERE
		id_producto = idPaqueteIN
    );
	IF existe IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el paquete no existe.';
		LEAVE this_proc;
    END IF;
    #Actualizar valores del paquete
    UPDATE PRODUCTO
    SET precio = precioIN,
    descripcion = descripcionIN
    WHERE id_producto = idPaqueteIN
    ;
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminarPaquete
DELIMITER //
CREATE PROCEDURE eliminarPaquete(
	IN idPaqueteIN VARCHAR(100)
)
this_proc:BEGIN
    DECLARE existe INT;
    #Verificar que exista
    SET existe = (SELECT id_producto FROM PRODUCTO WHERE
		id_producto = idPaqueteIN
    );
	IF existe IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Error, el paquete no existe.';
		LEAVE this_proc;
    END IF;
    #Actualizar valores del paquete
    UPDATE PRODUCTO
    SET e_pr = 0
    WHERE id_producto = idPaqueteIN
    ;
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerPaquetes
DELIMITER //
CREATE PROCEDURE obtenerPaquetes(
	IN tipoUsuarioIN INT
)
this_proc:BEGIN
    DECLARE estadoPaquete INT DEFAULT 1;
    #Configurar el estado de los paquetes a retornar
    IF tipoUsuarioIN = 1 THEN
		SET estadoPaquete = 0;
    END IF;
    #Obtener todos los paquetes dependiendo del tipo de usuario
    SELECT a.id_producto as id ,a.id_producto,a.nombre,a.precio,a.descripcion, 
    a.id_t_pr as id_tipo, b.nombre as tipo, a.id_c_pr as  id_categoria,
    c.nombre as categoria, a.e_pr as estado FROM PRODUCTO a
    #nombre de tipo
    INNER JOIN T_PR b
    ON a.id_t_pr = b.id_t_pr
    #nombre de categoria
    INNER JOIN C_PR c
    ON a.id_c_pr = c.id_c_pr
    WHERE a.id_t_pr = 3
    AND e_pr >= estadoPaquete;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS agregarServicioPaquete
DELIMITER //
CREATE PROCEDURE agregarServicioPaquete(
	IN idPaqueteIN INT,
    IN idServicioIN INT
)
this_proc:BEGIN
    #Asignar servicio a paquete
    INSERT INTO D_PA (id_paquete,id_producto)
    VALUES (idPaqueteIN, idServicioIN);
    SELECT 1 AS resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminarServicioPaquete
DELIMITER //
CREATE PROCEDURE eliminarServicioPaquete(
	IN idPaqueteIN INT,
    IN idServicioIN INT
)
this_proc:BEGIN
    #Eliminar registro
    DELETE FROM D_PA WHERE id_paquete = idPaqueteIN AND id_producto = idServicioIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerDetallePaquete
DELIMITER //
CREATE PROCEDURE obtenerDetallePaquete(
	IN idPaqueteIN INT
)
this_proc:BEGIN
    SELECT a.id_producto as id, c.nombre, c.precio, c.descripcion,
    a.duracion FROM SERVICIO a
    INNER JOIN D_PA b
    ON b.id_producto = a.id_producto
    #Datos del producto
    INNER JOIN PRODUCTO c
    ON c.id_producto = a.id_producto
    WHERE b.id_paquete = idPaqueteIN
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crearFactura
DELIMITER //
CREATE PROCEDURE crearFactura(
	IN idClienteIN INT,
	IN totalIN INT,
    IN puntosIN INT,
    IN addPuntosIN INT
)
this_proc:BEGIN
	DECLARE fechaIN DATETIME ;
    DECLARE idFactura INT;
    #Fecha actual
    SET fechaIN = CURRENT_TIMESTAMP();
	#Crear factura
    INSERT INTO FACTURA (id_persona,fecha,total)
    VALUES (idClienteIN,fechaIN,totalIN);
	#Recuperar id de la factura
    SET idFactura = (SELECT id_factura FROM FACTURA WHERE
		id_persona = idClienteIN 
		AND fecha = fechaIN 
		AND total = totalIN
        );
	/*Actualizar Puntos del usuario*/
    UPDATE USUARIO
    SET puntos = (puntos - puntosIN) + addPuntosIN
    WHERE id_usuario = idClienteIN;
    
    UPDATE FACTURA
    SET puntos = puntosIN
    WHERE id_factura = idFactura;
    
    /*Enviar respuesta*/
	SELECT idFactura as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS agregarProductoFactura
DELIMITER //
CREATE PROCEDURE agregarProductoFactura(
	IN idFacturaIN INT,
	IN idProductoIN INT,
    IN cantidadIN INT
)
this_proc:BEGIN
	DECLARE precioIN NUMERIC(8,2);
    #Conseguir el precio del producto
    SET precioIN = (SELECT precio FROM PRODUCTO WHERE id_producto = idProductoIN);
    #Inserta datos en detalle de factura
    INSERT INTO D_FA (cantidad,total,id_factura,id_producto)
    VALUES (cantidadIN,(precioIN * cantidadIN),idFacturaIN,idProductoIN);
    #Actualizar stock
    call actualizarStock(idProductoIN,cantidadIN);
    SELECT 1 as resultado;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

SELECT * FROM FACTURA;
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS agregarServicioPaqueteFactura
DELIMITER //
CREATE PROCEDURE agregarServicioPaqueteFactura(
	IN idFacturaIN INT,
	IN idServicioIN INT,
    IN idMascotaIN INT,
    IN fechaIN VARCHAR(20),
    IN totalIN NUMERIC(8,2),
    IN puntosIN INT
)
this_proc:BEGIN
	DECLARE fechaCita DATE;
	DECLARE precioIN NUMERIC(8,2);
    DECLARE idDetalleFa INT;
    #Set fecha de la cita
    SET fechaCita = STR_TO_DATE(fechaIN,'%d-%m-%Y');
    #Conseguir el precio del producto
    SET precioIN = (SELECT precio FROM PRODUCTO WHERE id_producto = idServicioIN);
    #Inserta datos en detalle de factura
    INSERT INTO D_FA (cantidad,total,id_factura,id_producto,puntos)
    VALUES (1,totalIN,idFacturaIN,idServicioIN,puntosIN);
    #Recuperar el id de detalle
    SET idDetalleFa = (SELECT id_d_fa FROM D_FA WHERE
    cantidad = 1
    AND total = totalIN
    AND id_factura = idFacturaIN
    AND id_producto = idServicioIN
    ORDER BY id_d_fa DESC
    LIMIT 1
    );
    #Crear cita para la mascota
    INSERT INTO CITA_S (fecha,id_d_fa,id_mascota)
    VALUES (fechaCita,idDetalleFa,idMascotaIN);
	SELECT 1 as resultado;
    
    #Agregar puntos al detalle factura
    
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizarStock
DELIMITER //
CREATE PROCEDURE actualizarStock(
	IN idProductoIN INT,
    IN cantidadIN INT
)
this_proc:BEGIN
	UPDATE FARMACIA
    SET stock = (stock - cantidadIN)
    WHERE id_producto = idProductoIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerFacturas
DELIMITER //
CREATE PROCEDURE obtenerFacturas(
	IN idClienteIN INT
)
this_proc:BEGIN
	SELECT id_factura as id,/*DATE_FORMAT(fecha, '%d-%m-%Y') as fecha*/fecha,total,puntos FROM FACTURA
    WHERE id_persona = idClienteIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerDetalleFactura
DELIMITER //
CREATE PROCEDURE obtenerDetalleFactura(
	IN idFacturaIN INT
)
this_proc:BEGIN
	SELECT /*a.id_factura,*/b.nombre,b.precio,b.descripcion, c.nombre as tipo,
    a.cantidad,a.total,puntos FROM D_FA a
	#Datos del producto
    INNER JOIN PRODUCTO b
    ON b.id_producto = a.id_producto
    #Datos del tipo de producto
    INNER JOIN T_PR c
    ON b.id_t_pr = c.id_t_pr
    WHERE a.id_factura = idFacturaIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////
SELECT * FROM FACTURA;
SELECT * FROM D_FA;
SELECT * FROM CITA;
SELECT * FROM D_MO;

SELECT * FROM USUARIO;
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtenerPuntosUsuario
DELIMITER //
CREATE PROCEDURE obtenerPuntosUsuario(
	IN idUsuarioIN INT
)
this_proc:BEGIN
	SELECT puntos FROM USUARIO
    WHERE id_usuario = idUsuarioIN;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////FUNCIONALIDADES DE PAQUETES/////////////////////////////////
# nombre, descripcion, precio
call crearPaquete('paquete1','descripcion de paquete 1',34.56);
#idPaquete,descripcion, precio
call modificarPaquete(23,'nueva descripcion del paquete 1',35.56);
#idPaquete
call eliminarPaquete(23);
#idTipoUsuario
call obtenerPaquetes(2);
call obtenerPaquetes(1);
#idPaquete, idServicio
call agregarServicioPaquete(23,6);
#idPaquete, idServicio
call eliminarServicioPaquete(23,6);
#idPaquete
call obtenerDetallePaquete(23);
#//////////////////////////////////////////////////////////////////////////////////////


#//////////////////////////FUNCIONALIDADES DE PAGOS////////////////////////////////////
#idCliente, total
call crearFactura(72,525.38);
#idFactura,idProducto,cantidad
call agregarProductoFactura(1,20,6);
#idFactura,idServicio,idMascota,fecha
call agregarServicioPaqueteFactura(1,23,1,'07-10-2022');
#idCliente
call obtenerFacturas(72);
#idFactura
call obtenerDetalleFactura(1);
SELECT * FROM FACTURA;
#//////////////////////////////////////////////////////////////////////////////////////


#/////////////////////////////////////PRUEBAS/////////////////////////////////////

/*
#INSERTAR NUEVO JSON
INSERT INTO PRUEBA_JSON (obj)
VALUES ('{"nombre":"baño"}')

#ACCEDER A UN ATRIBUTO DEL JSON
SELECT obj->'$.nombre' FROM PRUEBA_JSON
WHERE JSON_EXTRACT(obj,'$.nombre') = 'ojo';
*/

#*************************************CREAR PERSONA********************************************
#tipoUs,nombre,correo,edad,direccion,tel,pass,user,especialidad,horario
CALL crearPersona(2,'Tarif2a','tari2a@gmail.com',45,'México','12345678','1234','taria',4,3);
CALL crearPersona(2,'Jose Ramon','jose@gmail.com',28,'zona 10','12345678','1234','user1',1,1);
CALL crearPersona(2,'Jose Quiñonez','joseQ@gmail.com',28,'zona 10','12345678','1234','user2',2,2);
CALL crearPersona(2,'Josed Ral','josess@gmail.com',28,'zona 10','12345678','1234','user3',3,1);
CALL crearPersona(2,'Adal Ramones','adal@gmail.com',45,'México','12345678','1234','user4',4,3);
CALL crearPersona(4,'Jordi Rosado223','jordi232@gmail.com',45,'México','12345678','1234','user5246',4,3);
CALL crearPersonaEmergencia(4,'Jordi Amarillo','jordiz@gmail.com',45,'México','12345678','1234','user6',4,3);
SELECT * FROM PERSONA;
SELECT * FROM CLIENTE;
SELECT * FROM MEDICO;
SELECT * FROM H_L;
SELECT * FROM USUARIO;
SELECT * FROM T_US;
SELECT * FROM E_US;
#**********************************************************************************************




#*************************************ACTUALIZAR ESTADO USUARIO********************************
#idUsuario,estadoNuevo
call actualizarEstadoUsuario(5,2);
SELECT * FROM USUARIO;
SELECT * FROM MA_D;
#**********************************************************************************************


#*************************************INICIAR SESIÓN*******************************************
#idUsuario,estadoNuevo
call iniciarSesion('user3');
SELECT * FROM USUARIO;
#**********************************************************************************************

#*************************************OBTENER MEDICOS******************************************
call obtenerMedicos();
#**********************************************************************************************


#************************************OBTENER USUARIOS******************************************
call obtenerUsuarios(4);
#**********************************************************************************************


#*************************************CREAR MASCOTA********************************************
#usuario,nombre,edad,foto,genero,raza
CALL crearMascota(100,'Kindyz234',2,'fotos/foto2',2,15);

SELECT * FROM USUARIO;
SELECT * FROM E_US;
SELECT * FROM MASCOTA;
SELECT * FROM MA_D;
SELECT * FROM RAZA;
#**********************************************************************************************

#************************************OBTENER MASCOTAS******************************************
call obtenerMascotas(3);
#**********************************************************************************************


#************************************CREAR RAZAS***********************************************
call crearRaza('Selenium');
#**********************************************************************************************

#************************************OBTENER RAZAS*********************************************
call obtenerRazas();
#**********************************************************************************************

#*****************************************MANEJAR DESCUENTOS***********************************
#tipoAActividad,nombre,cantidad,id_descuento
#Crear
call manejarDescuento(1,'segundo',5,0);
#Activar
call manejarDescuento(2,'segundo',5,2);
#Desactivar
call manejarDescuento(3,'segundo',5,1);
#Eliminar
call manejarDescuento(4,'segundo',5,1);
use f1ayd2;
select * from DESCUENTO;
select * from e_de;
#**********************************************************************************************


#************************************OBTENER DESCUENTOS****************************************
call obtenerDescuentos();
#**********************************************************************************************


#**************************************CREAR ESTUDIO*******************************************
#nombre,direccion,mascota
call crearEstudio('Estudio 1','estudios/estudio.pdf',2);
call crearEstudio('Estudio 2','estudios/estudio.pdf',1);
call crearEstudio('Estudio 3','estudios/estudio.pdf',3);
#**********************************************************************************************

#**************************************OBTENER ESTUDIOS****************************************
#id mascota
call obtenerEstudios(1);
#**********************************************************************************************


#*****************************************CREAR LOG********************************************
INSERT INTO LOG (metodo,entrada,salida,esError,fechaHora)
VALUES ('metodo1','entrada1','salida1',0,NOW());

INSERT INTO LOG (metodo,entrada,salida,esError,fechaHora)
VALUES ('metodo2','entrada2','salida2',0,STR_TO_DATE('10-09-2022 17:20:50', '%d-%m-%Y %H:%i:%s'));
#**********************************************************************************************


#****************************************CREAR CITA********************************************
INSERT INTO CITA (fecha,costo,id_mascota,id_descuento,id_e_ci,id_t_ci)
VALUES (STR_TO_DATE('10-09-2022','%d-%m-%Y'),0,1,1,1,1);


INSERT INTO LOG (metodo,entrada,salida,esError,fechaHora)
VALUES ('metodo2','entrada2','salida2',0,STR_TO_DATE('10-09-2022 17:20:50', '%d-%m-%Y %H:%i:%s'));
#**********************************************************************************************


SELECT * FROM USUARIO;
#*****************************************CREAR CITA*******************************************
# fecha,mascota,tipoCita,puntos,addPuntos
call crearCita('07-10-2022',64,1,100,200);
#**********************************************************************************************
SELECT * FROM CITA;

#*****************************************AGREGAR MOTIVO***************************************
# inicio,motivo,sale,cita,medico
call agregarMotivo('10:00:00',2,1,5,1);
#**********************************************************************************************

SELECT * FROM MOTIVO;
#**************************************ACTUALIZAR MOTIVO***************************************
# id_motivo, accion
call actualizarMotivo(13,2);
select * from cita;
select * from d_mo;
select * from pago;
select * from cliente;
UPDATE D_MO
SET id_e_mo = 2
WHERE id_d_mo = 9;
CALL pagar(54,2,1);
SELECT * FROM PAGO;
SELECT * FROM CITA;
#**********************************************************************************************


#***********************************OBTENER CALENDARIOS****************************************
# fecha,mascota,descuento,tipoCita
use f1ayd2;
call obtenerCalendarioOcupadoGeneral();
select * from PERSONA;
call obtenerCalendarioOcupadoDoctor(1);
call obtenerHistorialCitasMedico(69,2);
#**********************************************************************************************
SELECT * FROM CITA;

#**************************************OBTENER SALAS*******************************************
# id_motivo, accion
call obtenerSalas();
#**********************************************************************************************

#**************************************OBTENER HISTORIAL***************************************
# id_motivo, accion
call obtenerHistorialMedico(69,1,1);
call obtenerHistorialCitasCliente(72,1);
SELECT * FROM PERSONA;

#**********************************************************************************************

#**************************************CREAR RECETA********************************************
# id_mascota
call crearReceta(2,'Nueva receta para esa cita');
#**********************************************************************************************

#******************************AGREGAR ELEMENTO RECETA*****************************************
# id_receta id_medicina
call agregarElementoReceta(1,3);
#**********************************************************************************************

use f1ayd2;
#******************************AGREGAR ELEMENTO RECETA*****************************************
# id_usuario
call obtenerPagoGenerado(72,2);
#**********************************************************************************************

#******************************AGREGAR ELEMENTO RECETA*****************************************
# id_pago, id_tipo pago, tipo operacion
call pagar(19,1,1);
SELECT * FROM PAGO;
#**********************************************************************************************


#*********************************OBTENER RECETAS**********************************************
# id_pago, id_tipo pago
call obtenerRecetas(1);
#**********************************************************************************************

#*****************************OBTENER MOTIVOS X CITA*******************************************
call obtenerMotivosPorCita(3);
#**********************************************************************************************


#*****************************OBTENER RECETAS X CITA*******************************************
call obtenerRecetasPorCita(2);
#**********************************************************************************************



#******************************CREAR PRODUCTO**************************************************
#nombre,precio,descripcion,categoria,marca,stock
call crearProducto('Complete Nutrition Adult Dry Dog',8.74,
'Food Roasted Chicken 2.8 lbs',7,2,15);

call crearProducto('Clipper Pet-Pro Dog Grooming Kit',33.82,
'Duty Electric Corded Dog Clipper for Dogs & Cats',7,57,7);

call crearProducto('Enzymatic Toothpaste for Dogs2',3.96,
'1 Pack Fresh Breath Vanilla Ginger2',10,81,0);

#**********************************************************************************************


#******************************MODIFICAR PRODUCTO**********************************************
#idProducto,precio,descripcion,categoria,marca
call modificarProducto(9,9.74,
'Food Roasted Chicken 3.8 lbs',7,2);
#**********************************************************************************************

SELECT * FROM PRODUCTO;
#******************************ELIMINAR PRODUCTO***********************************************
#idProducto
call eliminarProducto(9);
#**********************************************************************************************


#*********************************OBTENER PRODUCTOs********************************************
#tipUsuario
/*
	1-> Admin
    2-> Cliente
*/
use f1ayd2;
call obtenerProductos(2);
call obtenerProductos(1);
#**********************************************************************************************



#*********************************OBTENER SERVICIOS********************************************
call obtenerServicios();
#**********************************************************************************************


#*********************************OBTENER SERVICIOS********************************************
call obtenerCategorias();
#**********************************************************************************************

#*********************************OBTENER SERVICIOS********************************************
call obtenerMarcas();
#**********************************************************************************************

#*********************************RESTOCK PRODUCTOS********************************************
call restock(18,5);
#**********************************************************************************************

SELECT * FROM PAGO;
SELECT * FROM CITA;
SELECT * FROM D_MO;
call obtenerPagoGenerado(17,1);

UPDATE PAGO
SET id_e_pa = 2
WHERE id_pago > 0;

call obtenerPagoGenerado(17,2)