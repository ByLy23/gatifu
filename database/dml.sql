#CARGAR RAZAS CON CSV razas.csv
SELECT * FROM RAZA;

#CARGAR MEDICAMENTOS DESDE CSV medicinas
SELECT * FROM MEDICAMENTO;

#CARGAR ESTADOS DE DESCUENTO
INSERT INTO E_DE(nombre) VALUES('Activo');
INSERT INTO E_DE(nombre) VALUES('Inactivo');
INSERT INTO E_DE(nombre) VALUES('Eliminado');


#CARGAR DIAS DE LA SEMANA
INSERT INTO DIA(nombre) VALUES('Lunes');
INSERT INTO DIA(nombre) VALUES('Martes');
INSERT INTO DIA(nombre) VALUES('Miércoles');
INSERT INTO DIA(nombre) VALUES('Jueves');
INSERT INTO DIA(nombre) VALUES('Viernes');
INSERT INTO DIA(nombre) VALUES('Sábado');
INSERT INTO DIA(nombre) VALUES('Domingo');
#SELECT * FROM DIA;

#CARGAR HORARIOS
INSERT INTO HORARIO(nombre,inicio,fin) VALUES ('8am - 4pm',STR_TO_DATE('08:00:00','%H:%i:%s'),
STR_TO_DATE('16:00:00','%H:%i:%s'));
INSERT INTO HORARIO(nombre,inicio,fin) VALUES ('4pm - 12am',STR_TO_DATE('16:00:00','%H:%i:%s'),
STR_TO_DATE('00:00:00','%H:%i:%s'));
INSERT INTO HORARIO(nombre,inicio,fin) VALUES ('12am - 8am',STR_TO_DATE('00:00:00','%H:%i:%s'),
STR_TO_DATE('08:00:00','%H:%i:%s'));
#SELECT * FROM HORARIO;


/*
UPDATE f1ayd2.HORARIO
SET inicio = STR_TO_DATE('00:00:00','%H:%i:%s')
WHERE id_horario = 3;

UPDATE f1ayd2.HORARIO
SET fin = STR_TO_DATE('00:00:00','%H:%i:%s')
WHERE id_horario = 3;
*/

#CARGAR ESPECIALIDADES
INSERT INTO ESPECIALIDAD(nombre) VALUES ('Medicina General');
INSERT INTO ESPECIALIDAD(nombre) VALUES ('Traumatología');
INSERT INTO ESPECIALIDAD(nombre) VALUES ('Oftalmología');
INSERT INTO ESPECIALIDAD(nombre) VALUES ('Ginecología');
INSERT INTO ESPECIALIDAD(nombre) VALUES ('Análisis de Laboratorio');
#SELECT * FROM ESPECIALIDAD;

#CARGAR MOTIVOS
INSERT INTO MOTIVO(nombre,precio,tiempo) VALUES ('Medicina General',100,1);
INSERT INTO MOTIVO(nombre,precio,tiempo) VALUES ('Traumatología',200,1);
INSERT INTO MOTIVO(nombre,precio,tiempo) VALUES ('Oftalmología',200,1);
INSERT INTO MOTIVO(nombre,precio,tiempo) VALUES ('Ginecología',300,1);
INSERT INTO MOTIVO(nombre,precio,tiempo) VALUES ('Análisis de Laboratorio',500,1);
#SELECT * FROM MOTIVO;

#CARGAR SALAS
INSERT INTO SALA(nombre) VALUES ('Sala General 1');
INSERT INTO SALA(nombre) VALUES ('Sala General 2');
INSERT INTO SALA(nombre) VALUES ('Sala General 3');
INSERT INTO SALA(nombre) VALUES ('Traumatología');
INSERT INTO SALA(nombre) VALUES ('Oftalmología');
INSERT INTO SALA(nombre) VALUES ('Ginecología');
INSERT INTO SALA(nombre) VALUES ('Laboratorio');
#SELECT * FROM SALA;



#CARGAR MONEDAS
INSERT INTO MONEDA(nombre) VALUES ('Quetzal');
INSERT INTO MONEDA(nombre) VALUES ('Dolar');
#SELECT * FROM MONEDA;

#CARGAR TIPO DE PAGO
INSERT INTO T_PA(nombre) VALUES ('Efectivo');
INSERT INTO T_PA(nombre) VALUES ('Tarjeta de crédito');
#SELECT * FROM T_PA;

#CARGAR ESTADOS DEL MOTIVOS
INSERT INTO E_MO(nombre) VALUES ('En espera');
INSERT INTO E_MO(nombre) VALUES ('Activo');
INSERT INTO E_MO(nombre) VALUES ('Finalizado');
#SELECT * FROM E_MO;

#CARGAR ESTADO DEL PAGO
INSERT INTO E_PA(nombre) VALUES ('Pendiente');
INSERT INTO E_PA(nombre) VALUES ('Cancelado');
INSERT INTO E_PA(nombre) VALUES ('No pagado');
#SELECT * FROM E_PA;

#CARGAR ESTADO DE LA CITA
INSERT INTO E_CI(nombre) VALUES ('En espera');
INSERT INTO E_CI(nombre) VALUES ('Activa');
INSERT INTO E_CI(nombre) VALUES ('Finalizada');
INSERT INTO E_CI(nombre) VALUES ('Eliminada');
#SELECT * FROM E_CI;

#CARGAR Tipo del cliente
INSERT INTO T_CL(nombre) VALUES ('Nuevo');
INSERT INTO T_CL(nombre) VALUES ('Recurrente');
#SELECT * FROM T_CL;

#CARGAR Estado del usuario
INSERT INTO E_US(nombre) VALUES ('Pendiente');
INSERT INTO E_US(nombre) VALUES ('De alta');
INSERT INTO E_US(nombre) VALUES ('De baja');
#SELECT * FROM E_US;

#CARGAR tipo del usuario
INSERT INTO T_US(nombre) VALUES ('Administrador');
INSERT INTO T_US(nombre) VALUES ('Médico');
INSERT INTO T_US(nombre) VALUES ('Secretaria');
INSERT INTO T_US(nombre) VALUES ('Cliente');
#SELECT * FROM T_US;

#CARGAR Tipos de citas
INSERT INTO T_CI(nombre) VALUES ('Normal');
INSERT INTO T_CI(nombre) VALUES ('Emergencia');
#SELECT * FROM T_CI;


#Insertar Descuentos
INSERT INTO DESCUENTO(nombre,cantidad,id_e_de)
VALUES ('Descuento de verano',5,1);

#CARGAR Descuentos
#INSERT INTO DESCUENTO(nombre,cantidad) VALUES ('2 motivos',10);
#INSERT INTO DESCUENTO(nombre,cantidad) VALUES ('3 motivos',20);
#INSERT INTO DESCUENTO(nombre,cantidad) VALUES ('4 motivos',30);
#SELECT * FROM DESCUENTO;


#CAMBIAR, QUITAR ID USUARIO AUTO INCREMENT
#SET FOREIGN_KEY_CHECKS=0;
#ALTER TABLE USUARIO CHANGE id_usuario id_usuario INT;
#SET FOREIGN_KEY_CHECKS=1;

#FASE 2
#Llenar tablas verdes
SELECT * FROM MARCA;
INSERT INTO MARCA (nombre) VALUES 
('Vets Preferred'),
('Radius'),
('Vets Best'),
('Virbac'),
('Arm & Hammer'),
('Petrodex');

INSERT INTO C_PR (nombre) VALUES ('Grooming'), ('Vitaminas'),('Medicina'),
('Ropa'),('Accesorios'),('Juguetes'),('Comida'),('Premios'),('Camas'),('Salud'),('Paquete');

SELECT * FROM C_PR;

INSERT INTO T_PR (nombre) VALUES ('Producto'),('Servicio'),('Paquete');


INSERT INTO PRODUCTO (nombre,precio,descripcion,id_t_pr,id_c_pr,e_pr) VALUES
('Baño',100,'Baño completo a mascota del cliente.',2,1,1),
('Corte de pelo',100,'Corte de pelo con estilo para la mascota del cliente.',2,1,1),
('Masaje',50,'Masaje de cuerpo completo para la mascota del cliente.',2,1,1),
('Corte de uñas',50,'Corte de uñas para la mascota del cliente.',2,1,1),
('Cepillado',100,'Cepillado completo de pelo para la mascota del cliente.',2,1,1),
('Limpieza de oidos',50,'Limpieza en seco de oidos para la mascota del cliente.',2,1,1),
('Cepillado de dientes',50,'Cepillado de dientes con pasta especial para la mascota del cliente.',2,1,1),
('Shampoo anti pulgas',75,'Aplicación de shampoo anti pulgas para la mascota del cliente.',2,1,1)
;


INSERT INTO SERVICIO (id_producto, duracion) VALUES 
(1,60),
(2,30),
(3,20),
(4,5),
(5,30),
(6,5),
(7,30),
(8,0);


/*********************************FASE 3 UPDATE*************************************/
/*Actualizar precio productos*/

UPDATE PRODUCTO
SET pt_gan = 0,
pt_cam = 0,
pt_precio = 0
WHERE id_producto > 0;

/*Actualizar puntos del usuario*/
UPDATE USUARIO
SET puntos = 0
WHERE id_usuario > 0;
/*Actualizar puntos del motivo*/
UPDATE MOTIVO
SET pt_gan = 0,
pt_cam = 0,
pt_precio = 0
WHERE id_motivo > 0;

/*Agregar puntos a los servicios*/
/*Agregar calculos de precios y puntos en servicios*/

/*Citas*/
UPDATE MOTIVO
SET pt_gan = 350,
pt_cam = 650,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_motivo = 1;

UPDATE MOTIVO
SET pt_gan = 300,
pt_cam = 600,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_motivo = 2;

UPDATE MOTIVO
SET pt_gan = 400,
pt_cam = 800,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_motivo = 3;

UPDATE MOTIVO
SET pt_gan = 150,
pt_cam = 300,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_motivo = 4;

UPDATE MOTIVO
SET pt_gan = 350,
pt_cam = 650,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_motivo = 5;

/*Productos*/
UPDATE PRODUCTO
SET pt_gan = 250,
pt_cam = 500,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_producto = 1;

UPDATE PRODUCTO
SET pt_gan = 200,
pt_cam = 400,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_producto = 2;

UPDATE PRODUCTO
SET pt_gan = 100,
pt_cam = 200,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_producto = 3;

UPDATE PRODUCTO
SET pt_gan = 150,
pt_cam = 300,
pt_precio = ROUND(precio/pt_cam,4)
WHERE id_producto > 3 AND id_producto < 9;

UPDATE D_MO
SET puntos = 0
WHERE id_d_mo > 0;

UPDATE D_FA
SET puntos = 0
WHERE id_d_fa > 0;

UPDATE CITA
SET puntos = 0
WHERE id_cita > 0;

UPDATE FACTURA
SET puntos = 0
WHERE id_factura > 0;


/***********************************************************************************/